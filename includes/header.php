<?php
require('manager/conn.php');
require('manager/db-tables.php');
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
$pageurl = 'http://'.$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
if($pageurl == SITEURL.'index.php'){

$find = 'index.php';
$search = strpos($pageurl,$find);
if($search !== false){
$meta = mysql_query("SELECT * FROM cms_posts WHERE id = 1") or die(mysql_error());
$row = mysql_fetch_array($meta);
$title = stripslashes($row['title_meta']);
$keywords = stripslashes($row['keywords_meta']);
$description = stripslashes($row['description_meta']);
}
}

elseif($pageurl == SITEURL.'rentals.php?category=4'){
$find4 = 'rentals.php?category=4';
$search4 = strpos($pageurl,$find4);
if($search4 !== false){
$meta = mysql_query("SELECT * FROM cms_posts WHERE id = 2") or die(mysql_error());
$row = mysql_fetch_array($meta);
$title = stripslashes($row['title_meta']);
$keywords = stripslashes($row['keywords_meta']);
$description = stripslashes($row['description_meta']);
}
}

elseif($pageurl == SITEURL.'south_fork.php'){
$find4 = 'south_fork.php';
$search4 = strpos($pageurl,$find4);
if($search4 !== false){
$meta = mysql_query("SELECT * FROM cms_posts WHERE id = 3") or die(mysql_error());
$row = mysql_fetch_array($meta);
$title = stripslashes($row['title_meta']);
$keywords = stripslashes($row['keywords_meta']);
$description = stripslashes($row['description_meta']);
}
}

elseif($pageurl == SITEURL.'faq.php'){
$find4 = 'faq.php';
$search4 = strpos($pageurl,$find4);
if($search4 !== false){
$meta = mysql_query("SELECT * FROM cms_posts WHERE id = 5") or die(mysql_error());
$row = mysql_fetch_array($meta);
$title = stripslashes($row['title_meta']);
$keywords = stripslashes($row['keywords_meta']);
$description = stripslashes($row['description_meta']);
}
}

elseif($pageurl == SITEURL.'contact.php'){
$find4 = 'contact.php';
$search4 = strpos($pageurl,$find4);
if($search4 !== false){
$meta = mysql_query("SELECT * FROM cms_posts WHERE id = 4") or die(mysql_error());
$row = mysql_fetch_array($meta);
$title = stripslashes($row['title_meta']);
$keywords = stripslashes($row['keywords_meta']);
$description = stripslashes($row['description_meta']);
}
}

elseif($pageurl == SITEURL.'contect.php'){
$find4 = 'contect.php';
$search4 = strpos($pageurl,$find4);
if($search4 !== false){
$meta = mysql_query("SELECT * FROM cms_posts WHERE id = 4") or die(mysql_error());
$row = mysql_fetch_array($meta);
$title = stripslashes($row['title_meta']);
$keywords = stripslashes($row['keywords_meta']);
$description = stripslashes($row['description_meta']);
}
}

elseif($pageurl == SITEURL.'special.php'){
$find4 = 'special.php';
$search4 = strpos($pageurl,$find4);
if($search4 !== false){
$meta = mysql_query("SELECT * FROM cms_posts WHERE id = 8") or die(mysql_error());
$row = mysql_fetch_array($meta);
$title = stripslashes($row['title_meta']);
$keywords = stripslashes($row['keywords_meta']);
$description = stripslashes($row['description_meta']);
}
}



else{
$meta = mysql_query("SELECT * FROM cms_posts WHERE id = 1") or die(mysql_error());
$row = mysql_fetch_array($meta);
$title = stripslashes($row['title_meta']);
$keywords = stripslashes($row['keywords_meta']);
$description = stripslashes($row['description_meta']);
}



?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title><?php echo $title; ?></title>

<meta content="<?php echo $keywords; ?>" name="keywords">

<meta content="<?php echo $description; ?>" name="description">
<title>South Fork Vacations</title>
<link href="css/style_sheet.css" rel="stylesheet" type="text/css"/>
<link href='http://fonts.googleapis.com/css?family=Ubuntu:300' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
<!--<script type="text/jscript" src="http://code.jquery.com/jquery.js"></script>-->
<script src="js/jquery.min.js"></script>
<script defer src="js/jquery.flexslider.js"></script>
<script type="text/javascript">
		jQuery(document).ready(function(){
			
			$("a.show_menu").click(function(){
			$("#menu").toggle();
			});
			
			$(function(){
				SyntaxHighlighter.all();
			});
    
			$('.flexslider').flexslider({
			  animation: "slide",
			  start: function(slider){
				$('body').removeClass('loading');
			  }
			});
		
		});
		
</script>

  
  <script type="text/javascript">
  function getdate(){
	  var datepick = $('#datepicker').val();
	  if(datepick == ''){
		  alert('Please enter date before search');
		  return false;
	  }
  }
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55017660-1', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body>
	<div id="wrapper">
    		<div id="header">
            	<div class="logo">
                	<a href="index.php"><img src="images/south_logo.png" alt="#"/></a>
                </div>
                <div class="contact">
                		<div class="contact_socai">
                        		<a href="https://www.facebook.com/pages/South-Fork-Vacation-Rentals/153753028021622" target="_blank">&nbsp;</a>
                        </div>
                        <div class="contact_number">
                        		<p>(719) 221-6744</p>
                        </div>
                        <div class="clr"></div>
                </div>
                <div class="clr"></div>
                <a class="show_menu" href="javascript:void(0)">&nbsp;</a>	
            </div>
            <div id="menu">
            	<ul>
                	<li>
                    	<a class="select" href="index.php">Home</a>
                    </li>
                    <li>
                    	<a href="javascript:void(0);">Vacation Rentals</a>
                        <ul>
                        	<li>
                            	<a href="rentals.php?bedroom=2">1 to 2 bedrooms</a>
                            </li>
                            <li>
                            	<a href="rentals.php?bedroom=3">3 bedrooms</a>
                            </li>
                            <li>
                            	<a href="rentals.php?bedroom=4">4 + bedrooms</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                    	<a href="south_fork.php">Experience South Fork</a>
                    </li>
                    <li>
                    	<a href="faq.php">FAQ's</a>
                    </li>
                    <li>
                    	<a href="http://southforkvacation.blogspot.com/">Blog</a>
                    </li>
                    <li>
                    	<a href="<?php if($pageurl == SITEURL.'contect.php'){ ?>contect.php<?php }else{ ?>contact.php <?php } ?>">Contact</a>
                    </li>
                </ul>	
                <div class="clr"></div>
            </div>