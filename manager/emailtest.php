<?php

//print_r ($_REQUEST);

if(file_exists('../phpmailer/class.phpmailer.php')){require_once('../phpmailer/class.phpmailer.php');}elseif(file_exists('phpmailer/class.phpmailer.php')){require_once('phpmailer/class.phpmailer.php');}elseif(file_exists('manager/phpmailer/class.phpmailer.php')){require_once('manager/phpmailer/class.phpmailer.php');}elseif(file_exists('phpmailer/class.phpmailer.php')){require_once('phpmailer/class.phpmailer.php');}

if(file_exists('../manager/db-tables.php')){require_once('../manager/db-tables.php');}elseif(file_exists('manager/db-tables.php')){require_once('manager/db-tables.php');}elseif(file_exists('db-tables.php')){require_once('db-tables.php');}

if( (isset($_REQUEST['date_start']) OR isset($_REQUEST['date_end'])) AND ($_REQUEST['date_start']=="mm/dd/yyyy" OR $_REQUEST['date_end']=="mm/dd/yyyy") ){
die("Please Select Dates on calendar before searching!<br/><br/><br/>");
}

$GLOBALS['message'] = "";
$message = "";

	function smtpmailer($to, $from, $from_name, $subject, $body, $ishtml = true, $is_gmail = false, $attachment = '') { 
				global $error;
				$mail = new PHPMailer();
				$mail->IsSMTP();
				$mail->IsHTML($ishtml);
				$mail->SMTPAuth = true; 
				if ($is_gmail) {
					$mail->SMTPSecure = 'ssl'; 
					$mail->Host = 'smtp.gmail.com';
					$mail->Port = 465;  
					$mail->Username = 'wolfwebdevelopment@gmail.com';  
					$mail->Password = 'password';   
				} else {
					$mail->Host = $_SESSION['SMTPSERVER'];
					$mail->Username = $_SESSION['SMTPUSER'];
					$mail->Password = $_SESSION['SMTPPWD'];
					$mail->Port = $_SESSION['SMTPPORT'];
				}        
				$mail->SetFrom($from, $from_name);
				$mail->Subject = $subject;
				$mail->Body = $body;
				if(!empty($attachment)) $mail->AddAttachment("../pictures/".$attachment);

				$mail->ClearAddresses();
				$mail->AddAddress($to);
				if(!$mail->Send()) {
					$warning = 'Mail error: '.$mail->ErrorInfo;
					return false;
				} else {
					$success = 'Message sent!';
					return true;
				}
			}

	function get_email_template($id){
	$q_email="SELECT * FROM ".EMAILTEMPLATES." WHERE id = '$id' AND active = '1'";
	$sql_email= @mysql_query($q_email);
	if(!$sql_email)
	die('<p>Error performing query: ' . mysql_error() .'</p>');
			while($row_email=mysql_fetch_array($sql_email))
			{
		
			$_SESSION['SMTPUSER'] = $row_email['smtp_user']; 
			$_SESSION['SMTPPWD'] = $row_email['smtp_password']; 
			$_SESSION['SMTPSERVER'] = $row_email['smtp_server']; 
			$_SESSION['SMTPPORT'] = $row_email['smtp_port']; 
			
			$_SESSION['EMAIL_FROM'] = $row_email['my_display_email']; 
			$_SESSION['EMAIL_FROM_NAME'] = $row_email['my_display_name']; 
			$_SESSION['EMAIL_SUBJECT'] = $row_email['email_subject']; 

			$_SESSION['EMAIL_BODY_FIXED'] = $row_email['email_body_fixed']; 
			$_SESSION['EMAIL_BODY'] = html_entity_decode($row_email['email_body']); 

			}
		}

	function sendpendingemail($email,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property)
	{
		
		$date_start = date("m/d/Y",strtotime($date_start));
		$date_end = date("m/d/Y",strtotime($date_end));
		$nights = daysDifference($date_start, $date_end);
		$property = get_property_name($property);
	
		get_email_template(1);

			$searchvariables  = array('[email]', '[firstname]', '[lastname]', '[adults]', '[children]', '[pets]', '[sum_detail]', '[total_amount]', '[date_start]', '[date_end]', '[nights]', '[property]', '[sitename]');
			$replacevariables = array($email, $firstname, $lastname, $adults, $children, $pets, $sum_detail, money($total_amount), $date_start, $date_end, $nights, $property, SITE_NAME);
			$msg1 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY_FIXED']);
			$msg2 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY']);
			$msg = $msg1."\n\n".$msg2;
			$msg = nl2br($msg);
			$msg = html_entity_decode($msg);
		
		//	$from = "From: ".$_SESSION['EMAIL_FROM_NAME']." <".$_SESSION['EMAIL_FROM'].">";
		//	mail("programmer@wolfwebdevelopment.com", $_SESSION['EMAIL_SUBJECT'], $msg, $from ); // Abrar
		
		
		
		if (smtpmailer($email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) { // Abrar
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
	
		
		if (smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) { // Abrar
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
	
	
	}

	function sendbookedemail50($email,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$status,$property)
	{
		
		$date_start = date("m/d/Y",strtotime($date_start));
		$date_end = date("m/d/Y",strtotime($date_end));
		$nights = daysDifference($date_start, $date_end);
		$owner = get_property_owner($property);
		$owner = get_owner_name($owner);
		$property = get_property_name($property);
		$requested_amount = $total_amount/2;
		$amount_due = $total_amount-$requested_amount;
		
		get_email_template(2);

			$searchvariables  = array('[email]', '[firstname]', '[lastname]', '[adults]', '[children]', '[pets]', '[sum_detail]', '[total_amount]', '[date_start]', '[date_end]', '[nights]', '[status]', '[property]', '[owner]', '[requested_amount]', '[amount_due]', '[sitename]');
			$replacevariables = array($email, $firstname, $lastname, $adults, $children, $pets, $sum_detail, money($total_amount), $date_start, $date_end, $nights, $status, $property, $owner, money($requested_amount), money($amount_due), SITE_NAME);
			$msg1 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY_FIXED']);
			$msg2 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY']);
			$msg = $msg1."\n\n".$msg2;
			$msg = nl2br($msg);
			$msg = html_entity_decode($msg);
		
			$from = "From: ".$_SESSION['EMAIL_FROM_NAME']." <".$_SESSION['EMAIL_FROM'].">";
		//	mail("programmer@wolfwebdevelopment.com", $_SESSION['EMAIL_SUBJECT'], $msg, $from ); // Abrar
				
		if (smtpmailer($email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) { // Abrar
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
		
		if (smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) { // Abrar
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
	
	}

	function sendbookedemail100($email,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$status,$property)
	{
		
		$date_start = date("m/d/Y",strtotime($date_start));
		$date_end = date("m/d/Y",strtotime($date_end));
		$nights = daysDifference($date_start, $date_end);
		$owner = get_property_owner($property);
		$owner = get_owner_name($owner);
		$property = get_property_name($property);
		$requested_amount = $total_amount;
		
		get_email_template(3);

			$searchvariables  = array('[email]', '[firstname]', '[lastname]', '[adults]', '[children]', '[pets]', '[sum_detail]', '[total_amount]', '[date_start]', '[date_end]', '[nights]', '[status]', '[property]', '[owner]', '[requested_amount]', '[sitename]');
			$replacevariables = array($email, $firstname, $lastname, $adults, $children, $pets, $sum_detail, money($total_amount), $date_start, $date_end, $nights, $status, $property, $owner, money($requested_amount), SITE_NAME);
			$msg1 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY_FIXED']);
			$msg2 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY']);
			$msg = $msg1."\n\n".$msg2;
			$msg = nl2br($msg);
			$msg = html_entity_decode($msg);
		
			$from = "From: ".$_SESSION['EMAIL_FROM_NAME']." <".$_SESSION['EMAIL_FROM'].">";
		//	mail("programmer@wolfwebdevelopment.com", $_SESSION['EMAIL_SUBJECT'], $msg, $from ); // Abrar
				
		if (smtpmailer($email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) { // Abrar
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
		
		if (smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) { // Abrar
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
	
	}

	function senddepositemail($email,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property,$amount_paid,$amount_due,$due_date)
	{
		
		$date_start = date("m/d/Y",strtotime($date_start));
		$date_end = date("m/d/Y",strtotime($date_end));
		$due_date = date("m/d/Y",strtotime($due_date));
		$nights = daysDifference($date_start, $date_end);
		$owner = get_property_owner($property);
		$owner = get_owner_name($owner);
		$property = get_property_name($property);
	
		get_email_template(4);

			$searchvariables  = array('[email]', '[firstname]', '[lastname]', '[adults]', '[children]', '[pets]', '[sum_detail]', '[total_amount]', '[date_start]', '[date_end]', '[nights]', '[property]', '[owner]', '[amount_paid]', '[amount_due]', '[due_date]', '[sitename]');
			$replacevariables = array($email, $firstname, $lastname, $adults, $children, $pets, $sum_detail, money($total_amount), $date_start, $date_end, $nights, $property, $owner, money($amount_paid), money($amount_due), $due_date, SITE_NAME);
			$msg1 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY_FIXED']);
			$msg2 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY']);
			$msg = $msg1."\n\n".$msg2;
			$msg = nl2br($msg);
			$msg = html_entity_decode($msg);
		
			$from = "From: ".$_SESSION['EMAIL_FROM_NAME']." <".$_SESSION['EMAIL_FROM'].">";
		//	mail("programmer@wolfwebdevelopment.com", $_SESSION['EMAIL_SUBJECT'], $msg, $from ); // Abrar
				
		if (smtpmailer($email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) { // Abrar
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
		
		if (smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) { // Abrar
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
	
	
	}

	function sendfinalemail($email,$firstname,$lastname,$adults,$children,$pets,$amount_due,$sum_detail,$total_amount,$date_start,$date_end,$property)
	{

		$date_start = date("m/d/Y",strtotime($date_start));
		$date_end = date("m/d/Y",strtotime($date_end));
		$nights = daysDifference($date_start, $date_end);
		$pdf = get_property_pdf($property);
		$property = get_property_name($property);
		
		get_email_template(6);

			$searchvariables  = array('[email]', '[firstname]', '[lastname]', '[adults]', '[children]', '[pets]', '[amount_due]', '[sum_detail]', '[total_amount]',  '[date_start]', '[date_end]', '[nights]', '[status]', '[property]', '[sitename]');
			$replacevariables = array($email, $firstname, $lastname, $adults, $children, $pets, $amount_due, $sum_detail, money($total_amount), $date_start, $date_end, $nights, $status, $property, SITE_NAME);
			$msg1 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY_FIXED']);
			$msg2 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY']);
			$msg = $msg1."\n\n".$msg2;
			$msg = nl2br($msg);
			$msg = html_entity_decode($msg);
		
		//	$from = "From: ".$_SESSION['EMAIL_FROM_NAME']." <".$_SESSION['EMAIL_FROM'].">";
		//	mail("programmer@wolfwebdevelopment.com", $_SESSION['EMAIL_SUBJECT'], $msg, $from ); // Abrar
		
		if (smtpmailer($email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg, true, false, $pdf)){ // Abrar
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
	
		
		if (smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg, true, false, $pdf)){ // Abrar
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
	
	
	}
	
	function senderroremail($email,$firstname,$lastname,$amount,$date_start,$date_end,$property)
	{

		$date_start = date("m/d/Y",strtotime($date_start));
		$date_end = date("m/d/Y",strtotime($date_end));
	
		$property = get_property_name($property);
		
		get_email_template(0);

			$searchvariables  = array('[firstname]', '[lastname]', '[email]', '[amount]', '[date_start]', '[date_end]', '[property]', '[sitename]');
			$replacevariables = array($firstname, $lastname, $email, $amount, $date_start, $date_end, $property, SITE_NAME);
			$msg1 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY_FIXED']);
			$msg2 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY']);
			$msg = $msg1."\n\n".$msg2;
			$msg = nl2br($msg);
			$msg = html_entity_decode($msg);
			
			//	$from = "From: ".$_SESSION['EMAIL_FROM_NAME']." <".$_SESSION['EMAIL_FROM'].">";
			//	mail("programmer@wolfwebdevelopment.com", $_SESSION['EMAIL_SUBJECT'], $msg, $from );
				 
		if (smtpmailer($email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) {
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
		 
		if (smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) {
			echo '';
		} else {
			echo 'Error Sending Email!<br/>';
		}
	
	
	}
	
	function sendowneremail($owner_id,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property)
	{
	
		$date_start = date("m/d/Y",strtotime($date_start));
		$date_end = date("m/d/Y",strtotime($date_end));
		$nights = daysDifference($date_start, $date_end);
		$property = get_property_name($property);
		$owner_email = get_owner_email($owner_id);
		
		get_email_template(7);

			$searchvariables  = array('[firstname]', '[lastname]', '[adults]', '[children]', '[pets]', '[sum_detail]', '[total_amount]', '[date_start]', '[date_end]', '[nights]', '[property]', '[sitename]');
			$replacevariables = array($firstname, $lastname, $adults, $children, $pets, $sum_detail, $total_amount, $date_start, $date_end, $nights, $property, SITE_NAME);
			$msg1 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY_FIXED']);
			$msg2 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY']);
			$msg = $msg1."\n\n".$msg2;
			$msg = nl2br($msg);
			$msg = html_entity_decode($msg);
		 
		if (smtpmailer($owner_email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) {
			echo '';
		} else {
			$error .= 'Error Sending Email to owner!<br/>';
		}
		
		if (smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) {
			echo '';
		} else {
			$error .= 'Error Sending Email to owner!<br/>';
		}
		
	}
	
	function sendhousekeeperemail($housekeeper,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property)
	{

		$date_start = date("m/d/Y",strtotime($date_start));
		$date_end = date("m/d/Y",strtotime($date_end));
		$nights = daysDifference($date_start, $date_end);
		$property = get_property_name($property);
		$housekeeper_name = get_housekeeper_name($housekeeper);
		$housekeeper_email = get_housekeeper_email($housekeeper);
		$deny = "<a href=\"".SITE_URL."/housekeeper-job.php?deny=".md5($GLOBALS['insert_guest_id'])."\">Deny</a>";
		$accept =  "<a href=\"".SITE_URL."/housekeeper-job.php?accept=".md5($GLOBALS['insert_guest_id'])."\">Accept</a>";
		
		get_email_template(8);

			$searchvariables  = array('[housekeeper]', '[firstname]', '[lastname]', '[adults]', '[children]', '[pets]', '[sum_detail]', '[total_amount]', '[date_start]', '[date_end]', '[nights]', '[property]', '[deny]', '[accept]', '[sitename]');
			$replacevariables = array($housekeeper_name, $firstname, $lastname, $adults, $children, $pets, $sum_detail, $total_amount, $date_start, $date_end, $nights, $property, $deny, $accept, SITE_NAME);
			$msg1 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY_FIXED']);
			$msg2 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY']);
			$msg = $msg1."\n\n".$msg2;
			$msg = nl2br($msg);
			$msg = html_entity_decode($msg);
				 
		if (smtpmailer($housekeeper_email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) {
			echo '';
		} else {
			$error .= 'Error Sending Email to housekeeper!<br/>';
		}
		
		if (smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) {
			echo '';
		} else {
			$error .= 'Error Sending Email to housekeeper!<br/>';
		}
	
	
	}
	
	function sendmaintenanceemail($maintenance,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property)
	{

		$date_start = date("m/d/Y",strtotime($date_start));
		$date_end = date("m/d/Y",strtotime($date_end));
		$nights = daysDifference($date_start, $date_end);
		$property = get_property_name($property);
		$maintenance_name = get_maintenance_name($maintenance);
		$maintenance_email = get_maintenance_email($maintenance);
		$deny = "<a href=\"".SITE_URL."/maintenance-job.php?deny=".md5($GLOBALS['insert_guest_id'])."\">Deny</a>";
		$accept =  "<a href=\"".SITE_URL."/maintenance-job.php?accept=".md5($GLOBALS['insert_guest_id'])."\">Accept</a>";
		
		get_email_template(9);

			$searchvariables  = array('[maintenance]', '[firstname]', '[lastname]', '[nights]', '[adults]', '[children]', '[pets]', '[sum_detail]', '[total_amount]', '[date_start]', '[date_end]', '[property]', '[deny]', '[accept]', '[sitename]');
			$replacevariables = array($maintenance_name, $firstname, $lastname, $nights, $adults, $children, $pets, $sum_detail, $total_amount, $date_start, $date_end, $property, $deny, $accept, SITE_NAME);
			$msg1 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY_FIXED']);
			$msg2 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY']);
			$msg = $msg1."\n\n".$msg2;
			$msg = nl2br($msg);
			$msg = html_entity_decode($msg);
		 
		if (smtpmailer($maintenance_email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) {
			echo '';
		} else {
			$error .= 'Error Sending Email to maintenance!<br/>';
		}
		
		if (smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) {
			echo '';
		} else {
			$error .= 'Error Sending Email to maintenance!<br/>';
		}
	
	}
	
	function sendhousekeeperacceptanceemail($id)
	{
	
	$sql = "SELECT * FROM ".GUESTS." WHERE md5(id) = '$id'"; //sortby
	$result= mysql_query($sql) or die(__LINE__.mysql_error());
	while($row = mysql_fetch_array($result)){
	
		$property = get_property_name($row['property_id']);
		$housekeepername = get_housekeeper_name($row['housekeeper']);
		$housekeeperemail = get_housekeeper_email($row['housekeeper']);
		$date_start = date("m/d/Y",strtotime($row['date_start']));
		$date_end = date("m/d/Y",strtotime($row['date_end']));
	
	}
	
	$emailbody = "Dear ".ADMIN_FIRSTNAME.",<br/>$housekeepername has accepted the housekeeper job for: <em>$property.</em><br/>";
	$emailbody .= "Arrival: $date_start<br/>Departure: $date_end<br/><br/>";
	
	$emailbody .= "<br/>Thanks.";
	
	get_email_template(8);
	
		if(smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], "Housekeeping Job Accepted", $emailbody)){
			echo '<font color="#006600">Thank you for accepting the job.</font>';
		} else {
			echo '<font color="#ff0000">Error sending email, please contact us at: '.ADMIN_EMAIL.'</font>';
		}
	
	}
	
	function sendhousekeeperdenyemail($id)
	{
		
	//echo 'Sending housekeeper email';
	$sql = "SELECT * FROM ".GUESTS." WHERE md5(id) = '$id'"; //sortby
	$result= mysql_query($sql) or die(__LINE__.mysql_error());
	while($row = mysql_fetch_array($result)){
	
		$property = get_property_name($row['property_id']);
		$housekeepername = get_housekeeper_name($row['housekeeper']);
		$housekeeperemail = get_housekeeper_email($row['housekeeper']);
		$date_start = date("m/d/Y",strtotime($row['date_start']));
		$date_end = date("m/d/Y",strtotime($row['date_end']));
	
	}
	
	$emailbody = "Dear ".ADMIN_FIRSTNAME.",<br/>$housekeepername has declined the housekeeper job for: <em>$property.</em><br/>";
	$emailbody .= "Arrival: $date_start<br/>Departure: $date_end<br/><br/>";
	
	$emailbody .= "<br/>Thanks.";
	
	get_email_template(8);
	
		if(smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], "Housekeeping Job Declined", $emailbody)){
			echo '<font color="#006600">Thank you for updating us about your unavailability.</font>';
		} else {
			echo '<font color="#ff0000">Error sending email, please contact us at: '.ADMIN_EMAIL.'</font>';
		}
	
	}
	
	function sendmaintenanceacceptanceemail($id)
	{
	
	$sql = "SELECT * FROM ".GUESTS." WHERE md5(id) = '$id'"; //sortby
	$result= mysql_query($sql) or die(__LINE__.mysql_error());
	while($row = mysql_fetch_array($result)){
	
		$property = get_property_name($row['property_id']);
		$maintenancename = get_maintenance_name($row['maintenance']);
		$maintenanceemail = get_maintenance_email($row['maintenance']);
		$date_start = date("m/d/Y",strtotime($row['date_start']));
		$date_end = date("m/d/Y",strtotime($row['date_end']));
	
	}
	
	$emailbody = "Dear ".ADMIN_FIRSTNAME.",<br/>$maintenancename has accepted the maintenance job for: <em>$property.</em><br/>";
	$emailbody .= "Arrival: $date_start<br/>Departure: $date_end<br/><br/>";
	
	$emailbody .= "<br/>Thanks.";
	
	get_email_template(9);
	
		if(smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], "Maintenance Job Accepted", $emailbody)){
			echo '<font color="#006600">Thank you for accepting the job.</font>';
		} else {
			echo '<font color="#ff0000">Error sending email, please contact us at: '.ADMIN_EMAIL.'</font>';
		}
	
	}
	
	function sendmaintenancedenyemail($id)
	{
		
	//echo 'Sending maintenance email';
	$sql = "SELECT * FROM ".GUESTS." WHERE md5(id) = '$id'"; //sortby
	$result= mysql_query($sql) or die(__LINE__.mysql_error());
	while($row = mysql_fetch_array($result)){
	
		$property = get_property_name($row['property_id']);
		$maintenancename = get_maintenance_name($row['maintenance']);
		$maintenanceemail = get_maintenance_email($row['maintenance']);
		$date_start = date("m/d/Y",strtotime($row['date_start']));
		$date_end = date("m/d/Y",strtotime($row['date_end']));
	
	}
	
	$emailbody = "Dear ".ADMIN_FIRSTNAME.",<br/>$maintenancename has declined the maintenance job for: <em>$property.</em><br/>";
	$emailbody .= "Arrival: $date_start<br/>Departure: $date_end<br/><br/>";
	
	$emailbody .= "<br/>Thanks.";
	
	get_email_template(9);
	
		if(smtpmailer(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], "Maintenance Job Declined", $emailbody)){
			echo '<font color="#006600">Thank you for updating us about your unavailability.</font>';
		} else {
			echo '<font color="#ff0000">Error sending email, please contact us at: '.ADMIN_EMAIL.'</font>';
		}
	
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	function insertGuestBackend()
	{ // done abrar
		//	echo "Booking...<br/>";

			$property_id	=	$_REQUEST['property_id'];

			$firstname	=	addslashes($_REQUEST['firstname']);
			$lastname	=	addslashes($_REQUEST['lastname']);
			$adults		=	$_REQUEST['adults'] ? addslashes($_REQUEST['adults']) : 0;
			$children	=	$_REQUEST['children'] ? addslashes($_REQUEST['children']) : 0;
			$pets	=	$_REQUEST['pets'] ? addslashes($_REQUEST['pets']) : 0;
			$address	=	addslashes($_REQUEST['address']);
			$property_id		=	addslashes($_REQUEST['property_id']);
			$sum_detail	=	addslashes($_REQUEST['sub_total_amount']);
			$total_rooms = 	$_POST['ifadditionalrooms'] ? $_POST['total_rooms'] : '1';
			$total_amount =	addslashes($_REQUEST['total_amount']);
			$total_amount = number_format($total_amount,2,'.','');
			$special_rate = $_POST['special_rate'] ? '1' : '0';
			$partialpayment = $_POST['partialpayment'] ? '1' : '0';
			$amount_paid = number_format($_REQUEST['amount_paid'],2,'.','');
			$percentage_paid = $_REQUEST['percentage_paid'];

			$amount_due = number_format($_REQUEST['amount_due'],2,'.','');

			$percentage_pending = $_REQUEST['percentage_pending'];
			$due_date_reminder = $_REQUEST['due_date_reminder'] ? '1' : '0';
			if($_REQUEST['paid_date']=='mm/dd/yyyy'){$paid_date = '0000-00-00';}else{$paid_date = date("Y-m-d",strtotime($_REQUEST['paid_date']));}
			if($_REQUEST['due_date']=='mm/dd/yyyy'){$due_date = '0000-00-00';}else{$due_date = date("Y-m-d",strtotime($_REQUEST['due_date']));}
			$final_payment_date = '0000-00-00';
			
			$state		=	addslashes($_REQUEST['state']);
			$zip		=	addslashes($_REQUEST['zip']);
			$city		=	addslashes($_REQUEST['city']);
			$email		=	addslashes($_REQUEST['email']);

			$phone		=	$_REQUEST['phone1']."-".$_REQUEST['phone2']."-".$_REQUEST['phone3'];
			$cell		=	$_REQUEST['cell1']."-".$_REQUEST['cell2']."-".$_REQUEST['cell3'];
			$cell_2		=	$_REQUEST['cell_21']."-".$_REQUEST['cell_22']."-".$_REQUEST['cell_23'];
			$phone_business		=	$_REQUEST['phone_business1']."-".$_REQUEST['phone_business2']."-".$_REQUEST['phone_business3'];
			$phone_notes		=	$_REQUEST['phone_notes'];
			$cell_notes		=	$_REQUEST['cell_notes'];
			$cell_2_notes		=	$_REQUEST['cell_2_notes'];
			$phone_business_notes		=	$_REQUEST['phone_business_notes'];

			$notes		=	addslashes($_REQUEST['notes']);
			$status		=	addslashes($_REQUEST['status']);
			
			$housekeeper		=	addslashes($_REQUEST['housekeeper']);
			
			$maintenance		=	addslashes($_REQUEST['maintenance']);

			$date_start = date("Y-m-d",strtotime($_REQUEST['date_start']));
			$date_end = date("Y-m-d",strtotime($_REQUEST['date_end']));
			
			$parent_guest_id = 0;
		
			$sql		=	"INSERT INTO ".GUESTS."(parent_guest_id,firstname,lastname,adults,children,pets,address,state,zip,city,email,phone,cell,cell_2,phone_business,phone_notes,cell_notes,cell_2_notes,phone_business_notes,date_start,date_end,property_id,sum_detail,total_amount,special_rate,partialpayment,amount_paid,percentage_paid,paid_date,amount_due,due_date,percentage_pending,due_date_reminder,final_payment_date,housekeeper,maintenance,notes,status,created) VALUES('$parent_guest_id','$firstname','$lastname','$adults','$children','$pets','$address','$state','$zip','$city','$email','$phone','$cell','$cell_2','$phone_business','$phone_notes','$cell_notes','$cell_2_notes','$phone_business_notes','$date_start','$date_end','$property_id','$sum_detail','$total_amount','$special_rate','$partialpayment','$amount_paid','$percentage_paid','$paid_date','$amount_due','$due_date','$percentage_pending','$due_date_reminder','$final_payment_date','$housekeeper','$maintenance','$notes','$status',NOW())";
			mysql_query($sql) or die(__LINE__.mysql_error());
			$guest_id = $parent_guest_id = mysql_insert_id();
			
			$GLOBALS['insert_guest_id'] = $guest_id;
			
			$message .= "Guest info saved. ID# ".$guest_id."<br/>";

				$check_date = $date_start;				
				$check_date = date ("Y-m-d", strtotime ($check_date));
				$while = '0';
				while ($check_date != $date_end)
				{

					$sql		=	"INSERT INTO ".CALENDAR."(property_id,guest_id,date,status) VALUES('$property_id','$guest_id','$check_date','$status')";
					$res		=	mysql_query($sql) or die(__LINE__.mysql_error());
					$message 	.= "Booked: ".date("m/d/Y", strtotime($check_date))."<br/>";
					$check_date = date ("Y-m-d", strtotime ("+1 day", strtotime($check_date)));
					$while++;
					if($while>1000) break;
				}
				
			if($status=='B') $status = 'Booked'; elseif($status=='P') $status = 'Pending';
			
			if(!isset($_POST['do_not_send_email'])){
			sendpendingemail($email,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property);
			
			//done
			}

				return $message;

			}
		
	function updateGuest()
	{ // done abrar
		//	echo "Booking...<br/>";
		//print_r($_POST);
			$error = "";
			$guest_id		=	$GLOBALS['insert_guest_id'] = $_REQUEST['guest_id'];

			$firstname	=	$_REQUEST['firstname'];
			if(empty($firstname)) $error .= "Please enter your first name";
			$lastname	=	$_REQUEST['lastname'];
			if(empty($lastname)) $error .= "Please enter your last name";
			$adults		=	$_REQUEST['adults'];
			$children	=	$_REQUEST['children'];
			$pets	=	$_REQUEST['pets'];
			$address	=	$_REQUEST['address'];
			$property_id	=	$_REQUEST['property_id'];
			$sum_detail	=	$_REQUEST['sum_detail'];
			$total_amount	=	$_REQUEST['total_amount'];
			$special_rate = $_POST['special_rate'] ? '1' : '0';
			$partialpayment = $_POST['partialpayment'] ? '1' : '0';
			$amount_paid = number_format($_REQUEST['amount_paid'],2,'.','');
			$percentage_paid = $_REQUEST['percentage_paid'];
			$amount_due = $_REQUEST['amount_due'];
			if($amount_due == '0' OR $amount_due == ''){$amount_due = $total_amount - $amount_paid;}
			$amount_due = number_format($amount_due,2,'.','');
			$percentage_pending = $_REQUEST['percentage_pending'];
			$due_date_reminder = $_REQUEST['due_date_reminder'] ? '1' : '0';
			if($_REQUEST['paid_date']=='mm/dd/yyyy'){$paid_date = '0000-00-00';}else{$paid_date = date("Y-m-d",strtotime($_REQUEST['paid_date']));}
			if($_REQUEST['due_date']=='mm/dd/yyyy'){$due_date = '0000-00-00';}else{$due_date = date("Y-m-d",strtotime($_REQUEST['due_date']));}
			
			if(!isset($_POST['final_payment_received'])){$final_payment_date = '0000-00-00';}
			if($_REQUEST['final_payment_date']<>'mm/dd/yyyy'){$final_payment_date = date("Y-m-d",strtotime($_REQUEST['final_payment_date']));}
			
			$final_payment_received = $_POST['final_payment_received'] ? '1' : '0';

			$state		=	$_REQUEST['state'];
			$zip		=	$_REQUEST['zip'];
			$city		=	$_REQUEST['city'];
			$email		=	$_REQUEST['email'];
			$phone		=	$_REQUEST['phone1']."-".$_REQUEST['phone2']."-".$_REQUEST['phone3'];
			$phone_notes		=	$_REQUEST['phone_notes'];
			$cell_notes		=	$_REQUEST['cell_notes'];
			$cell_2_notes		=	$_REQUEST['cell_2_notes'];
			$phone_business_notes		=	$_REQUEST['phone_business_notes'];
			$cell		=	$_REQUEST['cell1']."-".$_REQUEST['cell2']."-".$_REQUEST['cell3'];
			$cell_2		=	$_REQUEST['cell_21']."-".$_REQUEST['cell_22']."-".$_REQUEST['cell_23'];
			$phone_business		=	$_REQUEST['phone_business1']."-".$_REQUEST['phone_business2']."-".$_REQUEST['phone_business3'];
			$housekeeper		=	$_REQUEST['housekeeper'];
			$maintenance		=	$_REQUEST['maintenance'];
			$notes		=	$_REQUEST['notes'];
			$status		=	$_REQUEST['status'];
			if($status<>"B" AND $status<>"P" AND $status<>"N"){
			$status = "Z";
			$error .= "Please select a status";
			}

			$date_start		=	$_REQUEST['date_start'] ? date("Y-m-d",strtotime($_REQUEST['date_start'])) : '0000-00-00';
			$date_end		=	$_REQUEST['date_end'] ? date("Y-m-d",strtotime($_REQUEST['date_end'])) : '0000-00-00';

			if(empty($error)){
					
			$sql		=	"UPDATE  ".GUESTS." SET firstname = '$firstname',lastname='$lastname',adults='$adults',children='$children',pets='$pets',address='$address',state='$state',zip='$zip',city='$city',email='$email',phone='$phone',cell='$cell',cell_2='$cell_2',phone_business='$phone_business',phone_notes='$phone_notes',cell_notes='$cell_notes',cell_2_notes='$cell_2_notes',phone_business_notes='$phone_business_notes',date_start='$date_start',date_end='$date_end',property_id='$property_id',sum_detail='$sum_detail',total_amount='$total_amount',special_rate='$special_rate',partialpayment='$partialpayment',amount_paid='$amount_paid',percentage_paid='$percentage_paid',paid_date='$paid_date',amount_due='$amount_due',due_date='$due_date',percentage_pending='$percentage_pending',due_date_reminder='$due_date_reminder',final_payment_date='$final_payment_date',final_payment_received='$final_payment_received',notes='$notes',status='$status' WHERE id = '$guest_id'";
			mysql_query($sql) or die(__LINE__.mysql_error());

			$sql_payments		=	"UPDATE ".PAYMENTS." SET date_start = '$date_start', date_end='$date_end' WHERE guest_id = '$guest'";

			$res_payments		=	mysql_query($sql_payments) or die(__LINE__.mysql_error());
			
			$GLOBALS['message'] = "Guest information saved.  ID# ".$guest_id."<br/>";
			
			mysql_query("DELETE FROM ".CALENDAR." WHERE guest_id='".$guest_id."'") or die(__LINE__.mysql_error());

				$check_date = $date_start;
				
				$check_date = date ("Y-m-d", strtotime ($check_date));
				
				while ($check_date != $date_end)
				{
					$sql		=	"INSERT INTO ".CALENDAR."(property_id,guest_id,date,status) VALUES ('$property_id','$guest_id','$check_date','$status')";
					mysql_query($sql) or die(__LINE__.mysql_error());
					$check_date = date ("Y-m-d", strtotime ("+1 day", strtotime($check_date)));
				}
				
				if(!isset($_POST['dont_send_email'])){
				if(isset($_POST['send_booked_email_50'])){
				sendbookedemail50($email,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$status,$property_id);
				}
				if(isset($_POST['send_booked_email_100'])){
				sendbookedemail100($email,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$status,$property_id);
				}
				if(isset($_POST['send_deposit_email'])){
				if($due_date=='0000-00-00'){$due_date = date('m/d/Y',strtotime("-3 days",strtotime($date_end)));}
				senddepositemail($email,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property_id,$amount_paid,$amount_due,$due_date);
				}
				if(isset($_POST['final_payment_received'])){
				sendfinalemail($email,$firstname,$lastname,$adults,$children,$pets,$amount_due,$sum_detail,$total_amount,$date_start,$date_end,$property_id);
				sendowneremail($owner,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property_id);
				sendhousekeeperemail($housekeeper,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property_id);
				sendmaintenanceemail($maintenance,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property_id);
				}
				}
			unset($_POST);
			unset($_GET);
			return true;	
		}else{
			return $error;
		}	
	}

	function insertGuestBackendwithCC(){

	$GLOBALS['message'] .= "Booking and transacting...<br/>";

		extract($_REQUEST);
					
			$type="sale";
			
						if($amountbilled=="half"){
						$amountnow=$_REQUEST['ccamount']*50/100;
						}else{
						$amountnow=$_REQUEST['ccamount'];
						}
						
						$amountnow = number_format($amountnow,2,'.',',');

			$ccexp = $ccexp.$ccexp2;

			//set POST variables
			$url = 'https://secure.rabankcardgateway.com/api/transact.php';
			//$url = 'https://secure.nmi.com/api/transact.php';
			
			$ccfirstname=addslashes($ccfirstname);
			$cclastname=addslashes($cclastname);
			$ccaddress1=addslashes($ccaddress1);
			$ccaddress2=addslashes($ccaddress2);
			$customer_vault_id=time();

			$fields = array(
				'username'=>urlencode($username),
				'password'=>urlencode($password),
				'type'=>$type,
				'ccnumber'=>urlencode($ccnumber),
				'ccexp'=>urlencode($ccexp),
				'cvv'=>urlencode($cvv),
				'amount'=>urlencode($amountnow),
				'firstname'=>urlencode($ccfirstname),
				'lastname'=>urlencode($cclastname),
				'address1'=>urlencode($ccaddress1),
				'address2'=>urlencode($ccaddress2),
				//'city'=>urlencode($cccity),
				//'state'=>urlencode($ccstate),
				//'country'=>urlencode($cccountry),
				'customer_vault'=>urlencode('add_customer'),
				'customer_vault_id'=>urlencode($customer_vault_id)
			 );

			
			
			
			
			//url-ify the data for the POST
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string,'&');
			
			//open connection
			$ch = curl_init();
			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_POST,count($fields));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
			
			//execute post
			$result = curl_exec($ch);	
			
			curl_close($ch);
			
			$xtx=explode("&",$result);
			
			$str2=explode("=",$xtx[0]);
			$str3=$str2[1];
			$str=explode("=",$xtx[3]);
			$str1=$str[1];
			
			
			
			
			//print_r ($xtx);			echo "<br/>";.
			
			$transactionstatus = "Pending";
			
			if($str3=="1"){
				$str3='A';
				$transactionstatus = "Paid";
				sendsuccessemail($email,$firstname,$lastname,$amount,$date_start,$date_end,$property_id);
			}else{
				$str3='P';
				$transactionstatus = "Error";
				senderroremail($email,$firstname,$lastname,$amount,$date_start,$date_end,$property_id);
			
			$GLOBALS['message'] .= "Error: Transaction could not proceed.<br/>";
				echo "<br/>";
				echo "<br/>";
				echo "<br/>";
			//print_r ($xtx);
			
			}
			
			///////////////////////////////////////////////
			
			if ($str3 == "A"){

			$firstname	=	$firstname;
			$lastname	=	$lastname;
			$adults		=	$adults;
			$children	=	$children;
			$address	=	$_REQUEST['address'];//$_REQUEST['address1']."\n".$_REQUEST['address2'];
			
			$property_id		=	$_REQUEST['property_id'];
			
			
			$sum_detail		=	$_REQUEST['sub_total_amount'];
			$amount		=	$_REQUEST['ccamount'];
			$amount		=	number_format($amount,2,'.',',');
			if(isset($_REQUEST['pet'])) {$pet = "yes";}else{$pet = "no";}
			$state		=	$_REQUEST['state'];
			$zip		=	$_REQUEST['zip'];
			$city		=	$_REQUEST['city'];
			$email		=	$_REQUEST['email'];
			$phone		=	$_REQUEST['phone1']."-".$_REQUEST['phone2']."-".$_REQUEST['phone3'];
			$cell		=	$_REQUEST['cell1']."-".$_REQUEST['cell2']."-".$_REQUEST['cell3'];
			$housekeeper		=	$_REQUEST['housekeeper'];
			$notes		=	$_REQUEST['notes'];
			$status		=	$_REQUEST['status'];

			$date_start		=	$date_start;
			$date_start		=	explode("/",$date_start);
			$date_start		=	$date_start[2]."-".$date_start[0]."-".$date_start[1];
			$date_end			=	$date_end;
			$date_end			=	explode("/",$date_end);
			$date_end			=	$date_end[2]."-".$date_end[0]."-".$date_end[1];
			//echo $date_start;
			
		
			$sql		=	"INSERT INTO ".GUESTS."(firstname,lastname,adults,children,address,state,zip,city,email,phone,cell,date_start,date_end,property_id,sum_detail,total_amount,housekeeper,notes,status,created) VALUES('$firstname','$lastname','$adults','$children','$address','$state','$zip','$city','$email','$phone','$cell','$date_start','$date_end','$property_id','$sum_detail','$amount','$housekeeper','$notes','$status',NOW())";
			$res		=	mysql_query($sql) or die(mysql_error());
			$guest_id = mysql_insert_id();
			
			$GLOBALS['message'] .= "Guest information has been saved.  ID# ".$guest_id."<br/>";
			
			$GLOBALS['insert_guest_id'] = $guest_id;

				$check_date = $date_start;
				
				$check_date = date ("Y-m-d", strtotime ($check_date));
				
				while ($check_date != $date_end)
				{
				
				
				//echo $check_date."<br/>";
					$sql		=	"INSERT INTO ".CALENDAR."(property_id,guest_id,date,status) VALUES('$property_id','$guest_id','$check_date','B')";
					$res		=	mysql_query($sql) or die(mysql_error());
			
				$check_date = date ("Y-m-d", strtotime ("+1 day", strtotime($check_date)));
				
				$GLOBALS['message'] .= "Booked: ".date("m/d/Y", strtotime($check_date))."<br/>";
			
				}

			
			$sql		=	"INSERT INTO ".PAYMENTS."(guest_id,firstname,lastname,address1,address2,amount,billed,card_type,cvv,check_ref_num,lodging_charge,special_rate,tax,lodger_tax,other_fees,transaction_id,customer_vault_id,status,cronjob,type,transactionstatus,date_start,date_end,paid_date) VALUES('$guest_id','$ccfirstname','$cclastname','$ccaddress1','$ccaddress2','$amountnow','$amountbilled','$card_type','$cvv','$check_ref_num','$lodging_charge','$special_rate','$tax','$lodger_tax','$other_fees','$str1','$customer_vault_id','$str3','$GLOBALS[cronjobdays]','$type','$transactionstatus','$date_start','$date_end',CURDATE())";
			$res		=	mysql_query($sql) or die(mysql_error());
			
			
			} // insertGuestInfo // insertPaymentInfo
			///////////////////////////////////////////////
			
			
			if($str3=="A"){
			
			if($amountbilled=="half"){


			$transactionstatus = "Pending";
			$type = "vault";
			$str1 = "";
			$str3 = "";

			$sql = "INSERT INTO ".PAYMENTS."(guest_id,firstname,lastname,address1,address2,amount,billed,card_type,cvv,check_ref_num,lodging_charge,special_rate,tax,lodger_tax,other_fees,transaction_id,customer_vault_id,status,cronjob,type,transactionstatus,date_start,date_end) VALUES('$guest_id','$ccfirstname','$cclastname','$ccaddress1','$ccaddress2','$amountnow','$amountbilled','$card_type','$cvv','$check_ref_num','$lodging_charge','$special_rate','$tax','$lodger_tax','$other_fees','$str1','$customer_vault_id','$str3','$GLOBALS[cronjobdays]','$type','$transactionstatus','$date_start','$date_end')";

			mysql_query($sql) or die(mysql_error());
			$GLOBALS['message'] .= "\$".$amountnow." will be charged later from vault.<br/>";
			

			}

				sendsuccessemail($email,$firstname,$lastname,$amount,$date_start,$date_end,$property_id);
			
			
			}

		
	}

	function ProcessCreditCard(){

	$error=="";
	if(empty($_REQUEST['ccfirstname'])){
	$error .= "Please enter first name on credit card.<br/>";
	}
	if(empty($_REQUEST['cclastname'])){
	$error .= "Please enter last name on credit card.<br/>";
	}
	if(empty($_REQUEST['ccnumber'])){
	$error .= "Please enter card number.<br/>";
	}
	if(empty($_REQUEST['ccamount'])){
	$error .= "Please enter the amount to be charged.<br/>";
	}
	if($error!=""){
	$GLOBALS['message'] .= $error;
	}else{
	
	$GLOBALS['message'] .= "Transacting...<br/>";
	
		extract($_REQUEST);
					
			$type="sale";
			$guest_id = $_REQUEST['guest_id'];
			
						if($amountbilled=="half"){
						$amountnow=$_REQUEST['ccamount']*50/100;
						}else{
						$amountnow=$_REQUEST['ccamount'];
						}

			$ccexp = $ccexp.$ccexp2;

			//set POST variables
			$url = 'https://secure.rabankcardgateway.com/api/transact.php';
			//$url = 'https://secure.nmi.com/api/transact.php';
			
			$ccfirstname=addslashes($ccfirstname);
			$cclastname=addslashes($cclastname);
			$ccaddress1=addslashes($ccaddress1);
			$ccaddress2=addslashes($ccaddress2);
			$customer_vault_id=time();

			$fields = array(
				'username'=>urlencode($username),
				'password'=>urlencode($password),
				'type'=>$type,
				'ccnumber'=>urlencode($ccnumber),
				'ccexp'=>urlencode($ccexp),
				'cvv'=>urlencode($cvv),
				'amount'=>urlencode($amountnow),
				'firstname'=>urlencode($ccfirstname),
				'lastname'=>urlencode($cclastname),
				'address1'=>urlencode($ccaddress1),
				'address2'=>urlencode($ccaddress2),
				//'city'=>urlencode($city),
				//'state'=>urlencode($state),
				//'country'=>urlencode($country),
				'customer_vault'=>urlencode('add_customer'),
				'customer_vault_id'=>urlencode($customer_vault_id)
			 );

			
			
			
			
			//url-ify the data for the POST
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string,'&');
			
			//open connection
			$ch = curl_init();
			
			//set the url, number of POST vars, POST data
			curl_setopt($ch,CURLOPT_URL,$url);
			curl_setopt($ch,CURLOPT_POST,count($fields));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
			curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
			
			//execute post
			$result = curl_exec($ch);	
			
			curl_close($ch);
			
			$xtx=explode("&",$result);
			
			$str2=explode("=",$xtx[0]);
			$str3=$str2[1];
			$str=explode("=",$xtx[3]);
			$str1=$str[1];
			
			
			
			
			//print_r ($xtx);			echo "<br/>";
			
			$transactionstatus = "Pending";
			
			if($str3=="1"){
				$str3='A';
				$transactionstatus = "Paid";
				//sendsuccessemail($email,$firstname,$lastname,$amount,$date_start,$date_end,$property_id);
			}else{
				$str3='P';
				$transactionstatus = "Error";
				//senderroremail($email,$firstname,$lastname,$amount,$date_start,$date_end,$property_id);
			}
			
			///////////////////////////////////////////////


			$sql		=	"INSERT INTO ".PAYMENTS."(guest_id,firstname,lastname,address1,address2,amount,billed,card_type,cvv,check_ref_num,lodging_charge,special_rate,tax,lodger_tax,other_fees,transaction_id,customer_vault_id,status,cronjob,type,transactionstatus,date_start,date_end,paid_date) VALUES('$guest_id','$ccfirstname','$cclastname','$ccaddress1','ccaddress2','$amountnow','$amountbilled','$card_type','$cvv','$check_ref_num','$lodging_charge','$special_rate','$tax','$lodger_tax','$other_fees','$str1','$customer_vault_id','$str3','$GLOBALS[cronjobdays]','$type','$transactionstatus','$ccdate_start','$ccdate_end',CURDATE())";

			
			
			mysql_query($sql) or die(mysql_error());
			$GLOBALS['message'] .= "\$".$amountnow." has been paid.<br/>";
			
			if($amountbilled=="full"){unset($_REQUEST);}
			
			if($str3=="A"){
			
		
			if($amountbilled=="half"){

			$transactionstatus = "Pending";
			$type = "vault";
			$str1 = "";
			$str3 = "";

			$sql = "INSERT INTO ".PAYMENTS."(guest_id,firstname,lastname,address1,address2,amount,billed,card_type,cvv,check_ref_num,lodging_charge,special_rate,tax,lodger_tax,other_fees,transaction_id,customer_vault_id,status,cronjob,type,transactionstatus,date_start,date_end) VALUES('$guest_id','$ccfirstname','$cclastname','$ccaddress1','$ccaddress2','$amountnow','$amountbilled','$card_type','$cvv','$check_ref_num','$lodging_charge','$special_rate','$tax','$lodger_tax','$other_fees','$str1','$customer_vault_id','$str3','$GLOBALS[cronjobdays]','$type','$transactionstatus','$date_start','$date_end')";

			mysql_query($sql) or die(mysql_error());
			$GLOBALS['message'] .= "\$".$amountnow." will be charged later from vault.<br/>";
			
			unset($_REQUEST);




			}
			
			}
			
		} // fields validation
		
		
	}

	function insertGuestFrontend()
	{ // done abrar
		//	echo "Booking...<br/>";
		//print_r($_POST);
		
		if($amountbilled=="half"){
		$amountnow=$_REQUEST['total_amount']*50/100;
		}else{
		$amountnow=$_REQUEST['total_amount'];
		}
		$amountnow = number_format($amountnow,2,'.','');
		//////////////////////////////////////////////////////////////////////////
		/*
		$post_url = "https://test.authorize.net/gateway/transact.dll";

		$post_values = array(
			
			// the API Login ID and Transaction Key must be replaced with valid values
			"x_login"			=> "7gVyv26Mg",
			"x_tran_key"		=> "22GY959zMjf3g854",
		
			"x_version"			=> "3.1",
			"x_delim_data"		=> "TRUE",
			"x_delim_char"		=> "|",
			"x_relay_response"	=> "FALSE",
		
			"x_type"			=> "AUTH_CAPTURE",
			"x_method"			=> "CC",
			"x_card_num"		=> $_REQUEST['ccnumber'],
			"x_exp_date"		=> $_REQUEST['ccexpM'].$_REQUEST['ccexpY'],
		
			"x_amount"			=> $amountnow,
			"x_description"		=> "Guest Reservation",
		
			"x_first_name"		=> $_REQUEST['ccfirstname'],
			"x_last_name"		=> $_REQUEST['cclastname'],
			"x_address"			=> $_REQUEST['ccaddress1'],
			"x_state"			=> $_REQUEST['ccstate'],
			"x_zip"				=> $_REQUEST['cczip']
			// Additional fields can be added here as outlined in the AIM integration
			// guide at: http://developer.authorize.net
		);
		$post_string = "";
		foreach( $post_values as $key => $value )
			{ $post_string .= "$key=" . urlencode( $value ) . "&"; }
		$post_string = rtrim( $post_string, "& " );
		$request = curl_init($post_url); // initiate curl object
			curl_setopt($request, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
			curl_setopt($request, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
			curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
			curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE); // uncomment this line if you get no gateway response.
			$post_response = curl_exec($request); // execute curl post and store results in $post_response
			// additional options may be required depending upon your server configuration
			// you can find documentation on curl options at http://www.php.net/curl_setopt
		curl_close ($request); // close curl object
		
		// This line takes the response and breaks it into an array using the specified delimiting character
		$response_array = explode($post_values["x_delim_char"],$post_response);
		
		/**/
		
		
		//////////////////////////////////////////////////////////////////////////

			$property_id	=	$_POST['property_id'];

			$firstname	=	addslashes($_POST['firstname']);
			$lastname	=	addslashes($_POST['lastname']);
			$adults		=	addslashes($_POST['adults']);
			$children	=	addslashes($_POST['children']);
			$pets		=	addslashes($_POST['pets']);
			$address1	=	addslashes($_POST['address1']);
			$address2	=	addslashes($_POST['address2']);
			$address	=	$address1.'\n'.$address2;

			$sum_detail	=	addslashes($_POST['sub_total_amount']);
			$total_rooms = 	$_POST['ifadditionalrooms'] ? $_POST['total_rooms'] : '1';
			$total_amount =	addslashes($_POST['total_amount']);
			$total_amount = number_format($total_amount,2,'.','');
			$state		=	addslashes($_POST['state']);
			$zip		=	addslashes($_POST['zip']);
			$city		=	addslashes($_POST['city']);
			$email		=	addslashes($_POST['email']);
			$phone		=	$_POST['phone1']."-".$_POST['phone2']."-".$_POST['phone3'];
			$cell		=	$_POST['cell1']."-".$_POST['cell2']."-".$_POST['cell3'];
			$notes		=	addslashes($_POST['notes']);
			
			$housekeeper = get_property_housekeeper($property_id);
			$maintenance = get_property_maintenance($property_id);
			
			$status_guest		=	"Z";
			$status_calendar	=	"P";

			$date_start		=	date("Y-m-d",strtotime($_POST['date_start']));
			$date_end		=	date("Y-m-d",strtotime($_POST['date_end']));
/*
///
		$profile_id = doCustomerProfile(); //
		$GLOBALS['success_doCustomerProfile'];
		if($GLOBALS['success_doCustomerProfile']=="I00001"){
		$payment_profile_id = doCustomerPaymentProfileCC($profile_id); //
		}else{echo ("<font color='#ff0000'><br /><br /><br /><br />".$GLOBALS['message_doCustomerProfile']."<br /><br /><br /><br /></font>");}

		if($GLOBALS['success_doCustomerPaymentProfileCC']=="I00001"){
		//$transaction_id = doCustomerProfileTransactionAuthCapture($profile_id,$payment_profile_id,$amountnow,$cvv); //
		}else{echo ("<font color='#ff0000'><br /><br /><br /><br />".$GLOBALS['message_doCustomerPaymentProfileCC']."<br /><br /><br /><br /></font>");}
		
		$transactionstatus = "Pending";
		if(isset($profile_id) AND isset($payment_profile_id)){ // AND isset($transaction_id)
///
*/
		
			$sql		=	"INSERT INTO ".GUESTS."(firstname,lastname,adults,children,pets,address,state,zip,city,email,phone,cell,date_start,date_end,property_id,sum_detail,total_amount,housekeeper,maintenance,notes,status,created) VALUES('$firstname','$lastname','$adults','$children','$pets','$address','$state','$zip','$city','$email','$phone','$cell','$date_start','$date_end','$property_id','$sum_detail','$total_amount','$housekeeper','$maintenance','$notes','$status_guest',NOW())";
			mysql_query($sql) or die(__LINE__.mysql_error());
			$guest_id = mysql_insert_id();
						
			$message .= "Guest info saved. ID# ".$guest_id."<br/>";

				$check_date = $date_start;				
				$check_date = date ("Y-m-d", strtotime ($check_date));
				while ($check_date != $date_end)
				{

					$sql		=	"INSERT INTO ".CALENDAR."(property_id,guest_id,date,status) VALUES('$property_id','$guest_id','$check_date','$status_calendar')";
					$res		=	mysql_query($sql) or die(__LINE__.mysql_error());
					$message .= "Booked: ".date("m/d/Y", strtotime($check_date))."<br/>";
					$check_date = date ("Y-m-d", strtotime ("+1 day", strtotime($check_date)));
			
				}
				//////////////
					//$transactionstatus = "Paid";
				$type = "sale";
				$transaction_id = "";
				/*
				$sql		=	"INSERT INTO ".PAYMENTS."(guest_id,firstname,lastname,address1,address2,amount,billed,card_type,cvv,check_ref_num,lodging_charge,special_rate,tax,lodger_tax,other_fees,transaction_id,customer_profile_id,customer_payment_profile_id,status,cronjob,type,transaction_status,date_start,date_end,paid_date) VALUES('$guest_id','$ccfirstname','$cclastname','$ccaddress1','$ccaddress2','$amountnow','$amountbilled','$card_type','$cvv','$check_ref_num','$lodging_charge','$special_rate','$tax','$lodger_tax','$other_fees','$transaction_id','$profile_id','$payment_profile_id','','$GLOBALS[cronjobdays]','$type','$transactionstatus','$date_start','$date_end',CURDATE())";
				mysql_query($sql) or die(__LINE__.mysql_error());
				///////////////
				if($amountbilled=="half"){
				$transactionstatus = "Pending";
				$type = "vault";
				$transaction_id = "";
				
				$sql = "INSERT INTO ".PAYMENTS."(guest_id,firstname,lastname,address1,address2,amount,billed,card_type,cvv,check_ref_num,lodging_charge,special_rate,tax,lodger_tax,other_fees,transaction_id,customer_vault_id,status,cronjob,type,transaction_status,date_start,date_end) VALUES('$guest_id','$ccfirstname','$cclastname','$ccaddress1','$ccaddress2','$amountnow','$amountbilled','$card_type','$cvv','$check_ref_num','$lodging_charge','$special_rate','$tax','$lodger_tax','$other_fees','$transaction_id','$customer_vault_id','','$GLOBALS[cronjobdays]','$type','$transactionstatus','$date_start','$date_end')";
				
				mysql_query($sql) or die(mysql_error());
				$message .= "\$".$amountnow." will be charged later from vault.<br/>";
				}
				*/
				//sendpendingemail($email,$firstname,$lastname,$total_amount,$date_start,$date_end,$property_id);
				sendpendingemail($email,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property);
				
				//return $message; //abrar
				
				//return "Thank you for your reservation from ".date("m/d/Y",strtotime($date_start))." to ".date("m/d/Y",strtotime($date_end)).". Please check your email for confirmation details.";
				
				return "Your reservation request has been received.  We will contact you within 24 hrs to confirm your reservation and give you payment information."; 

			/*}else{
				return false;
			}*/
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
	function checkAvailable()
	{ // done abrar
		
		$error = "";
		
		if($_REQUEST['date_start']=="mm/dd/yyyy" OR $_REQUEST['date_end']=="mm/dd/yyyy")
		{
			$error .= "Start Date or End Date is missing\n";
		}

		$daysDifference = daysDifference($_REQUEST['date_start'], $_REQUEST['date_end']);
		if($daysDifference<MINIMUM_STAY AND !isset($_SESSION['admin_loggedin']))
		{
			$error .= "We have a ".MINIMUM_STAY." nights minimum, please select atleast ".MINIMUM_STAY." nights stay\n";
		}

		if(empty($error)){
		
				if(isset($_POST['property_id']) AND !empty($_POST['property_id'])){$property_id=$_POST['property_id'];}elseif(isset($_GET['property_id']) AND !empty($_GET['property_id'])){$property_id=$_GET['property_id'];}
				$date_start		=	date("Y-m-d",strtotime($_REQUEST['date_start']));
				$date_end		=	date("Y-m-d",strtotime($_REQUEST['date_end']));
	
				$check_date = $date_start;
		
				while ($check_date != $date_end)
				{
					if(checkCalendar($check_date,$property_id)===true) //true
					{
						$error .= "No space available for: ".date("m/d/Y", strtotime($check_date))."<br/>";
					}
					$check_date = date("Y-m-d", strtotime("+1 day", strtotime($check_date)));
				}
		}
	
	if(empty($error)) {return true;}else{return $error;}
	
	}
			
	function checkAvailableCount()
	{ // done abrar
		
		$error = "";
		
		if($_REQUEST['date_start']=="mm/dd/yyyy" OR $_REQUEST['date_end']=="mm/dd/yyyy")
		{
			$error .= "Start Date or End Date is missing\n";
		}

		$daysDifference = daysDifference($_REQUEST['date_start'], $_REQUEST['date_end']);
		if($daysDifference<MINIMUM_STAY AND !isset($_SESSION['admin_loggedin']))
		{
			$error .= "We have a ".MINIMUM_STAY." nights minimum, please select atleast ".MINIMUM_STAY." nights stay\n";
		}

		if(empty($error)){
		
				if(isset($_REQUEST['property_id'])){$property_id=$_REQUEST['property_id'];}elseif(isset($_POST['property_id'])){$property_id=$_POST['property_id'];}
				$date_start		=	date("Y-m-d",strtotime($_REQUEST['date_start']));
				$date_end		=	date("Y-m-d",strtotime($_REQUEST['date_end']));
			
			$result_extensions= mysql_query("SELECT * FROM ".PROPERTIESEXTENSIONS." WHERE status = '1' AND property_id = '$property_id'");
			$num_of_property_extensions_booked = mysql_num_rows($result_extensions);
			while($row_extensions = mysql_fetch_array($result_extensions)){
		
					$check_date = $date_start;
			
					while ($check_date != $date_end)
					{
						if(checkCalendarForExtension($check_date,$row_extensions['id'])===true) //true
						{
							$booked = "No space available for: ".date("m/d/Y", strtotime($check_date))."<br/>";
						}
						$check_date = date("Y-m-d", strtotime("+1 day", strtotime($check_date)));
					}
					if(isset($booked)){$num_of_property_extensions_booked--;}
					unset($booked);
			
			}
			
			
			
		}
	
	if(empty($error)) {return $num_of_property_extensions_booked;}else{return $error;}
	
	}
			
	function checkAvailableForExtension($extension_id)
	{ // done abrar
	
	$error = "";

		if($_REQUEST['date_start']=="mm/dd/yyyy" OR $_REQUEST['date_end']=="mm/dd/yyyy")
		{
			$error .= "Start Date or End Date is missing\n";
		}

		$daysDifference = daysDifference($_REQUEST['date_start'], $_REQUEST['date_end']);
		if($daysDifference<MINIMUM_STAY AND !isset($_SESSION['admin_loggedin']))
		{
			$error .= "We have a ".MINIMUM_STAY." nights minimum, please select atleast ".MINIMUM_STAY." nights stay\n";
		}

		if(empty($error)){
		
				$date_start		=	date("Y-m-d",strtotime($_REQUEST['date_start']));
				$date_end		=	date("Y-m-d",strtotime($_REQUEST['date_end']));
		
				$check_date = $date_start;
		
				while ($check_date != $date_end)
				{
					if(checkCalendarForExtension($check_date,$extension_id)===true) //true
					{
						$error .= "No space available for: ".date("m/d/Y", strtotime($check_date))."<br/>";
					}
					$check_date = date("Y-m-d", strtotime("+1 day", strtotime($check_date)));
				}
		}
	
	if(empty($error)) {return true;}else{return $error;}
	
	}
			
	function checkAvailableForExtensionWithGuest($extension_id,$guest_id)
	{ // done abrar
	
	$error = "";

		if($_REQUEST['date_start']=="mm/dd/yyyy" OR $_REQUEST['date_end']=="mm/dd/yyyy")
		{
			$error .= "Start Date or End Date is missing\n";
		}

		$daysDifference = daysDifference($_REQUEST['date_start'], $_REQUEST['date_end']);
		if($daysDifference<MINIMUM_STAY AND !isset($_SESSION['admin_loggedin']))
		{
			$error .= "We have a ".MINIMUM_STAY." nights minimum, please select atleast ".MINIMUM_STAY." nights stay\n";
		}

		if(empty($error)){
		
				$date_start		=	date("Y-m-d",strtotime($_REQUEST['date_start']));
				$date_end		=	date("Y-m-d",strtotime($_REQUEST['date_end']));
		
				$check_date = $date_start;
		
				while ($check_date != $date_end)
				{
					if(checkCalendarForExtensionWithGuest($check_date,$extension_id,$guest_id)===true) //true
					{
						$error .= "No space available for: ".date("m/d/Y", strtotime($check_date))."<br/>";
					}
					$check_date = date("Y-m-d", strtotime("+1 day", strtotime($check_date)));
				}
		}
	
	if(empty($error)) {return true;}else{return $error;}
	
	}

	function checkCalendar($check_date,$property_id)
	{  // done abrar
	
	$sql_calendar_date = "SELECT * FROM ".CALENDAR." WHERE property_id = '$property_id' AND date = '$check_date'";
	$result_calendar_date = mysql_query($sql_calendar_date) or die(__LINE__.mysql_error());

		if(mysql_num_rows($result_calendar_date)>0)
		{
			return true;
		}else{
			return false;
		}
	
	}

	function checkCalendarForExtensionWithGuest($check_date,$extension_id,$guest_id)
	{ // done abrar
	
	$sql_calendar_date = "SELECT * FROM ".CALENDAR." WHERE property_extension_id = '$extension_id' AND date = '$check_date' AND guest_id != '$guest_id'";
	//echo $sql_calendar_date."\n";
	$result_calendar_date = mysql_query($sql_calendar_date) or die(__LINE__.mysql_error());

		if(mysql_num_rows($result_calendar_date)>0)
		{
			return true;
		}else{
			return false;
		}
	
	}
		
	function updateGuestRezo()
	{
		//	done abrar;

			$guest_id		=	$_REQUEST['guest_id'];
			$property_id	=	$_REQUEST['property_id'];
			$date_start		=	$_REQUEST['date_start'];
			$date_end		=	$_REQUEST['date_end'];


			$resultSt= mysql_query("SELECT * FROM ".GUESTS." WHERE id = '$guest_id'") or die(mysql_error());
				while($rowSt = mysql_fetch_array($resultSt)){
				$status  = $rowSt['status'];
				}

			
			
		
			$sql		=	"UPDATE ".GUESTS." SET date_start='$date_start', date_end='$date_end', property_id='$property_id' WHERE id = '$guest_id'";
			
			
			mysql_query($sql) or die(__LINE__.mysql_error());

			$sql_payments		=	"UPDATE  ".PAYMENTS." SET date_start='$date_start', date_end='$date_end' WHERE guest_id = '$guest_id'";

			mysql_query($sql_payments) or die(__LINE__.mysql_error());
						
			mysql_query("DELETE FROM ".CALENDAR." WHERE guest_id='".$guest_id."'") or die(__LINE__.mysql_error());


				$check_date = $date_start;
								
				while ($check_date != $date_end)
				{
					$sql		=	"INSERT INTO ".CALENDAR."(property_id,date,status,guest_id) VALUES ('$property_id','$check_date','$status','$guest_id')";
					
					mysql_query($sql) or die(__LINE__.mysql_error());
					$check_date = date ("Y-m-d", strtotime ("+1 day", strtotime($check_date)));
				}
			
			return true;
			
			}
	
	function CheckAvailableForUpdate()
	{ // done abrar
		
		$error = "";
		
		if($_REQUEST['date_start']=="mm/dd/yyyy" OR $_REQUEST['date_end']=="mm/dd/yyyy")
		{
			$error .= "Start Date or End Date is missing\n";
		}

		$daysDifference = daysDifference($_REQUEST['date_start'], $_REQUEST['date_end']);
		if($daysDifference<MINIMUM_STAY AND !isset($_SESSION['admin_loggedin']))
		{
			$error .= "We have a ".MINIMUM_STAY." nights minimum, please select atleast ".MINIMUM_STAY." nights stay\n";
		}

		if(empty($error)){
	
			if(isset($_REQUEST['property_id'])){$property_id=$_REQUEST['property_id'];}elseif(isset($_POST['property_id'])){$property_id=$_POST['property_id'];}
			if(isset($_REQUEST['guest_id'])){$guest_id=$_REQUEST['guest_id'];}elseif(isset($_POST['guest_id'])){$guest_id=$_POST['guest_id'];}
			$date_start		=	date("Y-m-d",strtotime($_REQUEST['date_start']));
			$date_end		=	date("Y-m-d",strtotime($_REQUEST['date_end']));

			$check_date = $date_start;
	
			while ($check_date != $date_end)
			{
				if(checkCalendarWithGuest($check_date,$property_id,$guest_id)===true) //true
				{
					$error .= "No space available for: ".date("m/d/Y", strtotime($check_date))."<br/>";
				}
				$check_date = date("Y-m-d", strtotime("+1 day", strtotime($check_date)));
			}
		}
	
	if(empty($error)) {return true;}else{return $error;}
	
	}

	function checkCalendarWithGuest($check_date,$property_id,$guest_id)
	{ // done abrar

	$sql_calendar_date = "SELECT * FROM ".CALENDAR." WHERE property_id = '$property_id' AND date = '$check_date' AND guest_id != '$guest_id'";
	$result_calendar_date = mysql_query($sql_calendar_date) or die(__LINE__.mysql_error());

		if(mysql_num_rows($result_calendar_date)>0)
		{
			return true;
		}else{
			return false;
		}
	
	}
	
	function calculatePriceBAK($property_id='', $date_start='', $date_end='', $strict = true)
	{	// done abrar
				
				if(empty($property_id)){$property_id = $_REQUEST['property_id'];}
				if(empty($date_start)){$date_start = $arrivaldate = date("Y-m-d", strtotime($_REQUEST['date_start']));}
				if(empty($date_end)){$date_end = date("Y-m-d", strtotime($_REQUEST['date_end']));}

				$GLOBALS['seasons'] = array();

				$price = "";
				$totalprice = "0";
				
				while ($date_start != $date_end)
				{
				
				//echo $date_start."<br/>";
					
					$sql1="SELECT * FROM ".PROPERTIESRATES." WHERE importance = '1' AND nightly = '1' AND property_id = '".$property_id."' AND '".$date_start."' >= `date_start` AND '".$date_end."' <= `date_end`";
					// AND vacation = 'Y'
					$result= mysql_query($sql1) or die($sql1.mysql_error());
					$num_rows = mysql_num_rows($result);
					if($num_rows != '1'){
									
							$sql2="SELECT * FROM ".PROPERTIESRATES." WHERE importance = '2' AND nightly = '1' AND property_id = '".$property_id."' AND '".$date_start."' >= `date_start` AND '".$date_end."' <= `date_end`";
							$result2= mysql_query($sql2) or die($sql2.mysql_error());
							$num_rows2 = mysql_num_rows($result2);

							if($num_rows2 != '1'){
							die ( "No price available for: ".date("m/d/Y",strtotime($date_start)));
							}
							while($row2 = mysql_fetch_array($result2)){
								if(empty($row2['price_daily']) OR $row2['price_daily']=="0") die ("No price available for:- ".date("m/d/Y",strtotime($date_start)));
								$price .= "+".$row2['price_daily'];
								$totalprice += $row2['price_daily'];
								
									if(!in_array($row2['season_id'],$GLOBALS['seasons'])){
									array_push($GLOBALS['seasons'],$row2['season_id']);
									}
								
								}
					
					}
					while($row = mysql_fetch_array($result)){
						if(empty($row['price_daily']) OR $row['price_daily']=="0") die ("No price available for:-- ".date("m/d/Y",strtotime($date_start)));
						$price .= "+".$row['price_daily'];
						$totalprice += $row['price_daily'];
									if(!in_array($row['season_id'],$GLOBALS['seasons'])){
									array_push($GLOBALS['seasons'],$row['season_id']);
									}

					}
				
				$date_start = date ("Y-m-d", strtotime ("+1 day", strtotime($date_start))); 
				
				}
				
				//print_r($GLOBALS['seasons']);
				
				$cronjobheight = array();
				 
				 $idsseasons = implode(',',$GLOBALS['seasons']); 
				 $sqlseasons = "SELECT * FROM ".RATESSEASONS." WHERE id IN ($idsseasons)";
				 //echo $sqlseasons;
					$resultseasons = mysql_query($sqlseasons);
					while($rowseasons = mysql_fetch_array($resultseasons)){
					$season_vs_arrival_days = daysDifference($arrivaldate, $rowseasons['date_start']);
					
				//	echo " VS: ".$season_vs_arrival_days." $rowseasons[date_start] <br/>"; 
				//	echo " Cronjob: ".$rowseasons['cronjob']."<br/>";
					
						if($season_vs_arrival_days < "0") { $season_vs_arrival_days = "0"; }
						
						$cronjobdays = $rowseasons['cronjob'] - $season_vs_arrival_days;
						
						array_push($cronjobheight,$cronjobdays);
				 	
						unset($cronjobdays);
						unset($season_vs_arrival_days);
					
					}
				 
			//	print_r($cronjobheight);
				$GLOBALS['cronjobdays'] = max($cronjobheight);
				// echo $GLOBALS['cronjobdays'];
				return $totalprice;
			
			}
	
	function calculatePrice($property_id='', $date_start='', $date_end='', $strict = true)
	{	// done abrar
				
				if(empty($property_id)){$property_id = $_REQUEST['property_id'];}
				if(empty($date_start)){$date_start = $arrivaldate = date("Y-m-d", strtotime($_REQUEST['date_start']));}
				if(empty($date_end)){$date_end = date("Y-m-d", strtotime($_REQUEST['date_end']));}

				$GLOBALS['seasons'] = array();

				$error = "";
				$price = "";
				$totalprice = "0";
				
				$daysDifference = daysDifference($date_start, $date_end);
				
				if($daysDifference>=30){
				$pricetype = 'monthly';
				}elseif($daysDifference>=14){
				$pricetype = 'two_weekly';
				}elseif($daysDifference>=7){
				$pricetype = 'weekly';
				}else{
				$pricetype = 'daily';
				}
				
				while($date_start != $date_end)
				{
				
				//echo $date_start."<br/>";
					
					$sql1="SELECT * FROM ".PROPERTIESRATES." WHERE importance = '1' AND nightly = '1' AND property_id = '".$property_id."' AND '".$date_start."' >= `date_start` AND '".$date_end."' <= `date_end`";// AND vacation = 'Y'
					
					$result= mysql_query($sql1) or die($sql1.mysql_error());
					$num_rows = mysql_num_rows($result);
					if($num_rows > '0'){
					while($row = mysql_fetch_array($result)){
						
						
					if($pricetype == 'daily'){
					if(empty($row['price_daily']) OR $row['price_daily']=="0") $error .= "No price available for:-- ".date("m/d/Y",strtotime($date_start));
					$price .= "+".$row['price_daily'];
					$totalprice += $row['price_daily'];
					}
					if($pricetype == 'weekly'){
					if(empty($row['price_weekly']) OR $row['price_weekly']=="0") $error .= "No price available for:-- ".date("m/d/Y",strtotime($date_start));
					$price .= "+".($row['price_weekly']/7);
					$totalprice += ($row['price_weekly']/7);
					}
					if($pricetype == 'two_weekly'){
					if(empty($row['price_two_weekly']) OR $row['price_two_weekly']=="0") $error .= "No price available for:-- ".date("m/d/Y",strtotime($date_start));
					$price .= "+".($row['price_two_weekly']/14);
					$totalprice += ($row['price_two_weekly']/14);
					}
					if($pricetype == 'monthly'){
					if(empty($row['price_monthly']) OR $row['price_monthly']=="0") $error .= "No price available for:-- ".date("m/d/Y",strtotime($date_start));
					$price .= "+".($row['price_monthly']/30);
					$totalprice += ($row['price_monthly']/30);
					}
						
						
									if(!in_array($row['season_id'],$GLOBALS['seasons'])){
									array_push($GLOBALS['seasons'],$row['season_id']);
									}

					}
					
					}else{
					
					
					
			
							
					$sql2="SELECT * FROM ".PROPERTIESRATES." WHERE importance = '2' AND nightly = '1' AND property_id = '".$property_id."' AND '".$date_start."' >= `date_start` AND '".$date_end."' <= `date_end`";
					$result2= mysql_query($sql2) or die($sql2.mysql_error());
					$num_rows2 = mysql_num_rows($result2);

					if($num_rows2 == '0'){
					$error .= "No price available for: ".date("m/d/Y",strtotime($date_start));
					}
					while($row2 = mysql_fetch_array($result2)){
																
					if($pricetype == 'daily'){
					if(empty($row2['price_daily']) OR $row2['price_daily']=="0") $error .= "No price available for:-- ".date("m/d/Y",strtotime($date_start));
					$price .= "+".$row2['price_daily'];
					$totalprice += $row2['price_daily'];
					}
					if($pricetype == 'weekly'){
					if(empty($row2['price_weekly']) OR $row2['price_weekly']=="0") $error .= "No price available for:-- ".date("m/d/Y",strtotime($date_start));
					$price .= "+".($row2['price_weekly']/7);
					$totalprice += ($row2['price_weekly']/7);
					}
					if($pricetype == 'two_weekly'){
					if(empty($row2['price_two_weekly']) OR $row2['price_two_weekly']=="0") $error .= "No price available for:-- ".date("m/d/Y",strtotime($date_start));
					$price .= "+".($row2['price_two_weekly']/14);
					$totalprice += ($row2['price_two_weekly']/14);
					}
					if($pricetype == 'monthly'){
					if(empty($row2['price_monthly']) OR $row2['price_monthly']=="0") $error .= "No price available for:-- ".date("m/d/Y",strtotime($date_start));
					$price .= "+".($row2['price_monthly']/30);
					$totalprice += ($row2['price_monthly']/30);
					}
				
				
					if(!in_array($row2['season_id'],$GLOBALS['seasons'])){
					array_push($GLOBALS['seasons'],$row2['season_id']);
					}
				
					}

					}
				
				$date_start = date ("Y-m-d", strtotime ("+1 day", strtotime($date_start))); 
				
				}
				
				$cronjobheight = array();
				 
				 
				 if(count($GLOBALS['seasons'])>'0'){
				 $idsseasons = implode(',',$GLOBALS['seasons']); 
				 $sqlseasons = "SELECT * FROM ".RATESSEASONS." WHERE id IN ($idsseasons)";
					$resultseasons = mysql_query($sqlseasons) or die(__LINE__.mysql_error());
					while($rowseasons = mysql_fetch_array($resultseasons)){
					$season_vs_arrival_days = daysDifference($arrivaldate, $rowseasons['date_start']);
					
				//	echo " VS: ".$season_vs_arrival_days." $rowseasons[date_start] <br/>"; 
				//	echo " Cronjob: ".$rowseasons['cronjob']."<br/>";
					
						if($season_vs_arrival_days < "0") { $season_vs_arrival_days = "0"; }
						
						$cronjobdays = $rowseasons['cronjob'] - $season_vs_arrival_days;
						
						array_push($cronjobheight,$cronjobdays);
				 	
						unset($cronjobdays);
						unset($season_vs_arrival_days);
					
					}
				 
				$GLOBALS['cronjobdays'] = max($cronjobheight);
				}
				return $totalprice;
			
			}
	
	function daysDifference($date_start, $date_end)
	{ // done abrar
		return intval((strtotime($date_end) - strtotime($date_start))/86400);
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

function doCustomerProfile(){
$AuthnetCIM = new AuthnetCIM();
    // Create the profile
    $AuthnetCIM->setParameter('email', $_POST['email']);
    $AuthnetCIM->setParameter('description', 'Guest No. ' . md5(uniqid(rand(), true)));
    $AuthnetCIM->setParameter('merchantCustomerId', substr(md5(uniqid(rand(), true)), 16, 16));
    $AuthnetCIM->createCustomerProfile();
 
    // Get the profile ID returned from the request
    if ($AuthnetCIM->isSuccessful())
    {
		$profile_id = $AuthnetCIM->getProfileID();
    }
 
    // Print the results of the request
#    echo '<b>Code:</b> ' . $AuthnetCIM->getCode() . '<br />';
 #   echo '<b>Message:</b> ' . $AuthnetCIM->getResponse() . '<br />';
  #  echo '<b>Profile ID:</b> ' . $profile_id . '<br /><br />';
        $GLOBALS['success_doCustomerProfile'] = $AuthnetCIM->getCode();
        $GLOBALS['message_doCustomerProfile'] = $AuthnetCIM->getResponse();
	return $profile_id;
}

function doCustomerPaymentProfile($customer_profile_id){
$AuthnetCIM = new AuthnetCIM();

    // Step 2: create Payment Profile
    //
    // Create fake user billing information
    $b_first_name   = $_POST['firstname'];
    $b_last_name    = $_POST['lastname'];
    $b_address      = $_POST['address1'];
    $b_city         = $_POST['city'];
    $b_state        = $_POST['state'];
    $b_zip          = $_POST['zip'];
    $b_country      = 'US';
    $b_phone_number = $_POST['phone1'].'-'.$_POST['phone2'].'-'.$_POST['phone3'];

    $credit_card    = $_POST['ccnumber'];
    $expiration     = $_POST['ccexpY']."-".$_POST['ccexpM'];
    //$expiration     = (date("Y") + 1) . '-12';
	
 
    // Create the Payment Profile
    $AuthnetCIM->setParameter('customerProfileId', $customer_profile_id);
    $AuthnetCIM->setParameter('billToFirstName', $b_first_name);
    $AuthnetCIM->setParameter('billToLastName', $b_last_name);
    $AuthnetCIM->setParameter('billToAddress', $b_address);
    $AuthnetCIM->setParameter('billToCity', $b_city);
    $AuthnetCIM->setParameter('billToState', $b_state);
    $AuthnetCIM->setParameter('billToZip', $b_zip);
    $AuthnetCIM->setParameter('billToCountry', $b_country);
    $AuthnetCIM->setParameter('billToPhoneNumber', $b_phone_number);

    $AuthnetCIM->setParameter('cardNumber', $credit_card);
    $AuthnetCIM->setParameter('expirationDate', $expiration);
    $AuthnetCIM->createCustomerPaymentProfile();
 
    // Get the payment profile ID returned from the request
    if ($AuthnetCIM->isSuccessful())
    {
		$payment_profile_id = $AuthnetCIM->getPaymentProfileId();
    }
 
    // Print the results of the request
#    echo '<b>Code:</b> ' . $AuthnetCIM->getCode() . '<br />';
 #   echo '<b>Message:</b> ' . $AuthnetCIM->getResponse() . '<br />';
  #  echo '<b>Payment Profile ID:</b> ' . $payment_profile_id . '<br /><br />';
        $GLOBALS['success_doCustomerPaymentProfile'] = $AuthnetCIM->getCode();
        $GLOBALS['message_doCustomerPaymentProfile'] = $AuthnetCIM->getResponse();
	return $payment_profile_id;
 
} 

function doCustomerPaymentProfileCC($customer_profile_id){
$AuthnetCIM = new AuthnetCIM();
    // echo " Step 2: create Payment Profile";
    //
    // Create fake user billing information
    $b_first_name   = $_POST['ccfirstname'];
    $b_last_name    = $_POST['cclastname'];
    $b_address      = $_POST['ccaddress1'];
    $b_city         = $_POST['cccity'];
    $b_state        = $_POST['ccstate'];
    $b_zip          = $_POST['cczip'];
    $b_country      = 'US';
    $b_phone_number = $_POST['phone1'].'-'.$_POST['phone2'].'-'.$_POST['phone3'];

    $credit_card    = $_POST['ccnumber'];
    $expiration     = $_POST['ccexpY']."-".$_POST['ccexpM'];
    //$expiration     = (date("Y") + 1) . '-12';
	
 
    // Create the Payment Profile
    $AuthnetCIM->setParameter('customerProfileId', $customer_profile_id);
    $AuthnetCIM->setParameter('billToFirstName', $b_first_name);
    $AuthnetCIM->setParameter('billToLastName', $b_last_name);
    $AuthnetCIM->setParameter('billToAddress', $b_address);
    $AuthnetCIM->setParameter('billToCity', $b_city);
    $AuthnetCIM->setParameter('billToState', $b_state);
    $AuthnetCIM->setParameter('billToZip', $b_zip);
    $AuthnetCIM->setParameter('billToCountry', $b_country);
    $AuthnetCIM->setParameter('billToPhoneNumber', $b_phone_number);

    $AuthnetCIM->setParameter('cardNumber', $credit_card);
    $AuthnetCIM->setParameter('expirationDate', $expiration);
    $AuthnetCIM->createCustomerPaymentProfile();
 
    // Get the payment profile ID returned from the request
    if ($AuthnetCIM->isSuccessful())
    {
		$payment_profile_id = $AuthnetCIM->getPaymentProfileId();
    }
 
    // Print the results of the request
#    echo '<b>Code:</b> ' . $AuthnetCIM->getCode() . '<br />';
 #   echo '<b>Message:</b> ' . $AuthnetCIM->getResponse() . '<br />';
  #  echo '<b>Payment Profile ID:</b> ' . $payment_profile_id . '<br /><br />';
        $GLOBALS['success_doCustomerPaymentProfileCC'] = $AuthnetCIM->getCode();
        $GLOBALS['message_doCustomerPaymentProfileCC'] = $AuthnetCIM->getResponse();
	return $payment_profile_id;
 
} 

function doCustomerProfileTransactionAuthCapture($profile_id,$payment_profile_id,$amount,$cvv){ //sale
$AuthnetCIM = new AuthnetCIM();

    // Step 4: Process a transaction
    //

    // Process the transaction

    $AuthnetCIM->setParameter('amount', $amount);
    $AuthnetCIM->setParameter('customerProfileId', $profile_id);
    $AuthnetCIM->setParameter('customerPaymentProfileId', $payment_profile_id);
    $AuthnetCIM->setParameter('cardCode', $cvv);
    $AuthnetCIM->createCustomerProfileTransaction('profileTransAuthCapture');

  //profileTransAuthCapture
  //profileTransCaptureOnly
  //profileTransAuthOnly
 
 
    // Get the payment profile ID returned from the request
    if ($AuthnetCIM->isSuccessful())
    {
        $approval_code = $AuthnetCIM->getAuthCode();
        $getTransactionID = $AuthnetCIM->getTransactionID();
    }
 
    // Print the results of the request
#    echo '<b>Code:</b> ' . $AuthnetCIM->getCode() . '<br />';
 #   echo '<b>Message:</b> ' . $AuthnetCIM->getResponse() . '<br />';
  #  echo '<b>AuthCapture TransID:</b> ' . $getTransactionID . '<br />';
   # echo '<b>Approval code:</b> ' . $approval_code;
        $GLOBALS['success_doCustomerProfileTransactionAuthCapture'] = $AuthnetCIM->getCode();
        $GLOBALS['message_doCustomerProfileTransactionAuthCapture'] = $AuthnetCIM->getResponse();
	return $getTransactionID;
}

function doCustomerProfileTransactionRefund($profile_id,$payment_profile_id,$transaction_id,$amount){ //sale
$AuthnetCIM = new AuthnetCIM();
    //

    // Process the transaction

    $AuthnetCIM->setParameter('amount', $amount);
    $AuthnetCIM->setParameter('customerProfileId', $profile_id);
    $AuthnetCIM->setParameter('customerPaymentProfileId', $payment_profile_id);
    $AuthnetCIM->createCustomerProfileTransactionRefund('profileTransRefund');

  //profileTransAuthCapture
  //profileTransCaptureOnly
  //profileTransAuthOnly
 
 
    // Get the payment profile ID returned from the request
    if ($AuthnetCIM->isSuccessful())
    {
        $approval_code = $AuthnetCIM->getAuthCode();
        $getTransactionID = $AuthnetCIM->getTransactionID();
    }
 
    // Print the results of the request
#    echo '<b>Code:</b> ' . $AuthnetCIM->getCode() . '<br />';
 #   echo '<b>Message:</b> ' . $AuthnetCIM->getResponse() . '<br />';
  #  echo '<b>AuthCapture TransID:</b> ' . $getTransactionID . '<br />';
   # echo '<b>Approval code:</b> ' . $approval_code;
        $GLOBALS['success_doCustomerProfileTransactionRefund'] = $AuthnetCIM->getCode();
        $GLOBALS['message_doCustomerProfileTransactionRefund'] = $AuthnetCIM->getResponse();
	
	return $getTransactionID;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

?>