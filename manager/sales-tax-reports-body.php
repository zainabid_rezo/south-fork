<?php 
$error = '';
if(isset($_POST['btnSearch']) AND ($_POST['date_start']=="mm/dd/yyyy" OR $_POST['date_end']=="mm/dd/yyyy") ){
$error = "Please select date range!";
}
$date_start = date("Y-m-d",strtotime($_POST['date_start']));
$date_end = date("Y-m-d",strtotime($_POST['date_end']));
?>
<!------------------------------------------------------------------------------->
<?php include"messages-display.php";?>

<form name="statistics" method="post" action="">
  <table width="800" class="noprint">
    <tr>
      <td colspan="4"><div class="quickt"><strong>Search:</strong></div></td>
    
    <tr>
      <td width="25%" class="redtext">From:</td>
      <td width="25%"><input name="date_start" type="text" id="date_start" value="<?php if(isset($_POST['date_start'])) echo $_POST['date_start']; else echo "mm/dd/yyyy";?>" size="12" readonly="readonly" /></td>
      <td width="25%"><span class="redtext">To:</span></td>
      <td width="25%"><input name="date_end" type="text" id="date_end" value="<?php if(isset($_POST['date_end'])) echo $_POST['date_end']; else echo "mm/dd/yyyy";?>" size="12" readonly="readonly" /></td>
    </tr>
    <tr>
      <td class="redtext">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><input type="submit" name="btnSearch" id="Search" value="Search" class="button" />      </td>
    </tr>
  </table>
</form>

<?php 
//print_r($_POST);
?>
<?php if(isset($_POST['btnSearch']) AND empty($error)){ ?>
<br />
<br />
<table class="noprint">
    <tr><td>
	<input type="button" value="Print this page" onClick="printpage()" class="button" />
	</td></tr>
</table>
<br />
<br />
<div class="hiddenprint">
<h2>Sales Tax</h2>
<h3>Dates between: <?php if(isset($_POST['date_start'])) echo $_POST['date_start']; else echo "mm/dd/yyyy";?> and <?php if(isset($_POST['date_end'])) echo $_POST['date_end']; else echo "mm/dd/yyyy";?>
</h3>
</div>
<?php } ?>
<?php 
if(isset($_POST['btnSearch']) AND empty($error)){ ?>
<div class="printable">
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
<thead>
  <tr>
    <th class="topHeader">Date</th>
    <th>Customer Name </th>
    <th>Reservation Dates</th>
    <th>Total Amount Paid</th>
    <th>Tax Amount(<?=$sales_tax_rate?>%)</th>
    <td> </td>
  </tr>
</thead>
<tbody>

  <?php
	$sales_tax_rate = '7';
	$sales_tax_total = '0';
    ?>

  <?php
	$sqlR = "SELECT * FROM ".GUESTS.""; // 
	$resR = mysql_query($sqlR) or die(__LINE__.mysql_error());
    while($rowR=mysql_fetch_array($resR))
    {
	
	if($rowR['paid_date']>=$date_start AND $rowR['paid_date']<=$date_end )
	{
    ?>
	  <tr>
		<td height="21"><?php
			echo date("m/d/Y",strtotime($rowR['paid_date']));
			?></td>
		<td><?php
			echo "<strong>".$rowR['firstname']." ".$rowR['lastname']."</strong><br/>";
			?></td>
		<td><?php
			echo date("m/d/Y",strtotime($rowR['date_start']));
			?> - <?php
			echo date("m/d/Y",strtotime($rowR['date_end']));
			?></td>
		<td>$<?php echo money($rowR['amount_paid']);?></td>
		<td>$<?php echo money($rowR['amount_paid']*$sales_tax_rate/100); $sales_tax_total += $rowR['amount_paid']*$sales_tax_rate/100;?></td>
		<td><a href="reservations.php?editid=<?=$rowR['id']?>" class="noprint" target="_target">[detail]</a></td>
	  </tr>
	  <?php
	} 
	
	if($rowR['final_payment_received']=='1' AND $rowR['final_payment_date']>=$date_start AND $rowR['final_payment_date']<=$date_end ){
    ?>
	  <tr>
		<td height="21"><?php
			echo date("m/d/Y",strtotime($rowR['final_payment_date']));
			?></td>
		<td><?php
			echo "<strong>".$rowR['firstname']." ".$rowR['lastname']."</strong><br/>";
			?></td>
		<td><?php
			echo date("m/d/Y",strtotime($rowR['date_start']));
			?> - <?php
			echo date("m/d/Y",strtotime($rowR['date_end']));
			?></td>
		<td>$<?php echo money($rowR['lodging_amount']);?></td>
		<td>$<?php echo money($rowR['lodging_amount']*$sales_tax_rate/100); $sales_tax_total += $rowR['lodging_amount']*$sales_tax_rate/100;?></td>
		<td><a href="reservations.php?editid=<?=$rowR['id']?>" class="noprint" target="_target">[detail]</a></td>
	  </tr>
	  <?php
	}
	?>




  <?php
	} // while
	?>
  <tr>
    <td height="21" colspan="3"></td>
    <td><strong>Total</strong></td>
    <td>$<?php echo money($sales_tax_total);?></td>
    <td>&nbsp;</td>
  </tr>
</tbody>
</table>
</div>
<?php } ?>

</div>