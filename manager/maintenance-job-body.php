<?php 
$imgwidth = "300";
$imgheight = "225";
?>
<?php
if(isset($_POST['btnAdd'])){

$error = "";

	$title		=	addslashes($_POST['title']);
	if(empty($title)) $error .= "Please enter title.<br/>";
	$amount		=	addslashes($_POST['amount']);
	if(empty($amount)) $error .= "Please enter amount.<br/>";
	$date		=	addslashes($_POST['date']);
	if(empty($date) or $date=='mm/dd/yyyy'){ $error .= "Please select date.<br/>";}
	else{$date = date("Y-m-d",strtotime($date));}
	$property_id		=	addslashes($_POST['property_id']);
	if(empty($property_id)) $error .= "Please select a property.<br/>";
	$notes		=	htmlentities($_POST['notes'], ENT_QUOTES);
	//id	owner_type	company_name	name	address1	address2	city	state	zip	country	email	phone	mobile	sortby	status

	if(empty($error)){
	$sql		=	"INSERT INTO ".MAINTENANCEJOBS." (`title`, `amount`, `date`, `property_id`, `notes`, `created`) VALUES ('$title', '$amount', '$date', '$property_id', '$notes', NOW())";
	mysql_query($sql) or die(__LINE__.mysql_error());
	$id = $insert_id = mysql_insert_id();
	$success	= "Successfuly added.<br/>";
	unset($_GET);

}

}
?>
<?php
if(isset($_POST['btnEditDo'])){
	
$error = "";

	$editid		=	addslashes($_POST['editid']);

	$title		=	addslashes($_POST['title']);
	if(empty($title)) $error .= "Please enter title.<br/>";
	$amount		=	addslashes($_POST['amount']);
	if(empty($amount)) $error .= "Please enter amount.<br/>";
	$date		=	addslashes($_POST['date']);
	if(empty($date) or $date=='mm/dd/yyyy'){ $error .= "Please select date.<br/>";}
	else{$date = date("Y-m-d",strtotime($date));}
	
	
	$property_id		=	addslashes($_POST['property_id']);
	if(empty($property_id)) $error .= "Please select a property.<br/>";
	$notes		=	htmlentities($_POST['notes'], ENT_QUOTES);
	//id	owner_type	company_name	name	address1	address2	city	state	zip	country	email	phone	mobile	sortby	status


	if(empty($error))
	{

     if(mysql_query("UPDATE ".MAINTENANCEJOBS." SET title = '$title', amount = '$amount', date = '$date', property_id = '$property_id', notes = '$notes' WHERE id = '$editid'")){
	 $success = "Successfully updated.";
	 unset($_GET);
	 
		}else{ echo mysql_error(); }
	
   }

}
?>
<?php if(isset($_GET['delete']) AND !empty($_GET['delete'])){

$delid = $_GET['delete'];

	$sql		=	"DELETE FROM ".MAINTENANCEJOBS." WHERE id = '$delid'";
	mysql_query($sql) or die(mysql_error());
	$success	= "Successfully deleted.<br/>";
	unset($_GET);

}
?>
<!------------------------------------------------------------------------------->
<?php include"messages-display.php";?>
<?php if(isset($_GET['add'])){ ?>
<fieldset>
<legend>
<h2>Add Maintenance Job</h2>
</legend>
  <form action="" method="post" enctype="multipart/form-data">
    <table class="form-table">
      <tr>
        <th>Title<font color="#ff0000">*</font></th>
        <td><input name="title" type="text" value="<?=$_POST['title']?>" /></td>
      </tr>
      <tr>
        <th>Amount<font color="#ff0000">*</font></th>
        <td>$<input name="amount" type="text" value="<?=$_POST['amount']?>" size="5" /></td>
      </tr>
	  <tr>
        <th>Date<font color="#ff0000">*</font></th>
        <td><input type="text" name="date" size="12" id="date" readonly="readonly" value="<?php if(isset($_POST['date'])) echo $_POST['date']; else echo "mm/dd/yyyy";?>" /></td>
      </tr>
	  <tr>
        <th>Property<font color="#ff0000">*</font></th>
        <td>
        <select name="property_id" id="property_id">
          <option value="0" <?php if(!isset($_POST['property_id']) OR $_POST['property_id']=="0"){echo 'selected="selected"';}?> > - select - </option>
          <?php
					
					$resultK= mysql_query("SELECT * FROM ".PROPERTIES." ORDER BY sortby ASC");
						while($rowK = mysql_fetch_array($resultK)){
						
						echo '<option value="'.$rowK['id'].'"';
						if(isset($_POST['property_id']) AND $_POST['property_id']==$rowK['id']){echo 'selected="selected"';}
						echo '>'.stripslashes($rowK['title']).'</option>';
						
						}
					
					?>
        </select>
        </td>
      </tr>
      
      <tr>
        <th valign="top">Notes<font color="#ff0000"></font></th>
        <td><textarea name="notes" style="width:800px; height:100px;"><?=$_POST['notes']?></textarea></td>
      </tr>
      <tr>
        <th></th>
        <td><input type="submit" name="btnAdd" class="button" value="Add" /></td>
      </tr>
    </table>
  </form>
</fieldset>
<?php }
elseif(isset($_GET['editid'])){ ?>
<fieldset>
<legend>
<h2>Edit Maintenance Job</h2>
</legend>
<?php
$id = $_GET['editid'];
$result= mysql_query("SELECT * FROM ".MAINTENANCEJOBS." WHERE id = '$id'");
while($row = mysql_fetch_array($result)){
?>
<form action="" method="post" enctype="multipart/form-data">
  <table class="form-table">
	  <tr>
        <th>Title<font color="#ff0000">*</font></th>
        <td><input name="title" type="text" value="<?=stripslashes($row['title'])?>" size="10" />        </td>
      </tr>
	  <tr>
        <th>Amount<font color="#ff0000">*</font></th>
        <td>$<input name="amount" type="text" value="<?=money($row['amount'])?>" size="5" />        </td>
      </tr>
      <tr>
        <th>Date<font color="#ff0000">*</font></th>
        <td><input name="date" id="date" type="text" value="<?=date('m/d/Y',strtotime($row['date']))?>" /></td>
      </tr>
      <tr>
        <th>Property<font color="#ff0000">*</font></th>
        <td>
        <select name="property_id" id="property_id">
          <option value="0" <?php if($row['property_id']=="0"){echo 'selected="selected"';}?> > - select - </option>
          <?php
					
					$resultK= mysql_query("SELECT * FROM ".PROPERTIES." ORDER BY sortby ASC");
						while($rowK = mysql_fetch_array($resultK)){
						
						echo '<option value="'.$rowK['id'].'"';
						if($rowK['id']==$row['property_id']){echo 'selected="selected"';}
						echo '>'.stripslashes($rowK['title']).'</option>';
						
						}
					
					?>
        </select>     
        </td>
      </tr>

    <tr>
      <th valign="top">Notes</th>
      <td><textarea name="notes" id="notes" style="width:800px; height:100px;"><?=stripslashes($row['notes'])?></textarea></td>
    </tr>
    <tr>
      <th></th>
      <td><input type="submit" name="btnEditDo" class="button" value="Update" />
        <input name="editid" type="hidden" value="<?=$row['id']?>" /></td>
    </tr>
  </table>
</form>
</fieldset>
<?php } ?>
<?php }else{ ?>
<h2>View</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
<thead>
  <tr>
    <td width="100">Title</td>
    <th width="100">Amount</th>
    <th width="50" class="headerSortUp">Date</th>
    <td width="50">Edit</td>
    <td width="50">Delete</td>
  </tr>
</thead>
<tbody>
<?php
$result= mysql_query("SELECT * FROM ".MAINTENANCEJOBS." ORDER BY date DESC") or die(__LINE__.mysql_error());
while($row = mysql_fetch_array($result)){
?>
    <tr>
      <td><?=stripslashes($row['title'])?></td>
      <td>$<?=money($row['amount'])?></td>
      <td><?=date('m/d/Y',strtotime($row['date']))?></td>
      <td><a href="?editid=<?=$row['id']?>"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
      <td><a href="javascript:confirmDelete('?delete=<?=$row['id']?>');"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
  <?php } ?>
</tbody>
</table>
<?php } ?>