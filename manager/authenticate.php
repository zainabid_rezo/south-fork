<?php 
if(session_id() == '') {
    session_start();
}
if(!(isset($_SESSION["admin_loggedin"])) || empty($_SESSION["admin_loggedin"]))
{
redirect('login.php');
}
?>