<script language="javascript" src="reservations-calculate-rate-ajax.js.php"></script>
<script language="javascript" src="reservations-calculate-special-rate-ajax.js.php"></script>
<script language="javascript" src="reservations-populate-values-ajax.js"></script>
<?php
if(!isset($_GET['property_id']))
{
?>
<table class="form-table">
	<tr>
      <td><form method="post" action="reservations-add.php?add">
          <input name="keyword" type="text" id="keyword" value="<?=$_POST['keyword']?>" placeholder="Search..."/>
          <input type="submit" name="btnSearch" value="Go" class="button" />
        </form></td>
    </tr>
</table>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
  <thead>
    <tr>
      <td width="120">Preview</td>
      <th width="80">Name</th>
      <td width="300">Reservations</td>
    </tr>
  </thead>
  <tbody>
    <?php
	if(isset($_POST['keyword']) AND !empty($_POST['keyword'])){
		$sql = "SELECT * FROM ".PROPERTIES." WHERE vacation = '1' AND title LIKE '%".$_POST['keyword']."%' ORDER BY title ASC";
	}else{
		$sql = "SELECT * FROM ".PROPERTIES." WHERE vacation = '1' ORDER BY title ASC"; //sortby
	}
	$result= mysql_query($sql) or die(__LINE__.mysql_error());
	while($row = mysql_fetch_array($result)){
	?>
    <tr>
      <td><?php print_thumb("../properties/".get_property_image($row['id']), "100", "100"); ?></td>
      <td valign="middle"><?=stripslashes($row['title'])?>
      </td>
      <td valign="middle"><a href="reservations-add.php?property_id=<?=$row['id']?>" class="button tooltip" ><span class="ui-icon ui-icon-newwin"></span>Add Reservation</a></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<?php
}else{
$property_id = $_GET['property_id'];
?>
<?php
if(isset($_POST['btnAdd'])){
$success = '';
$error = '';

if(empty($_POST['firstname'])) $error .="Please enter first name.<br>";
if(empty($_POST['lastname'])) $error .="Please enter last name.<br>";
if(!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $_POST['email'])) $error .= "Please enter correct email address.<br/>";
if(empty($_POST['date_start']) OR $_POST['date_start']=='mm/dd/yyyy') $error .="Please enter arrival date.<br>";
if(empty($_POST['date_end']) OR $_POST['date_end']=='mm/dd/yyyy') $error .="Please enter departure date.<br>";
if(empty($_POST['status'])) $error .="Please select status.<br>";

			if($_POST['payment_method']=='Check' AND $_POST['status'] == 'B') $error .= "Please chose correct status for Check reservation.<br />";
			if($_POST['payment_method']=='Credit Card' AND $_POST['status'] == 'P') $error .= "Please chose correct status for Credit Card reservation.<br />";

if(empty($error)){
include"reservations.class.php";

$CheckAvailable = CheckAvailable();
	if($CheckAvailable===true)
	{
	$success .= "Property is available<br/>";
	$success .= insertGuestBackend();
	$done = true;
	unset($_POST);
	}else{
	$error .= "Property is already booked...<br/>";
	$error  .= $CheckAvailable;
	$failed = true;
	}
}
}


if(isset($_POST['btnAddwithCC']) AND !empty($_POST['ccamount'])){
$success = '';
include"reservations.class.php";

$CheckAvailable = CheckAvailable();
	if($CheckAvailable===true )
	{
	$success .= "Property is available...<br/>";
	$success .= insertGuestBackendwithCC();
	$done = true;
	unset($_POST);
	}else{
	$success .= "Property is already booked...<br/>";
	$success .= $CheckAvailable;
	$failed = true;
	}
}
?>
<!------------------------------------------------------------------------------->
<?php include"messages-display.php";?>
<?php
if(isset($GLOBALS['insert_guest_id']))
{
?>
<br />
<br />
<a href="reservations-view.php?editid=<?=$GLOBALS['insert_guest_id']?>">&raquo;&raquo;Edit&raquo;&raquo;</a> <br />
<br />
<?php
}
?>
<h2>Add reservation for:
  <?=stripslashes(get_property_name($_GET['property_id']))?>
</h2>
<form action="" method="post" enctype="multipart/form-data">
  <table class="form-table">
    <tr valign="top">
      <th scope="row"><label>First Name<span style="color:#FF0000">*</span></label></th>
      <td><input name="firstname" type="text" id="firstname" value="<?=$_POST['firstname']?>" required /></td>
      <td rowspan="15">&nbsp;</td>
      <td rowspan="15"><div style="background-color: #ffffff;">
          <iframe name="iframe" width="450" height="450" src="calendar.php?property_id=<?=$_GET['property_id']?>" frameborder="0"></iframe>
        </div></td>
    </tr>
    <tr valign="top">
      <th scope="row"><label>Last Name<span style="color:#FF0000">*</span></label></th>
      <td><input name="lastname" type="text" id="lastname" value="<?=$_POST['lastname']?>" required /></td>
    </tr>
    <tr valign="top">
      <th scope="row">Adults</th>
      <td><input name="adults" type="text" id="adults" size="5" value="<?=$_POST['adults']?>" /></td>
    </tr>
    <tr valign="top">
      <th scope="row">Children</th>
      <td><input name="children" type="text" id="children" size="5" value="<?=$_POST['children']?>" /></td>
    </tr>
    <tr valign="top">
      <th scope="row">Pets</th>
      <td><input name="pets" type="text" id="pets" size="5" value="<?=$_POST['pets']?>" />
        (Please add details in notes field) </td>
    </tr>
    <tr valign="top">
      <th scope="row"><label><span style="color:#FF0000"></span>Address</label></th>
      <td><textarea name="address" cols="50" id="address" style="width:200px; height:40px;"><?=$_POST['address']?>
</textarea></td>
    </tr>
    <tr valign="top">
      <th scope="row"><label>City</label></th>
      <td><input name="city" type="text" id="city" value="<?=$_POST['city']?>" /></td>
    </tr>
    <tr valign="top">
      <th scope="row"><label>State</label></th>
      <td><select name="state" id="state">
          <?php if (isset($_POST['state'])) echo '<option value="'.$_POST['state'].'">'.$_POST['state'].'</option>';?>
          <option value="">----Select----</option>
          <option value="AL">Alabama</option>
          <option value="AK">Alaska</option>
          <option value="AZ">Arizona</option>
          <option value="AR">Arkansas</option>
          <option value="CA">California</option>
          <option value="CO">Colorado</option>
          <option value="CT">Connecticut</option>
          <option value="DE">Delaware</option>
          <option value="DC">District of Columbia</option>
          <option value="FL">Florida</option>
          <option value="GA">Georgia</option>
          <option value="HI">Hawaii</option>
          <option value="ID">Idaho</option>
          <option value="IL">Illinois</option>
          <option value="IN">Indiana</option>
          <option value="IA">Iowa</option>
          <option value="KS">Kansas</option>
          <option value="KY">Kentucky</option>
          <option value="LA">Louisiana</option>
          <option value="ME">Maine</option>
          <option value="MD">Maryland</option>
          <option value="MA">Massachusetts</option>
          <option value="MI">Michigan</option>
          <option value="MN">Minnesota</option>
          <option value="MS">Mississippi</option>
          <option value="MO">Missouri</option>
          <option value="MT">Montana</option>
          <option value="NE">Nebraska</option>
          <option value="NV">Nevada</option>
          <option value="NH">New Hampshire</option>
          <option value="NJ">New Jersey</option>
          <option value="NM">New Mexico</option>
          <option value="NY">New York</option>
          <option value="NC">North Carolina</option>
          <option value="ND">North Dakota</option>
          <option value="OH">Ohio</option>
          <option value="OK">Oklahoma</option>
          <option value="OR">Oregon</option>
          <option value="PA">Pennsylvania</option>
          <option value="RI">Rhode Island</option>
          <option value="SC">South Carolina</option>
          <option value="SD">South Dakota</option>
          <option value="TN">Tennessee</option>
          <option value="TX">Texas</option>
          <option value="UT">Utah</option>
          <option value="VT">Vermont</option>
          <option value="VA">Virginia</option>
          <option value="WA">Washington</option>
          <option value="WV">West Virginia</option>
          <option value="WI">Wisconsin</option>
          <option value="WY">Wyoming</option>
        </select></td>
    </tr>
    <tr valign="top">
      <th scope="row"><label>Zip</label></th>
      <td><input name="zip" type="text" id="zip" size="10" maxlength="5" value="<?=$_POST['zip']?>" /></td>
    </tr>
    <tr valign="top">
      <th scope="row">Email<span style="color:#FF0000">*</span> </th>
      <td><input name="email" type="text" id="email" size="50" value="<?=$_POST['email']?>" required /></td>
    </tr>
    <tr valign="top">
      <th scope="row"><label>Phone</label></th>
      <td><input type="text" name="phone1" id="phone1" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" value="<?=$_POST['phone1']?>" />
        <input type="text" name="phone2" id="phone2" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" value="<?=$_POST['phone2']?>" />
        <input type="text" name="phone3" id="phone3" onKeyUp="return autoTab(this, 4, event);" size="5" maxlength="4" value="<?=$_POST['phone3']?>" />
        &nbsp;&nbsp;&nbsp;
        <input name="phone_notes" type="text" id="phone_notes" value="<?=$_POST['phone_notes']?>" />
      </td>
    </tr>
    <tr valign="top">
      <th scope="row"><label>Cell</label></th>
      <td><input type="text" name="cell1" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" value="<?=$_POST['cell1']?>" />
        <input type="text" name="cell2" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" value="<?=$_POST['cell2']?>" />
        <input type="text" name="cell3" onKeyUp="return autoTab(this, 4, event);" size="5" maxlength="4" value="<?=$_POST['cell3']?>" />
        &nbsp;&nbsp;&nbsp;
        <input name="cell_notes" type="text" id="cell_notes" value="<?=$_POST['cell_notes']?>" />
      </td>
    </tr>
    <tr valign="top">
      <th scope="row"><label>Cell2</label></th>
      <td><input type="text" name="cell_21" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" value="<?=$_POST['cell_21']?>" />
        <input type="text" name="cell_22" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" value="<?=$_POST['cell_22']?>" />
        <input type="text" name="cell_23" onKeyUp="return autoTab(this, 4, event);" size="5" maxlength="4" value="<?=$_POST['cell_23']?>" />
        &nbsp;&nbsp;&nbsp;
        <input name="cell_2_notes" type="text" id="cell_2_notes" value="<?=$_POST['cell_2_notes']?>" />
      </td>
    </tr>
    <tr valign="top">
      <th scope="row"><label>Business Phone</label></th>
      <td><input type="text" name="phone_business1" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" value="<?=$_POST['phone_business1']?>" />
        <input type="text" name="phone_business2" onKeyUp="return autoTab(this, 3, event);" size="4" maxlength="3" value="<?=$_POST['phone_business2']?>" />
        <input type="text" name="phone_business3" onKeyUp="return autoTab(this, 4, event);" size="5" maxlength="4" value="<?=$_POST['phone_business3']?>" />
        &nbsp;&nbsp;&nbsp;
        <input name="phone_business_notes" type="text" id="phone_business_notes" value="<?=$_POST['phone_business_notes']?>" />
      </td>
    </tr>
    <tr valign="top">
      <th scope="row">Arrival<span style="color:#FF0000">*</span></th>
      <td><input type="text" name="date_start" size="12" id="date_start" readonly="readonly" value="<?php if(isset($_POST['date_start'])) echo $_POST['date_start']; else echo "mm/dd/yyyy";?>" /></td>
    </tr>
    <tr valign="top">
      <th scope="row">Departure<span style="color:#FF0000">*</span></th>
      <td><input type="text" name="date_end" size="12" id="date_end" readonly="readonly" value="<?php if(isset($_POST['date_end'])) echo $_POST['date_end']; else echo "mm/dd/yyyy";?>" /></td>
      <td></td>
      <td rowspan="6" valign="middle">
	  <div style="display:<?php if(isset($_POST['payment_method']) AND $_POST['payment_method']!="Credit Card"){echo 'none';}elseif(!isset($_POST['payment_method'])){echo 'none';}?>" id="credit_card_payment_block"> Deposit Term:<br />
          <select name="deposit_term" id="deposit_term">
            <option value="half" <?php if(isset($_POST['payment_method']) AND $_POST['payment_method']=="half"){echo 'selected="selected"';}?> >50%</option>
            <option value="full" <?php if(isset($_POST['payment_method']) AND $_POST['payment_method']=="full"){echo 'selected="selected"';}?> >100%</option>
          </select>
          <br />
          <br />
          <br />
		  <?php if(!isset($_POST['guest_id_temp'])){$guest_id_temp=time();}else{$guest_id_temp=$_POST['guest_id_temp'];}?>
		  <input name="guest_id_temp" type="hidden" id="guest_id_temp" value="<?=$guest_id_temp?>" size="10" />
          <a href="javascript:ccProcessPopup('<?=$guest_id_temp?>','yes')" class="button tooltip"><span class="ui-icon ui-icon-newwin"></span>Open a Credit Card Process</a>
          <div id="credit_card_payments"></div>
        </div>

		
		<div id="check_payment_block" style="display:<?php if(isset($_POST['payment_method']) AND $_POST['payment_method']=="Check"){echo 'block';}elseif($row['payment_method']=="Check"){echo 'block';}else{echo 'none';}?>">
		
		<label>
        <input name="send_deposit_request_email_50" type="checkbox" value="1" />
        Send email request for 50% payment.</label>
        <br />
        <label>
        <input name="send_deposit_request_email_100" type="checkbox" value="1" />
        Send email request for 100% payment.</label>
        <br />
        </div>
		
		</td>
    </tr>
    <tr valign="top">
      <th scope="row">Lodging Amount</th>
      <td>$
        <input name="lodging_amount" type="text" id="lodging_amount" size="15" value="<?=$_POST['lodging_amount']?>" required />
        <label>
        <input name="special_rate" onclick="switchCalculate();" id="special_rate" type="checkbox" value="1" <?php if(isset($_POST['special_rate'])) echo 'checked="checked"';?> />
        Special Rate.</label>
      </td>
      <td></td>
    </tr>
    <?php
		$result_lineitem= mysql_query("SELECT * FROM ".LINEITEMS." WHERE importance = 'optional' ORDER BY sortby ASC"); //
		while($row_lineitem = mysql_fetch_array($result_lineitem)){
		?>
    <tr valign="top">
      <th scope="row"><p>
          <?=$row_lineitem['name']?>
        </p></th>
      <td><label>
        <input name="<?=$row_lineitem['idname']?>" type="checkbox" id="<?=$row_lineitem['idname']?>" value="<?=$row_lineitem['idname']?>">
        <?=$row_lineitem['title']?>
        (
        <?php if($row_lineitem['type']=="fixed") echo "\$"; ?>
        <?=money($row_lineitem['value'])?>
        <?php if($row_lineitem['type']=="percentage") echo "%"; ?>
        ) </label></td>
      <td></td>
    </tr>
    <?php } ?>
    <tr valign="top">
      <th scope="row"><font color="#FF0000">Calculate</font></th>
      <td><script>
	  function switchCalculate(){
	  if(document.getElementById('special_rate').checked==true){
	  document.getElementById('Calculate').style.display = 'none';
	  document.getElementById('CalculateSpecial').style.display = 'block';
	  }else{
	  document.getElementById('Calculate').style.display = 'block';
	  document.getElementById('CalculateSpecial').style.display = 'none';
	  }
	  
	  }

	  
	  </script>
        <input name="Calculate" type="button" id="Calculate" value="Calculate" onClick="CalculateRateAjax();" class="button" />
        <input name="Calculate" type="button" id="CalculateSpecial" value="Calculate Special Rate" onClick="CalculateSpecialRateAjax();" class="button" style="display:none" />
        <input name="cleaning_fee" type="hidden" id="cleaning_fee" value="<?=get_cleaning_fee($_GET['property_id']);?>" />
      </td>
      <td></td>
    </tr>
    <tr valign="top">
      <th scope="row">Sum Detail
        <input name="salestaxrate" type="hidden" id="salestaxrate" value="<?=get_sales_tax_percentage($_GET['property_id'])?>" />
        <input name="lodgertaxrate" type="hidden" id="lodgertaxrate" value="<?=get_lodger_tax_percentage($_GET['property_id'])?>" /></th>
      <td><textarea name="sub_total_amount" rows="6" id="sub_total_amount" style="width:300px; height:100px;"><?=$_POST['sub_total_amount']?></textarea></td>
      <td width="200"> Method of Payment: <br />
        <select name="payment_method" id="payment_method" onchange="swithPaymentMethodBlock();" required >
          <option value="0" <?php if(isset($_POST['payment_method']) AND $_POST['payment_method']=="0"){echo 'selected="selected"';}?> > - select - </option>
          <option value="Credit Card" <?php if(isset($_POST['payment_method']) AND $_POST['payment_method']=="Credit Card"){echo 'selected="selected"';}?> >Credit Card</option>
          <option value="Check" <?php if(isset($_POST['payment_method']) AND $_POST['payment_method']=="Check"){echo 'selected="selected"';}?> >Check</option>
        </select>
      </td>
    </tr>
    <tr valign="top">
      <th scope="row">Total Amount </th>
      <td>$
        <input name="total_amount" type="text" id="total_amount" size="15" value="<?=$_POST['total_amount']?>" required />
        <input name="total_amount_basic" type="hidden" id="total_amount_basic" value="<?=$_POST['total_amount_basic']?>" />
      </td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="top">
      <th scope="row">Notes</th>
      <td colspan="3"><textarea name="notes" cols="50" id="notes" style="width:800px; height:100px;"><?=$_POST['notes']?></textarea></td>
    </tr>
    <tr valign="top">
      <th scope="row">Housekeeper</th>
      <td height="30"><?php
		$housekeeper = get_property_housekeeper($_GET['property_id']);
		//echo stripslashes(get_housekeeper_name($housekeeper));
		?>
        <select name="housekeeper" id="housekeeper">
          <option value="0" <?php if('0'==$housekeeper){echo 'selected="selected"';} ?>>- NONE -</option>
          <?php
		$resultK= mysql_query("SELECT * FROM ".HOUSEKEEPERS." ORDER BY lastname ASC");
			while($rowK = mysql_fetch_array($resultK)){
			
			echo '<option value="'.$rowK['id'].'"';
			if($rowK['id']==$housekeeper){echo 'selected="selected"';}
			echo '>'.stripslashes($rowK['lastname']).', '.stripslashes($rowK['firstname']).'</option>';
			
			}
		?>
        </select>
        <?php if($housekeeper=="0"){echo "No housekeeper assigned to this property.";}?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <!--
    <tr valign="top">
      <th scope="row">Maintenance:</th>
      <td height="30">
	  <?php
		$maintenance = get_property_maintenance($_GET['property_id']);
		//echo stripslashes(get_maintenance_name($maintenance));
		?>
	  
	  <select name="maintenance" id="maintenance">
		<?php
		$resultM= mysql_query("SELECT * FROM ".MAINTENANCE." ORDER BY lastname ASC");
			while($rowM = mysql_fetch_array($resultM)){
			
			echo '<option value="'.$rowM['id'].'"';
			if($rowM['id']==$maintenance){echo 'selected="selected"';}
			echo '>'.stripslashes($rowM['lastname']).', '.stripslashes($rowM['firstname']).'</option>';
			
			}
		?>
        </select>
        <?php if($maintenance=="0"){echo "No maintenance person assigned to this property.";}?></td>
      <td>&nbsp;</td>
    </tr>
    -->
    <tr valign="top">
      <th scope="row">Status<span style="color:#FF0000">*</span></th>
      <td height="30">
	    
		<div id="credit_card_payment_block_02" style="display:<?php if(isset($_POST['payment_method']) AND $_POST['payment_method']=="Credit Card"){echo 'block';}else{echo 'none';}?>">
		
		<label>
        <input name="status" type="radio" value="B" <?php if(isset($_POST['status']) AND $_POST['status']=="B"){echo "checked";}?> required />
        Booked </label>
        
		</div>
		
		<!---->
        <div id="check_payment_block_02" style="display:<?php if(isset($_POST['payment_method']) AND $_POST['payment_method']=="Check"){echo 'block';}else{echo 'none';}?>">
		<label>
        
		
		
		<input name="status" type="radio" value="P" <?php if(isset($_POST['status']) AND $_POST['status']=="P"){echo "checked";}?> required />
        Pending </label>
        <label>
		
		
        <input name="status" type="radio" value="N" <?php if(isset($_POST['status']) AND $_POST['status']=="N"){echo "checked";}?> required />
        Owner</label>
		
		</div>
		
		
		<div id="select_payment_method_warning" style="display:<?php if(isset($_POST)) echo 'none'; else echo 'block';?>">
		Please select payment method.
		</div>
		
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="top">
      <th scope="row"></th>
      <td>
	  
	  
	  
	  <label>
        <input name="do_not_send_email" type="checkbox" value="1" <?php if(isset($_POST['do_not_send_email'])) echo 'checked="checked"';?> />
        Do not send  any emails.</label></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="top">
      <th scope="row">&nbsp;</th>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr valign="top">
      <th scope="row">&nbsp;</th>
      <td><input type="submit" name="btnAdd" id="btnAdd" value="Reserve" class="button" />
        <input name="property_id" id="property_id" type="hidden" value="<?php echo $_GET['property_id']; ?>" />
        <?php
		$maintenance = get_property_maintenance($_GET['property_id']);
		?>
        <input name="maintenance" id="maintenance" type="hidden" value="<?php echo $maintenance; ?>" />
      </td>
      <td>&nbsp;</td>
      <td><!--<input type="button" name="btnDuplicate" id="btnDuplicate" value="Copy Values" class="button" />-->
      </td>
    </tr>
  </table>
</form>
<?php } //eof else?>
