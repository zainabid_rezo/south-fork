<?php 
header("Content-type: application/x-javascript");
if(file_exists("db-tables.php")){require_once("db-tables.php");}
elseif(file_exists("manager/db-tables.php")){require_once("manager/db-tables.php");}

if(file_exists("site-details.php")){require_once("site-details.php");}
elseif(file_exists("manager/site-details.php")){require_once("manager/site-details.php");}

if(file_exists("functions.php")){require_once("functions.php");}
elseif(file_exists("manager/functions.php")){require_once("manager/functions.php");}

?>
//  // 
//Browser Support Code
function getDaysBetweenDates(d0, d1) {

  var msPerDay = 8.64e7;

  // Copy dates so don't mess them up
  var x0 = new Date(d0);
  var x1 = new Date(d1);

  // Set to noon - avoid DST errors
  x0.setHours(12,0,0);
  x1.setHours(12,0,0);

  // Round to remove daylight saving errors
  return Math.round( (x1 - x0) / msPerDay );
}

function CalculateRateAjax(){
	var date_start = document.getElementById('date_start').value;
	var date_end = document.getElementById('date_end').value;
	var property_id = document.getElementById('property_id').value;
	var days = getDaysBetweenDates(date_start,date_end);

	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
	// Create a function that will receive data sent from the server
	ajaxRequest.onreadystatechange = function(){
		if(ajaxRequest.readyState == 4){
			var ajaxanswer = ajaxRequest.responseText;

			if(is_int(ajaxanswer)){
			var lodgingamount = ajaxanswer;
			
			//var salestaxrate = document.getElementById('salestaxrate').value;
			//var lodgertaxrate = document.getElementById('lodgertaxrate').value;
			//var salestaxamount = lodgingamount*salestaxrate/100;
			//var lodgertaxamount = lodgingamount*lodgertaxrate/100;
			//var salestaxamount = roundNumber(salestaxamount,2);
			//var lodgertaxamount = roundNumber(lodgertaxamount,2);
			var cleaning_fee = document.getElementById('cleaning_fee').value;

			//var totalamount = +lodgingamount + +salestaxamount + +lodgertaxamount;
			//var totalamount = lodgingamount;
			var totalamount = +lodgingamount + +cleaning_fee;
						//alert(totalamount);


			<?php
			  $result_lineitem= mysql_query("SELECT * FROM ".LINEITEMS." ORDER BY sortby ASC") or die(__LINE__.mysql_error()); //WHERE importance = 'optional' 
				while($row_lineitem = mysql_fetch_array($result_lineitem)){
				?>
				<?php
				if($row_lineitem['id'] == '5'){echo 'if(days<28){';}
				?>
				
			<?php if($row_lineitem['importance'] === 'optional'){?>
			//optional
			
			<?php if($row_lineitem['type']=="fixed"){?>
							if(document.getElementById('<?=$row_lineitem['idname']?>').checked == true){
							var <?=$row_lineitem['idname']?> = "<?=$row_lineitem['value']?>";
							totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
							}else{
							var <?=$row_lineitem['idname']?> = "0";
							}
			<?php }elseif($row_lineitem['type']=="percentage"){ ?>
							if(document.getElementById('<?=$row_lineitem['idname']?>').checked == true){
							<?php if($row_lineitem['applyto']=="base"){$applyto = "lodgingamount";}
							elseif($row_lineitem['type']=="percentage"){$applyto = "totalamount";}?>
							var <?=$row_lineitem['idname']?> = <?=$applyto?>*<?php echo $row_lineitem['value']/100; ?>;
							<?=$row_lineitem['idname']?> = roundNumber(<?=$row_lineitem['idname']?>,2);
							totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
							}else{
							var <?=$row_lineitem['idname']?> = "0";
							}
			<?php } ?>
			<?php }elseif($row_lineitem['importance'] === 'mandatory'){?>
			//mandatory
				<?php if($row_lineitem['type']=="fixed"){ ?>
				var <?=$row_lineitem['idname']?> = "<?=$row_lineitem['value']?>";
				totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
				<?php }elseif($row_lineitem['type']=="percentage"){?>

				<?php if($row_lineitem['applyto']=="base"){$applyto = "lodgingamount";}
				elseif($row_lineitem['type']=="percentage"){$applyto = "totalamount";}?>
				var <?=$row_lineitem['idname']?> = <?=$applyto?>*<?php echo $row_lineitem['value']/100; ?>;
				<?=$row_lineitem['idname']?> = roundNumber(<?=$row_lineitem['idname']?>,2);
				totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
				<?php } ?>
			
			<?php } ?>

			<?php
				if($row_lineitem['id'] == '5'){echo '}';}
				?>
			<?php } //while ?>
			cleaning_fee = 1*cleaning_fee;
			cleaning_fee = cleaning_fee.toFixed(2);
			
			lodgingamount = 1*lodgingamount;
			lodgingamount = lodgingamount.toFixed(2);
			
			totalamount = 1*totalamount;
			totalamount = totalamount.toFixed(2);

			var subamounttext = "Lodging: $" + lodgingamount + "\n";

			<?php
			/**/
			  $result_subamount= mysql_query("SELECT * FROM ".LINEITEMS." ORDER BY sortby ASC") or die(__LINE__.mysql_error()); // WHERE importance = 'optional'
				while($row_subamount = mysql_fetch_array($result_subamount)){
				?>
				<?php
				if($row_subamount['id'] == '5') echo 'if(days<28){';
				?>
				<?php if($row_subamount['type']=="fixed"){$valuesymbol = ": \$";}elseif($row_subamount['type']=="percentage"){$valuesymbol =  " ".$row_subamount['value']."%: \$";} ?>
				subamounttext = subamounttext + "<?=$row_subamount['title']?><?=$valuesymbol?>"+ <?=$row_subamount['idname']?> + "\n";
			<?php
				if($row_subamount['id'] == '5'){echo '}';}
				?>
			<?php } //while 
			?>
			
			subamounttext = subamounttext + "Cleaning Fee: $"+ cleaning_fee + "\n";
			
			document.getElementById('lodging_amount').value = lodgingamount; 
			document.getElementById('sub_total_amount').value = subamounttext;
			document.getElementById('total_amount').value = totalamount;
			document.getElementById('total_amount_basic').value = totalamount;
		}else{alert(ajaxanswer)}
		}
	}

	var queryString = "?date_start=" + date_start + "&date_end=" + date_end + "&property_id=" + property_id;
	ajaxRequest.open("GET", "reservations-calculate-rate-ajax.php" + queryString, true);
	ajaxRequest.send(null); 
}

function roundNumber(num, dec) {
	//var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	num = 1*num;
	var result = num.toFixed(dec);
	return result;
}
function is_int(value){ 
   for (i = 0 ; i < value.length ; i++) { 
      if ((value.charAt(i) < '0') || (value.charAt(i) > '9')) return false 
    } 
   return true; 
}