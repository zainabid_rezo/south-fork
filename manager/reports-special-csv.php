<?php 
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
//include_once("authenticate.php");
ini_set('display_errors', '1');

$output = "";

$output .= 'Property,Guest,Date,Lodging Total,Lodging Tax,Sales Tax,Damage Deposit,Cleaning,Total Amount,Damage,Payment 1,Date 1,Payment 2,Date 2';

$output .="\n";

$lodging_total = '0';
$lodger_tax_total = '0';
$sales_tax_total = '0';
$damage_deposit_total = '0';
$cleaning_fee_total = '0';
$maintenance_total = '0';
$total_amount = '0';
$payment_amount_total = '0';
$maintenance_amount_total = '0';

if($_GET['property_id']=="all"){
$sqlP = "SELECT * FROM ".PROPERTIES." ORDER BY title ASC"; // 
}else{
$sqlP = "SELECT * FROM ".PROPERTIES." WHERE id = '".$_GET['property_id']."'"; // 
}
$resP = mysql_query($sqlP) or die(__LINE__.mysql_error());
while($rowP=mysql_fetch_array($resP))
{

$date_start = date("Y-m-d",strtotime($_GET['date_start']));
$date_end = date("Y-m-d",strtotime($_GET['date_end']));

while($date_start!=$date_end){
$sqlG = "SELECT * FROM ".GUESTS." WHERE status='B' AND property_id = '".$rowP['id']."' AND date_start = '$date_start'";//$date_start
$resultG= mysql_query($sqlG) or die(__LINE__.mysql_error());
while($rowG = mysql_fetch_array($resultG)){

$lodging_total += $rowG['lodging_amount'];
$lodger_tax	= ($rowP['lodger_tax_show']=='1') ? $rowP['lodger_tax'] : '0';
$lodger_tax_total += ($rowG['lodging_amount']*$lodger_tax/100);
$sales_tax	= ($rowP['sales_tax_show']=='1') ? $rowP['sales_tax'] : '0';
$sales_tax_total += ($rowG['lodging_amount']*$sales_tax/100);
$cleaning_fee	= ($rowP['cleaning_fee_show']=='1') ? $rowP['cleaning_fee'] : '0';
$cleaning_fee_total += ($cleaning_fee);


	  $damage_deposit = '';
	  $sum_detail = nl2br($rowG['sum_detail']);
	  $sum_detail = explode('<br />',$sum_detail);
	  foreach($sum_detail as $subtotal){
	  $subtotal = explode(':',$subtotal);
	  if(trim($subtotal[0])=='Damage Deposit') $damage_deposit = $subtotal[1];
	  }
	  $damage_deposit = str_replace('$','',$damage_deposit);
	  if(!empty($damage_deposit)) {$damage_deposit_total+=$damage_deposit; $damage_deposit='$'.money($damage_deposit);}





if($rowG['paid_date']<>'0000-00-00'){$paid_date = date("m/d/Y",strtotime($rowG['paid_date']));}else{$paid_date = '';}

$output .= stripslashes($rowP['title']).','.stripslashes($rowG['lastname']).','.date("m/d/Y",strtotime($rowG['date_start'])).','.stripslashes('\$'.money($rowG['lodging_amount'])).','.stripslashes('\$'.money($rowG['lodging_amount']*$lodger_tax/100)).' ('.$lodger_tax.'%),'.stripslashes('\$'.money($rowG['lodging_amount']*$sales_tax/100)).'('.$sales_tax.'%),'.$damage_deposit.','.stripslashes('\$'.money($cleaning_fee)).','.stripslashes('\$'.money($rowG['total_amount'])).',,'.stripslashes('\$'.money($rowG['amount_paid'])).','.$paid_date;

if($rowG['final_payment_received']=='1' AND $rowG['amount_due']!='0'){
$output .= ','.stripslashes('\$'.money($rowG['amount_due']));
$payment_amount_total += $rowG['amount_due'];
if($rowG['final_payment_date']<>'0000-00-00') $output .= ','. date("m/d/Y",strtotime($rowG['final_payment_date']));
}

$total_amount += $rowG['total_amount'];
$payment_amount_total += $rowG['amount_paid'];

$output .="\n";



}




$sqlM = "SELECT * FROM ".MAINTENANCEJOBS." WHERE property_id = '".$rowP['id']."' AND date = '$date_start'";
$resultM= mysql_query($sqlM) or die(__LINE__.mysql_error());
while($rowM = mysql_fetch_array($resultM)){

$maintenance_total += $rowM['amount'];

$output .=    stripslashes($rowP['title']).',,,,,,,,'.stripslashes($rowM['title']).','.stripslashes('\-$'.money($rowM['amount'])).','.date("m/d/Y",strtotime($rowM['date']));

$maintenance_amount_total += $rowM['amount'];
$output .="\n";
  
}

$date_start = date('Y-m-d',strtotime("+1 day",strtotime($date_start)));

}

}

$output .= ',,Total:,'.stripslashes('\$'.money($lodging_total)).','.
stripslashes('\$'.money($lodger_tax_total)).','.
stripslashes('\$'.money($sales_tax_total)).','.
stripslashes('\$'.money($damage_deposit_total)).','.
stripslashes('\$'.money($cleaning_fee_total)).','.
stripslashes('\$'.money($total_amount));

$output .= ',,,,'.stripslashes('\$'.money($payment_amount_total));
$output .="\n";

$output .= ',,,,,,,,,,,'.stripslashes('\-$'.money($maintenance_amount_total));
$output .="\n";
$output .= ',,,,,,,,,,,'.stripslashes('\$'.money($payment_amount_total-$maintenance_amount_total));
$output .="\n";


//echo $output;
//exit;
// Download the file

$filename = "Reports_Special_".date('m-d-Y').".csv";
header('Content-type: application/csv');
header('Content-Disposition: attachment; filename='.$filename);

echo $output;
exit;

?>