<?php

$g_loginname = GATEWAY_LOGIN; // abrar

$g_transactionkey = GATEWAY_PASS; // abrar

$g_apihost = GATEWAY_API_HOST;// abrar

$g_apipath = "/xml/v1/request.api";

$authorize_error = "";

if(GATEWAY_MODE=='live'){

$gateway_mode = 'liveMode';//abrar

}else{

$gateway_mode = 'testMode';//abrar

}

//////////////////////////////////////////////////////////////////////

function authorizedotnet_send_xml_request($content)

{

	global $g_apihost, $g_apipath;

	return authorizedotnet_send_request_via_fsockopen($g_apihost,$g_apipath,$content);

}

 

//function to send xml request via fsockopen

//It is a good idea to check the http status code.

function authorizedotnet_send_request_via_fsockopen($host,$path,$content)

{

	$posturl = "ssl://" . $host;

	$header = "Host: $host\r\n";

	$header .= "User-Agent: PHP Script\r\n";

	$header .= "Content-Type: text/xml\r\n";

	$header .= "Content-Length: ".strlen($content)."\r\n";

	$header .= "Connection: close\r\n\r\n";

	$fp = fsockopen($posturl, 443, $errno, $errstr, 30);

	if (!$fp)

	{

		$body = false;

	}

	else

	{

		error_reporting(E_ERROR);

		fputs($fp, "POST $path  HTTP/1.1\r\n");

		fputs($fp, $header.$content);

		fwrite($fp, $out);

		$response = "";

		while (!feof($fp))

		{

			$response = $response . fgets($fp, 128);

		}

		fclose($fp);

		error_reporting(E_ALL ^ E_NOTICE);

		

		$len = strlen($response);

		$bodypos = strpos($response, "\r\n\r\n");

		if ($bodypos <= 0)

		{

			$bodypos = strpos($response, "\n\n");

		}

		while ($bodypos < $len && $response[$bodypos] != '<')

		{

			$bodypos++;

		}

		$body = substr($response, $bodypos);

	}

	return $body;

}



//function to send xml request via curl

function authorizedotnet_send_request_via_curl($host,$path,$content)

{

	$posturl = "https://" . $host . $path;

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $posturl);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));

	curl_setopt($ch, CURLOPT_HEADER, 1);

	curl_setopt($ch, CURLOPT_POSTFIELDS, $content);

	curl_setopt($ch, CURLOPT_POST, 1);

	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

	$response = curl_exec($ch);

	return $response;

}





//function to parse the api response

//The code uses SimpleXML. http://us.php.net/manual/en/book.simplexml.php 

//There are also other ways to parse xml in PHP depending on the version and what is installed.

function authorizedotnet_parse_api_response($content)

{

	$parsedresponse = simplexml_load_string($content, "SimpleXMLElement", LIBXML_NOWARNING);

	if ("Ok" != $parsedresponse->messages->resultCode) {

		$response_error = '';

		foreach ($parsedresponse->messages->message as $msg) {

			$response_error .= "[" . htmlspecialchars($msg->code) . "] " . htmlspecialchars($msg->text) . "<br>";

		}

	return $response_error;

	}

	return $parsedresponse;

}





///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





function authorize_create_customer_profile($email,$customer_id){

	global $g_loginname, $g_transactionkey, $authorize_error;

//build xml to post



$content =

	"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .

	"<createCustomerProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .

	"<merchantAuthentication>".

        "<name>" . $g_loginname . "</name>".

        "<transactionKey>" . $g_transactionkey . "</transactionKey>".

        "</merchantAuthentication>".

	"<profile>".

	"<merchantCustomerId>".$customer_id."</merchantCustomerId>". // Your own identifier for the customer.

	"<description></description>".

	"<email>" . $email . "</email>".

	"</profile>".

	"</createCustomerProfileRequest>";



$response = authorizedotnet_send_xml_request($content);

$parsedresponse = authorizedotnet_parse_api_response($response);

if ("Ok" == $parsedresponse->messages->resultCode) {

		return htmlspecialchars($parsedresponse->customerProfileId);

}else{

		$authorize_error = $parsedresponse;

		return false;

}



}





function authorize_create_customer_payment_profile($customer_profile_id,$firstname,$lastname,$address,$city,$state,$zip,$phone,$ccnumber,$expyear,$expmonth){

//build xml to post

global $g_loginname, $g_transactionkey, $gateway_mode, $authorize_error;

$content =

	"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .

	"<createCustomerPaymentProfileRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .

	"<merchantAuthentication>".

	"<name>" . $g_loginname . "</name>".

	"<transactionKey>" . $g_transactionkey . "</transactionKey>".

	"</merchantAuthentication>".

	"<customerProfileId>" . $customer_profile_id . "</customerProfileId>".

	"<paymentProfile>".

	"<billTo>".

	 "<firstName>" . $firstname . "</firstName>".

	 "<lastName>" . $lastname . "</lastName>".

	 "<address>" . $address . "</address>".

	 "<city>" . $city . "</city>".

	 "<state>" . $state . "</state>".

	 "<zip>" . $zip . "</zip>".

	 "<country>USA</country>".

	"</billTo>".

	"<payment>".

	 "<creditCard>".

	  "<cardNumber>" . $ccnumber . "</cardNumber>".

	  "<expirationDate>" . $expyear . "-" . $expmonth. "</expirationDate>". // required format for API is YYYY-MM

	 "</creditCard>".

	"</payment>".

	"</paymentProfile>".

	"<validationMode>". $gateway_mode ."</validationMode>".

	"</createCustomerPaymentProfileRequest>";



$response = authorizedotnet_send_xml_request($content);



$parsedresponse = authorizedotnet_parse_api_response($response);

if ("Ok" == $parsedresponse->messages->resultCode) {

	return $parsedresponse->customerPaymentProfileId;

}else{

		$authorize_error = $parsedresponse;

		return false;

}



}



function authorize_create_transaction($amount,$customer_profile_id,$customer_payment_profile_id,$invoice_number,$arrival_date,$departure_date,$property,$price){

global $g_loginname, $g_transactionkey, $authorize_error, $transaction_detail;


$yprice = str_replace(',','',$price);
$myprice = str_replace('.','',$yprice);
$finalprice = str_replace('00','',$myprice);

//build xml to post
$rand = mt_rand(100,100000);
$content =

	"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .

	"<createCustomerProfileTransactionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .

	"<merchantAuthentication>".

	"<name>" . $g_loginname . "</name>".

	"<transactionKey>" . $g_transactionkey . "</transactionKey>".

	"</merchantAuthentication>".

	"<transaction>".

	"<profileTransAuthCapture>".

	"<amount>" . $amount . "</amount>". // should include tax, shipping, and everything.
	"<lineItems>".
	"<itemId>".$rand."</itemId>".
	"<name>".$property."</name>".
	"<description>".'Arival Date:'.$arrival_date.'   Departure Date:'.$departure_date.''."</description>".
	"<quantity>1</quantity>".
	"<unitPrice>".$finalprice."</unitPrice>".
	"</lineItems>".

	"<customerProfileId>" .$customer_profile_id . "</customerProfileId>".

	"<customerPaymentProfileId>" . $customer_payment_profile_id . "</customerPaymentProfileId>".

	"<order>".

	"<invoiceNumber>" . $invoice_number . "</invoiceNumber>".

	"</order>".

	"</profileTransAuthCapture>".

	"</transaction>".

	"</createCustomerProfileTransactionRequest>";



//profileTransAuthOnly

//profileTransAuthCapture





$response = authorizedotnet_send_xml_request($content);

$parsedresponse = authorizedotnet_parse_api_response($response);

if ("Ok" == $parsedresponse->messages->resultCode) {

	return true;

}

elseif (isset($parsedresponse->directResponse)) {

		

	$directResponseFields = explode(",", $parsedresponse->directResponse);

	$responseCode = $directResponseFields[0]; // 1 = Approved 2 = Declined 3 = Error

	$responseReasonCode = $directResponseFields[2]; // See http://www.authorize.net/support/AIM_guide.pdf

	$responseReasonText = $directResponseFields[3];

	$approvalCode = $directResponseFields[4]; // Authorization code

	$transId = $directResponseFields[6];

	

	if ("1" == $responseCode) return true;

	else if ("2" == $responseCode) $authorize_error .= "The transaction was declined.<br>";

	else $authorize_error = "The transaction resulted in an error.<br>";

	$transaction_detail = "responseReasonCode = " . htmlspecialchars($responseReasonCode) . "<br>";

	$transaction_detail .=  "responseReasonText = " . htmlspecialchars($responseReasonText) . "<br>";

	$transaction_detail .=  "approvalCode = " . htmlspecialchars($approvalCode) . "<br>";

	$transaction_detail .=  "transId = " . htmlspecialchars($transId) . "<br>";

}else{

	$authorize_error = $parsedresponse;

	return false;

}



}



