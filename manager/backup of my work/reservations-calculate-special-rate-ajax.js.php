<?php 
header("Content-type: application/x-javascript");
if(file_exists("db-tables.php")){require_once("db-tables.php");}
elseif(file_exists("manager/db-tables.php")){require_once("manager/db-tables.php");}

if(file_exists("site-details.php")){require_once("site-details.php");}
elseif(file_exists("manager/site-details.php")){require_once("manager/site-details.php");}

if(file_exists("functions.php")){require_once("functions.php");}
elseif(file_exists("manager/functions.php")){require_once("manager/functions.php");}

?>
//  // 
//Browser Support Code

function CalculateSpecialRateAjax(){
	document.getElementById("special_rate").checked=true;
	var date_start = document.getElementById('date_start').value;
	var date_end = document.getElementById('date_end').value;
	var rental_total_days = getDaysBetweenDates(date_start,date_end);
		
	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
	// Create a function that will receive data sent from the server
	ajaxRequest.onreadystatechange = function(){
		if(ajaxRequest.readyState == 4){
			var ajaxanswer = ajaxRequest.responseText;

			if(is_int(ajaxanswer)){}else{}
			var lodgingamount = ajaxanswer;
			
			var salestaxrate = document.getElementById('salestaxrate').value;
			var lodgertaxrate = document.getElementById('lodgertaxrate').value;
			var salestaxamount = lodgingamount*salestaxrate/100;
			var lodgertaxamount = lodgingamount*lodgertaxrate/100;
			var salestaxamount = roundNumber(salestaxamount,2);
			var lodgertaxamount = roundNumber(lodgertaxamount,2);
			var cleaning_fee = document.getElementById('cleaning_fee').value;
			
			if(rental_total_days>=30){
			lodgertaxamount = '0';
			salestaxamount = '0';
			}

			//var totalamount = +lodgingamount + +salestaxamount + +lodgertaxamount;
			//var totalamount = lodgingamount;
			//var totalamount = +lodgingamount + +cleaning_fee;
			var totalamount = +lodgingamount + +salestaxamount + +lodgertaxamount + +cleaning_fee;
			//var totalamount = +lodgingamount + +salestaxamount + +cleaning_fee;
						//alert(totalamount);


			<?php
			  $result_lineitem= mysql_query("SELECT * FROM ".LINEITEMS." ORDER BY sortby ASC") or die(__LINE__.mysql_error()); //WHERE importance = 'optional' 
				while($row_lineitem = mysql_fetch_array($result_lineitem)){
				?>
			<?php if($row_lineitem['importance'] === 'optional'){?>
			//optional
			
			<?php if($row_lineitem['type']=="fixed"){?>
							if(document.getElementById('<?=$row_lineitem['idname']?>').checked == true){
							var <?=$row_lineitem['idname']?> = "<?=$row_lineitem['value']?>";
							totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
							}else{
							var <?=$row_lineitem['idname']?> = "0";
							}
			<?php }elseif($row_lineitem['type']=="percentage"){ ?>
							if(document.getElementById('<?=$row_lineitem['idname']?>').checked == true){
							<?php if($row_lineitem['applyto']=="base"){$applyto = "lodgingamount";}
							elseif($row_lineitem['type']=="percentage"){$applyto = "totalamount";}?>
							var <?=$row_lineitem['idname']?> = <?=$applyto?>*<?php echo $row_lineitem['value']/100; ?>;
							<?=$row_lineitem['idname']?> = roundNumber(<?=$row_lineitem['idname']?>,2);
							totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
							}else{
							var <?=$row_lineitem['idname']?> = "0";
							}
			<?php } ?>
			<?php }elseif($row_lineitem['importance'] === 'mandatory'){?>
			//mandatory
				<?php if($row_lineitem['type']=="fixed"){ ?>
				var <?=$row_lineitem['idname']?> = "<?=$row_lineitem['value']?>";
				totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
				<?php }elseif($row_lineitem['type']=="percentage"){?>

				<?php if($row_lineitem['applyto']=="base"){$applyto = "lodgingamount";}
				elseif($row_lineitem['type']=="percentage"){$applyto = "totalamount";}?>
				var <?=$row_lineitem['idname']?> = <?=$applyto?>*<?php echo $row_lineitem['value']/100; ?>;
				<?=$row_lineitem['idname']?> = roundNumber(<?=$row_lineitem['idname']?>,2);
				totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
				<?php } ?>
			
			<?php } ?>


			<?php } //while ?>
			cleaning_fee = 1*cleaning_fee;
			cleaning_fee = cleaning_fee.toFixed(2);
			
			lodgingamount = 1*lodgingamount;
			lodgingamount2 = lodgingamount.toFixed(2);
			
			totalamount = 1*totalamount;
			totalamount = totalamount.toFixed(2);


			var subamounttext = "***Special Rate***\n";
			var subamounttext = subamounttext + "Lodging: $" + lodgingamount2 + "\n";

			<?php
			/**/
			  $result_subamount= mysql_query("SELECT * FROM ".LINEITEMS." ORDER BY sortby ASC") or die(__LINE__.mysql_error()); // WHERE importance = 'optional'
				while($row_subamount = mysql_fetch_array($result_subamount)){
				?>
				<?php if($row_subamount['type']=="fixed"){$valuesymbol = ": \$";}elseif($row_subamount['type']=="percentage"){$valuesymbol =  " ".$row_subamount['value']."%: \$";} ?>
				subamounttext = subamounttext + "<?=$row_subamount['title']?><?=$valuesymbol?>"+ <?=$row_subamount['idname']?> + "\n";
			<?php } //while 
			?>
			
			if(rental_total_days<=29){
			subamounttext = subamounttext + "Lodgers Tax (" + lodgertaxrate + "): $"+ lodgertaxamount + "\n";//abrar
			subamounttext = subamounttext + "Sales Tax (" + salestaxrate + "): $"+ salestaxamount + "\n";//abrar
			}
			
			subamounttext = subamounttext + "Cleaning fee: $"+ cleaning_fee + "\n";
			
			document.getElementById('lodging_amount').value = lodgingamount;
			document.getElementById('sub_total_amount').value = subamounttext;
			document.getElementById('total_amount').value = totalamount;
			document.getElementById('total_amount_basic').value = totalamount;
			
			
		
		}
	}

	var lodging_amount = document.getElementById('lodging_amount').value;
	
	var queryString = "?lodging_amount=" + lodging_amount;
	ajaxRequest.open("GET", "reservations-calculate-special-rate-ajax.php" + queryString, true);
	ajaxRequest.send(null); 
}

function show_recalculate_special(){
	document.getElementById("special_rate").checked=true;
	var date_start = document.getElementById('date_start').value;
	var date_end = document.getElementById('date_end').value;
	var rental_total_days = getDaysBetweenDates(date_start,date_end);
		
	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
	// Create a function that will receive data sent from the server
	ajaxRequest.onreadystatechange = function(){
		if(ajaxRequest.readyState == 4){
			var ajaxanswer = ajaxRequest.responseText;

			if(is_int(ajaxanswer)){}else{}
			var lodgingamount = document.getElementById('lodging_amount_special').value;
			
			var salestaxrate = document.getElementById('salestaxrate').value;
			var lodgertaxrate = document.getElementById('lodgertaxrate').value;
			var salestaxamount = lodgingamount*salestaxrate/100;
			var lodgertaxamount = lodgingamount*lodgertaxrate/100;
			var salestaxamount = roundNumber(salestaxamount,2);
			var lodgertaxamount = roundNumber(lodgertaxamount,2);
			var cleaning_fee = document.getElementById('cleaningfee_special').value;
			
			if(rental_total_days>=30){
			lodgertaxamount = '0';
			salestaxamount = '0';
			}

			//var totalamount = +lodgingamount + +salestaxamount + +lodgertaxamount;
			//var totalamount = lodgingamount;
			//var totalamount = +lodgingamount + +cleaning_fee;
			var totalamount = +lodgingamount + +salestaxamount + +lodgertaxamount + +cleaning_fee;
			//var totalamount = +lodgingamount + +salestaxamount + +cleaning_fee;
            document.getElementById('cleaningfee').value = cleaning_fee;
            document.getElementById('lodging_amount').value = lodgingamount;
						//alert(totalamount);


			<?php
			  $result_lineitem= mysql_query("SELECT * FROM ".LINEITEMS." ORDER BY sortby ASC") or die(__LINE__.mysql_error()); //WHERE importance = 'optional' 
				while($row_lineitem = mysql_fetch_array($result_lineitem)){
				?>
			<?php if($row_lineitem['importance'] === 'optional'){?>
			//optional
			
			<?php if($row_lineitem['type']=="fixed"){?>
							if(document.getElementById('<?=$row_lineitem['idname']?>').checked == true){
							var <?=$row_lineitem['idname']?> = "<?=$row_lineitem['value']?>";
							totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
							}else{
							var <?=$row_lineitem['idname']?> = "0";
							}
			<?php }elseif($row_lineitem['type']=="percentage"){ ?>
							if(document.getElementById('<?=$row_lineitem['idname']?>').checked == true){
							<?php if($row_lineitem['applyto']=="base"){$applyto = "lodgingamount";}
							elseif($row_lineitem['type']=="percentage"){$applyto = "totalamount";}?>
							var <?=$row_lineitem['idname']?> = <?=$applyto?>*<?php echo $row_lineitem['value']/100; ?>;
							<?=$row_lineitem['idname']?> = roundNumber(<?=$row_lineitem['idname']?>,2);
							totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
							}else{
							var <?=$row_lineitem['idname']?> = "0";
							}
			<?php } ?>
			<?php }elseif($row_lineitem['importance'] === 'mandatory'){?>
			//mandatory
				<?php if($row_lineitem['type']=="fixed"){ ?>
				var <?=$row_lineitem['idname']?> = "<?=$row_lineitem['value']?>";
				totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
				<?php }elseif($row_lineitem['type']=="percentage"){?>

				<?php if($row_lineitem['applyto']=="base"){$applyto = "lodgingamount";}
				elseif($row_lineitem['type']=="percentage"){$applyto = "totalamount";}?>
				var <?=$row_lineitem['idname']?> = <?=$applyto?>*<?php echo $row_lineitem['value']/100; ?>;
				<?=$row_lineitem['idname']?> = roundNumber(<?=$row_lineitem['idname']?>,2);
				totalamount = +totalamount + +<?=$row_lineitem['idname']?>;
				<?php } ?>
			
			<?php } ?>


			<?php } //while ?>
			cleaning_fee = 1*cleaning_fee;
			cleaning_fee = cleaning_fee.toFixed(2);
			
			lodgingamount = 1*lodgingamount;
			lodgingamount2 = lodgingamount.toFixed(2);
			
			totalamount = 1*totalamount;
			totalamount = totalamount.toFixed(2);


			var subamounttext = "***Special Rate***\n";
			var subamounttext = subamounttext + "Lodging: $" + lodgingamount2 + "\n";

			<?php
			/**/
			  $result_subamount= mysql_query("SELECT * FROM ".LINEITEMS." ORDER BY sortby ASC") or die(__LINE__.mysql_error()); // WHERE importance = 'optional'
				while($row_subamount = mysql_fetch_array($result_subamount)){
				?>
				<?php if($row_subamount['type']=="fixed"){$valuesymbol = ": \$";}elseif($row_subamount['type']=="percentage"){$valuesymbol =  " ".$row_subamount['value']."%: \$";} ?>
				subamounttext = subamounttext + "<?=$row_subamount['title']?><?=$valuesymbol?>"+ <?=$row_subamount['idname']?> + "\n";
			<?php } //while 
			?>
			
			if(rental_total_days<=29){
			subamounttext = subamounttext + "Lodgers Tax (" + lodgertaxrate + "): $"+ lodgertaxamount + "\n";//abrar
			subamounttext = subamounttext + "Sales Tax (" + salestaxrate + "): $"+ salestaxamount + "\n";//abrar
			}
			
			subamounttext = subamounttext + "Cleaning fee: $"+ cleaning_fee + "\n";
			
			document.getElementById('lodging_amount').value = lodgingamount;
			document.getElementById('sub_total_amount').value = subamounttext;
			document.getElementById('total_amount').value = totalamount;
			document.getElementById('total_amount_basic').value = totalamount;
			
			
		
		}
	}

	var lodging_amount = document.getElementById('lodging_amount').value;
	
	var queryString = "?lodging_amount=" + lodging_amount;
	ajaxRequest.open("GET", "reservations-calculate-special-rate-ajax.php" + queryString, true);
	ajaxRequest.send(null); 
}

function roundNumber(num, dec) {
	//var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	num = 1*num;
	var result = num.toFixed(dec);
	return result;
}
function is_int(value){ 
   for (i = 0 ; i < value.length ; i++) { 
      if ((value.charAt(i) < '0') || (value.charAt(i) > '9')) return false 
    } 
   return true; 
}