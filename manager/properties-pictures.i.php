<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<?php
$property_id = $_GET['property_id'];
$property_name = get_property_name($property_id);

$total_images = 12; // SETTINGS
?>
<script>
function confirmDelete(delUrl) {
  if (confirm("Are you sure you want to delete?")) {
    document.location = delUrl;
  }
}
</script>

<?php
if(isset($_GET['deleteid'])){
$delid = $_GET['deleteid'];

	$result = mysql_query("SELECT * FROM ".PROPERTIESIMAGES." WHERE id = '$delid'");
		while($row = mysql_fetch_array($result)){
			if(!empty($row['image']) AND file_exists("../properties/".$row['image'])){
			unlink("../properties/".$row['image']);
			}
		}
		
	$sql = "UPDATE ".PROPERTIESIMAGES." SET title = '', image = '', status = '0' WHERE id = '$delid'";
			mysql_query($sql) or die(mysql_error());
}
?>
<?php

$query= mysql_query("SELECT * FROM ".PROPERTIESIMAGES." WHERE property_id = '$property_id'"); 
$num_rows = mysql_num_rows($query);
if($num_rows == '0'){

	$sql		=	"INSERT INTO ".PROPERTIESIMAGES." (`property_id`, `image`, `image_number`, `status`, `main`, `created`) VALUES ('$property_id', 'noimage.jpg', '1', '1', '1', NOW());";
	$res		=	mysql_query($sql) or die("Error 55 ".mysql_error());

}
?>
<?php
if(isset($_POST['btnEditDo'])){
//print_r($_POST);
//echo "Updating...<br/>";
$message = "";
$error = "";
$image_loop = 1;
while ($image_loop <= $total_images){
//id	property_id	image_number	title	image	status	sortby	main	created
$title = $_POST["title_$image_loop"];
if(isset($_POST["status_$image_loop"])){$status = "1";}else{$status = "0";}
$main = $_POST["main"];
if($main == $image_loop){$main = "1";}else{$main = "0";}
$sortby = $_POST["sortby_$image_loop"];
$image = $_POST["image_$image_loop"];

$filename = $_FILES["file_$image_loop"]['name'];

	if(!empty($filename)){
		$imgext = strtolower(substr($filename, -4));
		
		if(!empty($property_name)){		
		$img = ereg_replace("[^a-z0-9._]", "",str_replace(" ", "-",str_replace("%20", "-", strtolower($property_name))));
		}else{
		$img = $property_id;
		}
		
		$filename = $img."-".$image_loop.$imgext;
		$savefile = "../properties/".$filename;
		//upload  
		if(copy($_FILES["file_$image_loop"]['tmp_name'], $savefile)){
			//echo "....Image uploaded ";
		}else{echo "Failed to upload image! ";}
		chmod("$savefile",0777);
		
		if(resize_picture("$savefile","$savefile","640","480")){
		//echo "....Image resized ";
		
		}else{
			$error .= "Error: Image resizing failed.";
			$failed = true;
		}
			$image = $filename;
	
	}

		$query= mysql_query("SELECT * FROM ".PROPERTIESIMAGES." WHERE image_number = '$image_loop' AND property_id = '$property_id'"); 
	 	$num_rows = mysql_num_rows($query);
		  if($num_rows == '0'){
			//echo 'INSERT';
			
			$sql		=	"INSERT INTO ".PROPERTIESIMAGES." (property_id,image_number,title,image,status,sortby,main,created) 
			VALUES('$property_id','$image_loop','$title','$image','$status','$sortby','$main',NOW())";
				//echo $sql."<br/>";
				$res		=	mysql_query($sql) or die("Error 55 ".mysql_error());
		  }else{
			
			//echo 'UPDATE';
			
			$sql		=	"UPDATE ".PROPERTIESIMAGES." SET title = '$title', image = '$image', status = '$status', sortby = '$sortby', main = '$main' WHERE image_number = '$image_loop' AND property_id = '$property_id'";
				//echo $sql."<br/>";
				$res		=	mysql_query($sql) or die("Error 60 ".mysql_error());
		  }

$message	= "Successfuly updated.";
$done = true;

unset($title);
unset($filename);
unset($image);
unset($sortby);
unset($status);
unset($main);

$image_loop++;
}

}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link type="text/css" href="css/layout.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/easyTooltip.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/searchbox.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript">
function setActive(id){
document.getElementById('status_'+id).checked = true;
}
</script>

<style type="text/css">
body {
	 background:#fff;
	 margin:20px;
}
</style>
<?php if(isset($done)){ ?>
<SCRIPT LANGUAGE="JavaScript">
	setTimeout("parent.parent.GB_hide();",2000);
	</SCRIPT>
<?php } ?>
</head>
<body>
<?php include"messages-display.php";?>

<?php
$result_ifedit= mysql_query("SELECT * FROM ".PROPERTIESIMAGES." WHERE property_id = '$property_id'");
while($row_ifedit = mysql_fetch_array($result_ifedit)){$edit = 1; }

if(isset($edit)){ ?>
<?php if(!empty($property_name)){?>
<h2>Manage Pictures for: <?=stripslashes($property_name)?></h2>
<?php }else{?>
<h2>Add Pictures</h2>
<?php } ?>

<form action="properties-pictures.php?property_id=<?=$_GET['property_id']?>" method="post" enctype="multipart/form-data">
  <table align="center" class="form-table">
    <tr valign="top">
      <td width="25">#</td>
      <td width="100">Preview</td>
      <td width="150">Title</td>
      <td width="150">File: Dimensions: 640 x 480</td>
      <td width="50">Active</td>
      <td width="50"> Order</td>
      <td width="50">Main</td>
      <td width="50">Delete</td>    
    </tr>
    <?php
	$image_loop = 1;
	while ($image_loop <= $total_images){
	?>
	
	
	<?php
	  $sql_image = "SELECT * FROM ".PROPERTIESIMAGES." WHERE image_number = '$image_loop' AND property_id = '$property_id'";// OR image_number = '0'
	  //echo $sql_image."";
	  $result_image= mysql_query($sql_image); 
	  $num_rows = mysql_num_rows($result_image);
		  if($num_rows != '1'){
		  //echo "empty...<br/>";
			 $image_id = "";
			 $title = "";
			 $image = "";
			 $sortby = "";
			 $status = "";
			 $main = "";
		  }else{
			 while($row_image = mysql_fetch_array($result_image)){
			 $image_id = $row_image['id'];
			 $title = $row_image['title'];
			 $image = $row_image['image'];
			 $sortby = $row_image['sortby'];
			 $status = $row_image['status'];
			 $main = $row_image['main'];
			 }
		  }
	  ?>
	
    <tr valign="top">
      <td valign="middle"><strong><?php echo $image_loop;?>.
        <input name="image_<?=$image_loop?>" type="hidden" id="image_<?=$image_loop?>" value="<?php echo $image;?>" />
      </strong></td>
      <td valign="middle"><?php if(!empty($image) AND file_exists("../properties/$image")){ print_thumb("../properties/$image", "100", "100");}elseif(!empty($image) AND file_exists("../properties/$image")){ print_thumb("../properties/$image", "100", "100");}?></td>
      <td valign="middle"><input name="title_<?=$image_loop?>" type="text" id="title_<?=$image_loop?>" value="<?=$title?>"/></td>
	  <td valign="middle"><input type="file" name="file_<?=$image_loop?>" onClick="javascript:setActive(<?=$image_loop?>)" /></td>
      <td align="center" valign="middle"><input name="status_<?=$image_loop?>" type="checkbox" id="status_<?=$image_loop?>" value="1" <?php if(!isset($status) OR $status=="1"){echo 'checked="checked"';}?> /></td>
      <td align="center" valign="middle"><input name="sortby_<?=$image_loop?>" type="text" id="sortby_<?=$image_loop?>" value="<?php if(empty($sortby)){echo $image_loop;}else{echo $sortby;}?>" size="3"/></td>
      <td align="center" valign="middle"><input name="main" type="radio" value="<?=$image_loop?>" <?php if(isset($main) AND $main=="1"){echo 'checked="checked"';}?> /></td>
      <td align="center" valign="middle">
	  <a href="javascript:confirmDelete('?property_id=<?=$property_id?>&amp;deleteid=<?=$image_id?>')"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
    <?php
	unset($image_id);
	unset($title);
	unset($image);
	unset($sortby);
	unset($status);
	unset($main);
	
	$image_loop++;
	} // while
	?>
    <tr valign="top">
      <td></td>
      <td></td>
      <td></td>
      <td><span class="submit">
        <input type="submit" name="btnEditDo" id="btnEditDo" value="Update" class="button" />
      </span></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
</form>
<?php } ?>




</body>
</html>
