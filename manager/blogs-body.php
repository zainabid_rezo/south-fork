<?php
$imgwidth = "400";
$imgheight = "300";
?>
<?php
if(isset($_POST['btnAdd'])){

$error = "";

	$title		=	addslashes($_POST['title']);
	if(empty($title)) $error .= "Please enter title.<br/>";
	$description		=	addslashes($_POST['description']);
	if(empty($description)) $error .= "Please enter description.<br/>";
	$description_short		=	addslashes($_POST['description_short']);
	if(empty($description_short)) $error .= "Please enter short description.<br/>";
	$status		=	addslashes($_POST['status']);
	
	$date_published		=	date("Y-m-d",strtotime($_POST['date_published']));
	if(empty($date_published)) $error .= "Please enter publish date.<br/>";
	$filename = $_FILES['file']['name'];
	if(empty($filename)) $error .= "Please chose an image file.<br/>";
	
	if(empty($error)){
	$sql		=	"INSERT INTO ".BLOGS." (`title`, `description`, `description_short`, `date_published`, `status`) VALUES ('$title', '$description', '$description_short', '$date_published', '$status')";
	mysql_query($sql) or die(mysql_error());
	$id = $insert_id = mysql_insert_id();
	$success	= "Successfuly added.<br/>";
	 
	 $filename = $_FILES['file']['name'];
		
		if(!empty($filename) && isset($insert_id)){
		$imgext = strtolower(substr($filename, -4));
		
		$img = ereg_replace("[^a-z0-9._]", "",str_replace(" ", "-",str_replace("%20", "-", strtolower($title))));
		
		
		$filename = "blog-".$insert_id."-".$img.$imgext;
		$savefile = "../pictures/".$filename;
		//upload  
		if(copy($_FILES['file']['tmp_name'], $savefile)){
			//echo "....Image uploaded ";
		}else{$warning = "Failed to upload image!<br/>";}
		chmod("$savefile",0777);
		
		if(resize_picture("$savefile","$savefile","$imgwidth","$imgheight")){
		//echo "....Image resized ";
		}else{$warning = "Failed to resize image!<br/>";}
			$image = $filename;
		}
	 
	 if(mysql_query("UPDATE ".BLOGS." SET image='".$image."' WHERE id='".$id."'")){
	 	$success .= "Image added.<br/>";
		unset($_GET);
	 } else {die(mysql_error());}
}

}
?>
<?php
if(isset($_POST['btnEditDo'])){

//echo "Saving...";

$error = "";

	$editid		=	addslashes($_POST['editid']);


	$title		=	addslashes($_POST['title']);
	if(empty($title)) $error .= "Please enter title.<br/>";
	$description		=	addslashes($_POST['description']);
	if(empty($description)) $error .= "Please enter description.<br/>";
	$description_short		=	addslashes($_POST['description_short']);
	if(empty($description_short)) $error .= "Please enter short description.<br/>";
	$status		=	addslashes($_POST['status']);

	$date_published		=	date("Y-m-d",strtotime($_POST['date_published']));
	if(empty($date_published)) $error .= "Please enter publish date.<br/>";
	$image = $_POST['image'];

	if(empty($error))
	{

		$filename = $_FILES['file']['name'];
		
		if(!empty($filename)){
		$imgext = strtolower(substr($filename, -4));
		
		$img = ereg_replace("[^a-z0-9._]", "",str_replace(" ", "-",str_replace("%20", "-", strtolower($title))));
		
		
		$filename = "blog-".$editid."-".$img.$imgext;
		$savefile = "../pictures/".$filename;
		//upload  
		if(copy($_FILES['file']['tmp_name'], $savefile)){
		//echo "....Image uploaded ";
		}else{$warning = "Failed to upload image!<br/>";}
		chmod("$savefile",0777);
		
		if(resize_picture("$savefile","$savefile","$imgwidth","$imgheight")){
		//echo "....Image resized ";
		
		}else{$warning = "Failed to resize image!<br/>";}
			$image = $filename;
     		if(mysql_query("UPDATE ".BLOGS." SET image = '$image' WHERE id = '$editid'")){ $success = "Image uploaded.<br/>"; }
		}


	 if(mysql_query("UPDATE ".BLOGS." SET title = '$title', description = '$description', description_short = '$description_short', date_published = '$date_published', status = '$status' WHERE id = '$editid'")){
	 $success = "Successfully updated.";
	 unset($_GET);
	 
		}else{ die(mysql_error()); }
	
   }

}
?>
<?php if(isset($_GET['delete']) AND !empty($_GET['delete'])){

$delid = $_GET['delete'];

	$result= mysql_query("SELECT * FROM ".BLOGS." WHERE id = '$delid'");
		while($row = mysql_fetch_array($result)){
		if(!unlink("../pictures/".$row['image'])) {$warning = "Can not delete image";}
		}
	$sql		=	"DELETE FROM ".BLOGS." WHERE id = '$delid'";
	mysql_query($sql) or die(mysql_error());
	$success	= "Successfully deleted.<br/>";
	unset($_GET);

}
?>
<!------------------------------------------------------------------------------->
<?php include"messages-display.php";?>
<?php if(isset($_GET['add'])){ ?>
<h2>Add Blog </h2>
  <form action="" method="post" enctype="multipart/form-data">
    <table class="form-table">
      <tr>
        <th>Title<font color="#ff0000">*</font></th>
        <td><input name="title" type="text" value="<?=$_POST['title']?>" size="40" /></td>
      </tr>
      <tr>
        <th>Short Description <font color="#ff0000">*</font></th>
        <td><textarea name="description_short" cols="40"><?=$_POST['description_short']?></textarea></td>
      </tr>
      <tr>
        <th>Description<font color="#ff0000">*</font></th>
        <td><textarea name="description" cols="40"><?=$_POST['description']?></textarea></td>
      </tr>
      <tr>
        <th>Published<font color="#ff0000">*</font></th>
        <td><label>
        <input name="status" type="radio" value="1" <?php if(!isset($_POST['status']) OR $_POST['status']=="1") echo 'checked="checked"';?> />
        Active</label>
        <label>
        <input name="status" type="radio" value="0" <?php if(isset($_POST['status']) AND $_POST['status']=="0") echo 'checked="checked"';?> />
        Inactive </label></td>
      </tr>
      <tr>
        <th>Image<font color="#ff0000">*</font></th>
        <td><input type="file" name="file" /></td>
      </tr>
      <tr>
        <th>&nbsp;</th>
        <td> Dimensions: <?=$imgwidth?> x <?=$imgheight?> (Max: 2MB)&nbsp;<br />
          JPG format is the one recommended.</td>
      </tr>
      <tr>
        <th>Date<font color="#ff0000">*</font></th>
        <td><input name="date_published" id="date_published" type="text" value="<?php if(isset($_POST['date_published'])) echo $_POST['date_published']; else echo date("m/d/Y");?>" size="12" /> <img src="jquery_datepick/calendar.gif" width="16" height="15" /></td>
      </tr>
      <tr>
        <th></th>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th></th>
        <td><input type="submit" name="btnAdd" class="button" value="Add" /></td>
      </tr>
    </table>
  </form>
<?php }
elseif(isset($_GET['editid'])){ ?>
<h2>Edit Blog </h2>
<?php
$id = $_GET['editid'];
$result= mysql_query("SELECT * FROM ".BLOGS." WHERE id = '$id'");
while($row = mysql_fetch_array($result)){
?>
<form action="" method="post" enctype="multipart/form-data">
  <table class="form-table">
    <tr valign="top">
      <th>Title<font color="#ff0000">*</font></th>
      <td><input name="title" type="text" id="title" value="<?=stripslashes($row['title'])?>" size="40" /></td>
    </tr>
    <tr valign="top">
      <th>Short Description<font color="#ff0000">*</font></th>
      <td><textarea name="description_short" cols="40" id="title"><?=stripslashes($row['description_short'])?></textarea></td>
    </tr>
    <tr valign="top">
      <th>Description<font color="#ff0000">*</font></th>
      <td><textarea name="description" cols="40" id="title"><?=stripslashes($row['description'])?></textarea></td>
    </tr>
    <tr valign="top">
      <th>Published<font color="#ff0000">*</font></th>
      <td><label>
        <input name="status" type="radio" value="1" <?php if($row['status']=="1") echo 'checked="checked"';?> />
        Active</label>
        <label>
        <input name="status" type="radio" value="0" <?php if($row['status']=="0") echo 'checked="checked"';?> />
        Inactive </label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><?php print_thumb("../pictures/$row[image]", "50", "50"); ?></td>
    </tr>
    <tr>
      <th>Image<font color="#ff0000">*</font></th>
      <td><input type="file" name="file" /></td>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <td> Dimensions: <?=$imgwidth?> x <?=$imgheight?> (Max: 2MB)&nbsp;<br />
        JPG format is the one recommended.</td>
    </tr>
    <tr>
      <th>Date<font color="#ff0000">*</font></th>
      <td><input name="date_published" type="text" id="date_published" value="<?php if(isset($_POST['date_published'])) echo $_POST['date_published']; else echo date("m/d/Y",strtotime($row['date_published']));?>" size="12" /> <img src="jquery_datepick/calendar.gif" width="16" height="15" /></td>
    </tr>
    <tr>
      <th></th>
      <td><input name="editid" type="hidden" value="<?php echo $row['id']; ?>" />
        <input type="hidden" name="image" value="<? echo $row['image'];?>" />        </td>
    </tr>
    <tr>
      <th></th>
      <td><input type="submit" name="btnEditDo" class="button" value="Update" /></td>
    </tr>
  </table>
</form>
<?php } ?>
<?php }else{ ?>
<h2>View</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
<thead>
  <tr>
    <td width="200">Preview</td>
    <th width="200">Name</th>
    <th width="50" class="headerSortUp">Date</th>
    <td width="50">Edit</td>
    <td width="50">Delete</td>
  </tr>
</thead>
<tbody>
<?php
$result= mysql_query("SELECT * FROM ".BLOGS." ORDER BY date_published ASC") or die(__LINE__.mysql_error());
while($row = mysql_fetch_array($result)){
?>
    <tr>
      <td><?php print_thumb("../pictures/$row[image]", "100", "100"); ?></td>
      <td><?=stripslashes($row['title'])?></td>
      <td><?=date("m/d/Y",strtotime($row['date_published']))?></td>
      <td><a href="?editid=<?=$row['id']?>"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
      <td><a href="javascript:confirmDelete('?delete=<?=$row['id']?>');"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
  <?php } ?>
</tbody>
</table>
<?php } ?>