<?php

define("ADMIN","manager_admin");

define("NAVIGATION","manager_navigation");

define("CMS","cms_posts");

define("HEADERS","headers_posts");

define("ACTIVITIES","activities_posts");

define("BIOS","bios_posts");

define("EMAILARCHIVES","email_archives_posts");

define("CALENDAR","calendar_posts");

define("LINEITEMS","lineitems_posts");

define("PAYMENTS","payments_posts");

define("CATEGORIES","categories_posts");

define("BEDROOMS","bedrooms_posts");

define("BATHROOMS","bathrooms_posts");

define("SLEEPS","sleeps_posts");

define("UNITS","units_posts");

define("PROPERTIES","properties_posts");

define("PROPERTIESEXTENSIONS","properties_extensions_posts");

define("PROPERTIESIMAGES","properties_images");

define("PROPERTIESAMENITIES","properties_amenities");

define("PROPERTIESRATES","properties_rates");

define("AMENITIES","amenities_posts");

define("AMENITIESDROPDOWNVALUES","amenities_dropdown_values");

define("GUESTS","guests_posts");

define("EMAILTEMPLATES","email_templates_posts");

define("RATESSEASONS","rates_seasons_posts");

define("OWNERS","owners_posts");

define("LOCATIONS","locations_posts");

define("HOUSEKEEPERS","housekeepers_posts");

define("MAINTENANCE","maintenance_posts");

define("MAINTENANCEJOBS","maintenance_jobs_posts");

define("MAINTENANCEVENDORS","maintenance_vendors_posts");

define("RENTALSSETUP","rentals_setup_posts");

define("RENTALSCASH","rentals_cash_posts");

define("BLOGS","blogs_posts");



?>