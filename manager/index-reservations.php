<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>
<?=stripslashes(SITE_NAME)?>
</title>
<link type="text/css" href="css/layout.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/easyTooltip.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/searchbox.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<style type="text/css">
<!--
body {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	background:#ffffff;
	color:#000000;
	margin:20px;
}
-->
</style>
</head>
<body id="top">
<!-- Container -->
<div id="container">




<h2>New Reservations</h2>

<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0" style="width:880px">
  <thead>
    <tr>
      <th width="150">Name</td>
      <th width="150">Property</td>
      <th width="100">Created</td>
      <th width="50">Arrival</td>
      <th width="30">Amount</td>
      <td width="30">Edit</td>
      <td width="30">Delete</td>
    </tr>
  </thead>
  <?php
	$result= mysql_query("SELECT * FROM ".GUESTS." WHERE status = 'Z' OR status = 'P' ORDER BY id DESC");
	if(mysql_num_rows($result)=="0"){
	?>
    <tr>
      <td colspan="7">No results found!</td>
    </tr>
	<?php
	}else{
	while($row = mysql_fetch_array($result)){
	?>
    <tr>
      <td><?=stripslashes($row['firstname'])?> <?=stripslashes($row['lastname'])?></td>
      <td><?=stripslashes(get_property_name($row['property_id']))?></td>
      <td><font size="1"><?=date("m/d/Y H:i:s",strtotime($row['created']))?></font></td>
      <td><?=date("m/d/y",strtotime($row['date_start']))?></td>
      <td>$<?=money($row['total_amount'])?></td>
      <td><a href="reservations-view.php?editid=<?=$row['id']?>" target="_blank"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
      <td><a href="javascript:confirmDelete('reservations-view.php?delete=<?=$row['id']?>');" target="_blank"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
  <?php } ?>
  <?php } ?>
</table>





</div>
</body>
</html>