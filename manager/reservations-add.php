<?php

include_once("conn.php");

include_once("db-tables.php");

include_once("site-details.php");

include_once("functions.php");

include_once("authenticate.php");

?>

<!DOCTYPE html>

<html>

<head>

<?php include"head-include.php";?>

<script type="text/javascript">

function ccProcessPopup(guest_id,settled) {



	var deposit_term = document.getElementById("deposit_term").options[document.getElementById("deposit_term").selectedIndex].value;

	

	if(settled=='yes'){

	var totalamount = document.getElementById("total_amount").value;

	

		if(deposit_term=='half'){

		amount = totalamount/2;

		amount = amount.toFixed(2);

		}

	

	}else{

	var amount = '0';

	}



	var property_id = document.getElementById("property_id").value;

	var date_start = document.getElementById("date_start").value;

	var date_end = document.getElementById("date_end").value;





  window.open('reservations-process-cc.php?guest_id='+ guest_id +'&property_id='+ property_id +'&deposit_term=' + deposit_term +'&date_start=' + date_start +'&date_end=' + date_end +'&amount=' + amount +'&totalamount=' + totalamount,'popuppage','width=550,height=500,toolbar=1,resizable=1,scrollbars=yes,top=150,left=400');

}

</script>

<script type="text/javascript">

function swithPaymentMethodBlock(){



var payment_method = document.getElementById("payment_method").options[document.getElementById("payment_method").selectedIndex].value;



if(payment_method == "Credit Card"){

   document.getElementById("credit_card_payment_block").style.display= 'block';

   document.getElementById("check_payment_block").style.display= 'none';

   document.getElementById("credit_card_payment_block_02").style.display= 'block';

   document.getElementById("check_payment_block_02").style.display= 'none';

   document.getElementById("select_payment_method_warning").style.display= 'none';

   

}else if(payment_method == "Check"){

   document.getElementById("credit_card_payment_block").style.display= 'none';

   document.getElementById("check_payment_block").style.display= 'block';

   document.getElementById("credit_card_payment_block_02").style.display= 'none';

   document.getElementById("check_payment_block_02").style.display= 'block';

   document.getElementById("select_payment_method_warning").style.display= 'none';

}

}

</script>







</head>

<body id="top">

<!-- Container -->

<div id="container">

  <!-- Header -->

  <div id="header">

    <!-- Top -->

    <?php include"header-right.php";?>

    <!-- End of Top-->

    <!-- The navigation bar -->

    <div id="navbar">

      <?php include"navigation.php";?>

    </div>

    <!-- End of navigation bar" -->

  </div>

  <!-- End of Header -->

  <!-- Background wrapper -->

  <div id="bgwrap">

    <!-- Main Content -->

    <div id="content">

      <div id="main">

        <?php include"reservations-add-body.php";?>

      </div>

    </div>

    <!-- End of Main Content -->

  </div>

  <!-- End of bgwrap -->

</div>

<!-- End of Container -->

<!-- Footer -->

<div id="footer">

  <?php include"footer.php";?>

</div>

<!-- End of Footer -->

</body>

</html>

