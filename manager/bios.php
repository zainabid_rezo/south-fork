<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<!DOCTYPE html>
<html>
<head>
<?php include"head-include.php";?>
<style type="text/css">
body {
	 background:#fff;
	 margin:20px;
}
</style>
</head>
<body id="top">
<!-- Container -->
<div id="container">
  <!-- Header -->

  <!-- End of Header -->
  <!-- Background wrapper -->
  <div id="bgwrap">
    <!-- Main Content -->
    <div id="content">
      <div id="main">
        <?php include"bios-body.php";?>
      </div>
    </div>
    <!-- End of Main Content -->
  </div>
  <!-- End of bgwrap -->
</div>
<!-- End of Container -->
<!-- Footer -->

<!-- End of Footer -->
</body>
</html>
