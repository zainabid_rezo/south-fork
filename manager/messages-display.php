<?php if(isset($success) AND !empty($success)){?>
<div class="message success close">
  <h2>Congratulations!</h2>
  <p><?=$success?></p>
</div>
<?php } ?>
<?php if(isset($error) AND !empty($error)){?>
<div class="message error close">
  <h2>Error!</h2>
  <p><?=$error?></p>
</div>
<?php } ?>
<?php if(isset($warning) AND !empty($warning)){?>
<div class="message warning close">
  <h2>Warning!</h2>
  <p><?=$warning?></p>
</div>
<?php } ?>
<?php if(isset($information) AND !empty($information)){?>
<div class="message information close">
  <h2>Information</h2>
  <p><?=$information?></p>
</div>
<?php } ?>