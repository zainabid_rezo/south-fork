<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<?php
$property_id = $_GET['property_id'];
?>
<?php
if(isset($_POST['btnAdd'])){

$error = "";
$success = "";

	$title		=	addslashes($_POST['title']);
	$sortby		=	addslashes($_POST['sortby']);
	$status		=	addslashes($_POST['status']);

	if($title=="")
	{
	 $error .="Please enter title.<br>";
	}

	if($sortby=="")
	{
	 $error .="Please enter display order.<br>";
	}

	if($error=="")
	{
	
	$sql		=	"INSERT INTO ".PROPERTIESEXTENSIONS." (`title`, `property_id`, `sortby`, `status`) VALUES ('$title', '$property_id', '$sortby', '$status')";
	mysql_query($sql) or die(__LINE__.mysql_error());
	$insert_id = mysql_insert_id();
	$success	.= 	"Added successfuly.";
	
	
	$resultSame=mysql_query("SELECT * FROM ".PROPERTIESEXTENSIONS." WHERE property_id = '$_GET[property_id]' AND sortby = '$sortby' AND id != '$insert_id' ORDER BY sortby ASC") or die(__LINE__.mysql_error());
	if(mysql_num_rows($resultSame)>"0"){
	
	$resultSb=mysql_query("SELECT * FROM ".PROPERTIESEXTENSIONS." WHERE property_id = '$_GET[property_id]' AND sortby >= '$sortby' AND id != '$insert_id' ORDER BY sortby ASC") or die(__LINE__.mysql_error());
	while($rowSb = mysql_fetch_array($resultSb)){
	
		$sqlUdSb		=	"UPDATE ".PROPERTIESEXTENSIONS." SET sortby = '".($rowSb['sortby']+1)."' WHERE id = '$rowSb[id]'";
		mysql_query($sqlUdSb) or die(__LINE__.mysql_error());

	}
	
	}
	
	
	
	
	unset($_POST);
	$done = true;
	
	}

}
?>
<?php
if(isset($_POST['btnEditDo'])){

$error = "";
$success = "";

	$editid		=	addslashes($_POST['editid']);

	$title		=	addslashes($_POST['title']);
	$sortby		=	addslashes($_POST['sortby']);
	$status		=	addslashes($_POST['status']);

	if($title=="")
	{
	 $error .="Please enter title.<br>";
	}

	if($property_id=="0")
	{
	 $error .="Please select group.<br>";
	}
	
	if($sortby=="")
	{
	 $error .="Please enter display order.<br>";
	}

	if($error=="")
	{

	$sql		=	"UPDATE ".PROPERTIESEXTENSIONS." SET title = '$title', sortby = '$sortby', status = '$status' WHERE id = '$editid'";
	//echo $sql;
	mysql_query($sql) or die(__LINE__.mysql_error());
	$success	= "Successfully updated.";


	$resultSame=mysql_query("SELECT * FROM ".PROPERTIESEXTENSIONS." WHERE property_id = '$_GET[property_id]' AND sortby = '$sortby' AND id != '$editid' ORDER BY sortby ASC") or die(__LINE__.mysql_error());
	if(mysql_num_rows($resultSame)>"0"){
	
	$resultSb=mysql_query("SELECT * FROM ".PROPERTIESEXTENSIONS." WHERE property_id = '$_GET[property_id]' AND sortby >= '$sortby' AND id != '$editid' ORDER BY sortby ASC") or die(__LINE__.mysql_error());
	while($rowSb = mysql_fetch_array($resultSb)){
	
		$sqlUdSb		=	"UPDATE ".PROPERTIESEXTENSIONS." SET sortby = '".($rowSb['sortby']+1)."' WHERE id = '$rowSb[id]'";
		mysql_query($sqlUdSb) or die(__LINE__.mysql_error());

	}

	}



	unset($_POST);
	unset($_GET['editid']);
	$done = true;
	}
	
}
?>
<?php

if(isset($_GET['delete'])){

if(mysql_query("DELETE FROM ".PROPERTIESEXTENSIONS." WHERE id='".$_GET['delete']."'")){
	$success = "Room Deleted.<br/>";
}else{die(__LINE__.mysql_error());}

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link type="text/css" href="css/layout.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/easyTooltip.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/searchbox.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<style type="text/css">
body {
	 background:#fff;
	 margin:20px;
}
</style>
<script type="text/javascript">
function confirmDelete(delUrl) {
  if (confirm("Are you sure you want to delete")) {
    document.location = delUrl;
  }
}
</script>
<?php if(isset($done)){ ?>
<SCRIPT LANGUAGE="JavaScript">
	setTimeout("parent.parent.GB_hide();",30000);
	</SCRIPT>
<?php } ?>
</head>
<body>

<?php include"messages-display.php";?>

<h1><?php echo stripslashes(get_property_name($_GET['property_id']));?></h1>

<?php if(!isset($_GET['editid'])){ ?>

<form action="" method="post" enctype="multipart/form-data">
  <fieldset>
  <legend>
  <h2>Add Room </h2>
  </legend>
  <table class="form-table">
    <tr>
      <th>Title<span> <font color="#FF0000">*</font></span></th>
      <td><input name="title" type="text" id="title" value="<?=$_POST['title']?>" /></td>
    </tr>
    <tr>
      <th align="right" scope="row">Status</th>
      <td><label>
        <input name="status" type="radio" value="1" <?php if(!isset($_POST['status']) OR $_POST['status']=="1") echo 'checked="checked"';?> />
        Active</label>
        <label>
        <input name="status" type="radio" value="0" <?php if(isset($_POST['status']) AND $_POST['status']=="0") echo 'checked="checked"';?> />
        Inactive </label></td>
    </tr>
    <tr>
      <th>Display Order <font color="#FF0000">*</font></th>
      <td><input name="sortby" type="text" id="sortby" value="<?=$_POST['sortby']?>" size="5" /></td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td><input type="submit" name="btnAdd" id="btnAdd" value="Add" class="button" />      </td>
    </tr>
  </table>
  </fieldset>
</form>
<?php }
if(isset($_GET['editid'])){ ?>
<?php
$id = $_GET['editid'];
$result= mysql_query("SELECT * FROM ".PROPERTIESEXTENSIONS." WHERE id = '$id'");

while($row = mysql_fetch_array($result)){
?>
<form action="" method="post" enctype="multipart/form-data">
  <fieldset>
  <legend>
  <h2>Edit Room </h2>
  </legend>
  <table class="form-table">
    <tr>
      <th>Title <font color="#FF0000">*</font></th>
      <td><input name="title" type="text" id="title" value="<?php if(isset($_POST['title'])) {echo $_POST['title'];} else {echo stripslashes($row['title']);}?>" /></td>
    </tr>
    <tr>
      <th align="right" scope="row">Status</th>
      <td><label>
        <input name="status" type="radio" value="1" <?php if(!isset($row['status']) OR $row['status']=="1") echo 'checked="checked"';?> />
        Active</label>
        <label>
        <input name="status" type="radio" value="0" <?php if(isset($row['status']) AND $row['status']=="0") echo 'checked="checked"';?> />
        Inactive </label></td>
    </tr>
    <tr>
      <th>Display Order <font color="#FF0000">*</font></th>
      <td><input name="sortby" type="text" id="sortby" value="<?php if(isset($_POST['sortby'])) {echo $_POST['sortby'];} else {echo stripslashes($row['sortby']);}?>" size="5" /></td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td><input type="submit" name="btnEditDo" id="btnEditDo" value="Update" class="button" />
        <input name="editid" type="hidden" value="<?=$row['id']?>" />      </td>
    </tr>
  </table>
  </fieldset>
</form>
<?php } ?>
<?php } ?>

<h2>View</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
  <thead>
    <tr>
      <th width="200">Name</th>
      <th width="30">Status</th>
      <th width="30" class="headerSortUp">Order</th>
      <td width="50">Edit</td>
      <td width="50">Delete</td>
    </tr>
  </thead>
  <tbody>
    <?php
	$result= mysql_query("SELECT * FROM ".PROPERTIESEXTENSIONS." WHERE property_id = '$property_id' ORDER BY sortby ASC ") or die(__LINE__.mysql_error()); //
	while($row = mysql_fetch_array($result)){
	?>
    <tr>
      <td><?php echo stripslashes(get_property_name($row['property_id']));?> - <strong><?php echo stripslashes($row['title']);?></strong></td>
      <td><?php if($row['status']=="1"){echo "Active";}else{echo "Inactive";}?></td>
      <td><?=$row['sortby']?></td>
      <td><a href="?property_id=<?=$_GET['property_id']?>&editid=<?=$row['id']?>"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
      <td><a href="javascript:confirmDelete('?property_id=<?=$_GET['property_id']?>&delete=<?=$row['id']?>');"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
  <?php } ?>
  </tbody>
  
</table>

</body>
</html>