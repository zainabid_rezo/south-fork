<?php
if(isset($_POST['btnAdd'])){

$error = "";
$success = "";

	$title		=	addslashes($_POST['title']);
	if(empty($title)) $error .= "Please enter title.<br/>";
	$type		=	addslashes($_POST['type']);
	$importance	=	addslashes($_POST['importance']);
	$applyto	=	addslashes($_POST['applyto']);
	$value		=	addslashes($_POST['value']);
	$sortby		=	addslashes($_POST['sortby']);

	if(empty($error)){
	$sql		=	"INSERT INTO ".LINEITEMS." (`title`, `type`, `importance`, `applyto`, `value`, `sortby`) VALUES ('$title', '$type', '$importance', '$applyto', '$value', '$sortby')";
	mysql_query($sql) or die(__LINE__.mysql_error());
	$insertid = mysql_insert_id();
	$idname 	=	ereg_replace("[^a-z0-9._]", "",str_replace(" ", "-",str_replace("%20", "-", strtolower($title)))).$insertid;
	
	$sql2		=	"UPDATE ".LINEITEMS." SET idname = '$idname' WHERE id = '$insertid'";
	$res2		=	mysql_query($sql2) or die(__LINE__.mysql_error());
	$success	= "Line item added successfuly.";
	
	unset($_GET);
	}


}
?>
<?php
if(isset($_POST['btnEditDo'])){

$error = "";
$success = "";

	$editid		=	addslashes($_POST['editid']);
	$title		=	addslashes($_POST['title']);
	$idname 	=	ereg_replace("[^a-z0-9._]", "",str_replace(" ", "-",str_replace("%20", "-", strtolower($title)))).$editid;
	$type		=	addslashes($_POST['type']);
	$importance	=	addslashes($_POST['importance']);
	$applyto	=	addslashes($_POST['applyto']);
	$value		=	addslashes($_POST['value']);
	$sortby		=	addslashes($_POST['sortby']);
	if(empty($error)){
	$sql		=	"UPDATE ".LINEITEMS." SET title = '$title', idname = '$idname', type = '$type', importance = '$importance', applyto = '$applyto', value = '$value', sortby = '$sortby' WHERE id = '$editid'";
	$res		=	mysql_query($sql) or die(mysql_error());
	$success 	= "Line item updated successfuly.";
	unset($_GET);
	}
}
?>
<?php if(isset($_GET['delete']) AND !empty($_GET['delete'])){

	$sql		=	"DELETE FROM ".LINEITEMS." WHERE id = ".$_GET['delete'];
	mysql_query($sql) or die(__LINE__.mysql_error());
	$success 	= "Line item deleted.";
}
?>
<!------------------------------------------------------------------------------->
<?php include"messages-display.php";?>
<?php if(isset($_GET['add'])){ ?>

<form action="" method="post" enctype="multipart/form-data">
  <fieldset>
  <legend>
  <h2>Add Line Item </h2>
  </legend>
  <table class="form-table">
    <tr valign="top">
      <th width="100" scope="row">Title</th>
      <td><input name="title" type="text" id="title" /></td>
    </tr>
    <tr valign="top">
      <th scope="row">Type</th>
      <td><label>
        <input name="type" type="radio" value="fixed" checked="checked" />
        Fixed </label>
        <label>
        <input type="radio" name="type" value="percentage" />
        Percentage</label></td>
    </tr>
    <tr valign="top">
      <th scope="row">If it is percentage, Apply to</th>
      <td><label>
        <input name="applyto" type="radio" value="base" checked="checked" />
        Base</label>
        <label>
        <input type="radio" name="applyto" value="sum" />
        Sum</label></td>
    </tr>
    <tr valign="top">
      <th scope="row">Importance</th>
      <td><label>
        <input name="importance" type="radio" value="mandatory" checked="checked" />
        Mandatory</label>
        <label>
        <input type="radio" name="importance" value="optional" />
        Optional</label></td>
    </tr>
    <tr valign="top">
      <th scope="row">Value</th>
      <td><input name="value" type="text" id="value" size="6" /></td>
    </tr>
    <tr valign="top">
      <th scope="row">Display Order</th>
      <td><input name="sortby" type="text" id="sortby" size="6" /></td>
    </tr>
    <tr valign="top">
      <th scope="row"></th>
      <td><span class="submit">
        <input type="submit" name="btnAdd" id="btnAdd" value="Add" class="button" />
        </span></td>
    </tr>
  </table>
  </fieldset>
</form>
<?php }
elseif(isset($_GET['editid'])){ ?>
<?php
$id = $_GET['editid'];
$result= mysql_query("SELECT * FROM ".LINEITEMS." WHERE id = '$id'");
while($row = mysql_fetch_array($result)){
?>
<form action="" method="post" enctype="multipart/form-data">
  <fieldset>
  <legend>
  <h2>Edit Line Item </h2>
  </legend>
  <table class="form-table">
    <tr valign="top">
      <th scope="row">Title</th>
      <td><input name="title" type="text" id="title" value="<?=stripslashes($row['title'])?>" /></td>
    </tr>
    <tr valign="top">
      <th scope="row">Type</th>
      <td><label>
        <input type="radio" name="type" value="fixed" <?php if($row['type']=="fixed"){echo "checked";}?> />
        Fixed </label>
        <label>
        <input type="radio" name="type" value="percentage" <?php if($row['type']=="percentage"){echo "checked";}?> />
        Percentage </label>
      </td>
    </tr>
    <tr valign="top">
      <th scope="row">If it is percentage, Apply to</th>
      <td><label>
        <input type="radio" name="applyto" value="base" <?php if($row['applyto']=="base"){echo "checked";}?> />
        Base</label>
        <label>
        <input type="radio" name="applyto" value="sum" <?php if($row['applyto']=="sum"){echo "checked";}?> />
        Sum</label></td>
    </tr>
    <tr valign="top">
      <th scope="row">Importance</th>
      <td><label>
        <input type="radio" name="importance" value="mandatory" <?php if($row['importance']=="mandatory"){echo "checked";}?> />
        Mandatory</label>
        <label>
        <input type="radio" name="importance" value="optional" <?php if($row['importance']=="optional"){echo "checked";}?> />
        Optional</label></td>
    </tr>
    <tr valign="top">
      <th scope="row">Value</th>
      <td><input name="value" type="text" id="value" size="6" value="<?=stripslashes($row['value'])?>" /></td>
    </tr>
    <tr valign="top">
      <th scope="row">Display Order</th>
      <td><input name="sortby" type="text" id="sortby" size="6" value="<?=stripslashes($row['sortby'])?>" /></td>
    </tr>
    <tr valign="top">
      <th width="100" scope="row"></th>
      <td><span class="submit">
        <input type="submit" name="btnEditDo" id="btnEditDo" value="Update" class="button" />
        <input name="editid" type="hidden" value="<?php echo $row['id']; ?>" />
        </span></td>
    </tr>
  </table>
  </fieldset>
</form>
<?php } ?>
<?php }else{ ?>
<h2>View</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
  <thead>
    <tr>
      <th width="200">Name</th>
      <th width="50" class="headerSortUp">Order</th>
      <td width="50">Edit</td>
      <td width="50">Delete</td>
    </tr>
  </thead>
  <tbody>
    <?php
$result= mysql_query("SELECT * FROM ".LINEITEMS." ORDER BY sortby ASC");
while($row = mysql_fetch_array($result)){
?>
  <form action="" method="post">
    <tr>
      <td><?=stripslashes($row['title'])?></td>
      <td><?=stripslashes($row['sortby'])?></td>
      <td><a href="?editid=<?=$row['id']?>"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
      <td><a href="javascript:confirmDelete('?delete=<?=$row['id']?>');"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
  </form>
  <?php } ?>
  </tbody>
  
</table>
<?php } ?>
