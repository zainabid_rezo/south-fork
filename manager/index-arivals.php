<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>
<?=stripslashes(SITE_NAME)?>
</title>
<link type="text/css" href="css/layout.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/easyTooltip.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/searchbox.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript">
function print_page(){
	window.print();
}
</script>
<style type="text/css">
<!--
body {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
	background:#ffffff;
	color:#000000;
	margin:20px;
}
-->
</style>
</head>
<body id="top">
<!-- Container -->
<div id="container">



<div style="float:left;">
<h2>New Arrivals</h2>
</div>
<div style="float:right;"><input type="button" value="Print" onClick="print_page();" class="button"></div>

<div style="clear:both;"></div>

<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0" style="width:880px">
  <thead>
    <tr>
      <th width="150">Property Name</td>
      <th width="150">Guest Name</td>
      <th width="50">Arrival Date</td>
      <th width="50">Departure  Date</td>
      <td width="30">Edit</td>
      <td width="30">Delete</td>
    </tr>
  </thead>
  <?php
  $tday = date('Y-m-d');
	$result= mysql_query("SELECT * FROM ".GUESTS." WHERE (status = 'N' OR status = 'B' OR status = 'Z') AND date_start >= '".$tday."' AND archive != 1 ORDER BY date_start ASC");
	if(mysql_num_rows($result)=="0"){
	?>
    <tr>
      <td colspan="7">No results found!</td>
    </tr>
	<?php
	}else{
	while($row = mysql_fetch_array($result)){
	?>
    <tr>
      <td><?=stripslashes(get_property_name($row['property_id']))?></td>
      <td><?=stripslashes($row['firstname'])?> <?=stripslashes($row['lastname'])?></td>
      <td><?=date("m/d/y",strtotime($row['date_start']))?></td>
      <td><?=date("m/d/y",strtotime($row['date_end']))?></td>
      <td><a href="reservations-view.php?editid=<?=$row['id']?>" target="_blank"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
      <td><a href="javascript:confirmDelete('reservations-view.php?delete=<?=$row['id']?>');" target="_blank"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
  <?php } ?>
  <?php } ?>
</table>





</div>
</body>
</html>