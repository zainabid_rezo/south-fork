<?php
$imgwidth = "300";
$imgheight = "225";
?>
<?php
if(isset($_POST['btnAdd'])){

$error = "";

	$title		=	addslashes($_POST['title']);
	if(empty($title)) $error .= "Please enter title.<br/>";
	$sortby		=	addslashes($_POST['sortby']);
	if(empty($sortby)) $error .= "Please enter display order.<br/>";
	$filename = $_FILES['file']['name'];
	if(empty($filename)) $error .= "Please chose an image file.<br/>";
	
	if(empty($error)){
	$sql		=	"INSERT INTO ".CATEGORIES." (`title`, `sortby`, `status`) VALUES ('$title', '$sortby', '1')";
	mysql_query($sql) or die(mysql_error());
	$id = $insert_id = mysql_insert_id();
	$success	= "Successfuly added.<br/>";
	 
	 $filename = $_FILES['file']['name'];
		
		if(!empty($filename) && isset($insert_id)){
		$imgext = strtolower(substr($filename, -4));
		
		$img = ereg_replace("[^a-z0-9._]", "",str_replace(" ", "-",str_replace("%20", "-", strtolower($title))));
		
		
		$filename = "category-".$insert_id."-".$img.$imgext;
		$savefile = "../pictures/".$filename;
		//upload  
		if(copy($_FILES['file']['tmp_name'], $savefile)){
			//echo "....Image uploaded ";
		}else{$warning = "Failed to upload image!<br/>";}
		chmod("$savefile",0777);
		
		if(resize_picture("$savefile","$savefile","$imgwidth","$imgheight")){
		//echo "....Image resized ";
		}else{$warning = "Failed to resize image!<br/>";}
			$image = $filename;
		}
	 
	 if(mysql_query("UPDATE ".CATEGORIES." SET image='".$image."' WHERE id='".$id."'")){
	 	$success .= "Image added.<br/>";
		unset($_GET);
	 } else {die(mysql_error());}
}

}
?>
<?php
if(isset($_POST['btnEditDo'])){

//echo "Saving...";

$error = "";

	$editid		=	addslashes($_POST['editid']);


	$title		=	addslashes($_POST['title']);
	if(empty($title)) $error .= "Please enter title.<br/>";
	$sortby		=	addslashes($_POST['sortby']);
	if(empty($sortby)) $error .= "Please enter display order.<br/>";
	$image = $_POST['image'];

	if(empty($error))
	{


		$filename = $_FILES['file']['name'];
		
		if(!empty($filename)){
		$imgext = strtolower(substr($filename, -4));
		
		$img = ereg_replace("[^a-z0-9._]", "",str_replace(" ", "-",str_replace("%20", "-", strtolower($title))));
		
		
		$filename = "category-".$editid."-".$img.$imgext;
		$savefile = "../pictures/".$filename;
		//upload  
		if(copy($_FILES['file']['tmp_name'], $savefile)){
		//echo "....Image uploaded ";
		}else{$warning = "Failed to upload image!<br/>";}
		chmod("$savefile",0777);
		
		if(resize_picture("$savefile","$savefile","$imgwidth","$imgheight")){
		//echo "....Image resized ";
		
		}else{$warning = "Failed to resize image!<br/>";}
			$image = $filename;
     		if(mysql_query("UPDATE ".CATEGORIES." SET image = '$image' WHERE id = '$editid'")){ $success = "Image uploaded.<br/>"; }
		}



     if(mysql_query("UPDATE ".CATEGORIES." SET title = '$title', sortby = '$sortby' WHERE id = '$editid'")){
	 $success = "Successfully updated.";
	 unset($_GET);
	 
		}else{ echo mysql_error(); }
	
   }

}
?>
<?php if(isset($_GET['delete']) AND !empty($_GET['delete'])){

$delid = $_GET['delete'];

	$result= mysql_query("SELECT * FROM ".CATEGORIES." WHERE id = '$delid'");
		while($row = mysql_fetch_array($result)){
		if(!unlink("../pictures/".$row['image'])) {$warning = "Can not delete image";}
		}
	$sql		=	"DELETE FROM ".CATEGORIES." WHERE id = '$delid'";
	mysql_query($sql) or die(mysql_error());
	$success	= "Successfully deleted.<br/>";
	unset($_GET);

}
?>
<!------------------------------------------------------------------------------->
<?php include"messages-display.php";?>
<?php if(isset($_GET['add'])){ ?>
<fieldset>
<legend><h2>Add Category</h2></legend>
  <form action="" method="post" enctype="multipart/form-data">
    <table class="form-table">
      <tr>
        <th>Title<font color="#ff0000">*</font></th>
        <td><input name="title" type="text" value="<?=$_POST['title']?>" size="40" /></td>
      </tr>
      <tr>
        <th>Image<font color="#ff0000">*</font></th>
        <td><input type="file" name="file" /></td>
      </tr>
      <tr>
        <th>&nbsp;</th>
        <td> Dimensions: <?=$imgwidth?> x <?=$imgheight?> (Max: 2MB)&nbsp;<br />
          JPG format is the one recommended.</td>
      </tr>
      <tr>
        <th>Display Order<font color="#ff0000">*</font></th>
        <td><input name="sortby" type="text" value="<?=$_POST['sortby']?>" size="3" /></td>
      </tr>
      <tr>
        <th></th>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <th></th>
        <td><input type="submit" name="btnAdd" class="button" value="Add" /></td>
      </tr>
    </table>
  </form>
</fieldset>
<?php }
elseif(isset($_GET['editid'])){ ?>
<fieldset>
<legend><h2>Edit Category</h2></legend>
<?php
$id = $_GET['editid'];
$result= mysql_query("SELECT * FROM ".CATEGORIES." WHERE id = '$id'");
while($row = mysql_fetch_array($result)){
?>
<form action="" method="post" enctype="multipart/form-data">
  <table class="form-table">
    <tr valign="top">
      <th>Title<font color="#ff0000">*</font></th>
      <td><input name="title" type="text" id="title" value="<?=stripslashes($row['title'])?>" size="40" /></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><?php print_thumb("../pictures/$row[image]", "50", "50"); ?></td>
    </tr>
    <tr>
      <th>Image<font color="#ff0000">*</font></th>
      <td><input type="file" name="file" /></td>
    </tr>
    <tr>
      <th>&nbsp;</th>
      <td> Dimensions: <?=$imgwidth?> x <?=$imgheight?> (Max: 2MB)&nbsp;<br />
        JPG format is the one recommended.</td>
    </tr>
    <tr>
      <th>Display Order<font color="#ff0000">*</font></th>
      <td><input name="sortby" type="text" id="sortby" value="<?=stripslashes($row['sortby'])?>" size="3" /></td>
    </tr>
    <tr>
      <th></th>
      <td><input name="editid" type="hidden" value="<?php echo $row['id']; ?>" />
        <input type="hidden" name="image" value="<? echo $row['image'];?>" />        </td>
    </tr>
    <tr>
      <th></th>
      <td><input type="submit" name="btnEditDo" class="button" value="Update" /></td>
    </tr>
  </table>
</form>
</fieldset>
<?php } ?>
<?php }else{ ?>
<h2>View</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
<thead>
  <tr>
    <td width="200">Preview</td>
    <th width="200">Name</th>
    <th width="50" class="headerSortUp">Order</th>
    <td width="50">Edit</td>
    <td width="50">Delete</td>
  </tr>
</thead>
<tbody>
<?php
$result= mysql_query("SELECT * FROM ".CATEGORIES." ORDER BY sortby ASC");
while($row = mysql_fetch_array($result)){
?>
    <tr>
      <td><?php print_thumb("../pictures/$row[image]", "100", "100"); ?></td>
      <td><?=stripslashes($row['title'])?></td>
      <td><?=stripslashes($row['sortby'])?></td>
      <td><a href="?editid=<?=$row['id']?>"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
      <td><a href="javascript:confirmDelete('?delete=<?=$row['id']?>');"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
  <?php } ?>
</tbody>
</table>
<?php } ?>