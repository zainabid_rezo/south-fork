<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
include_once("authenticate.php");
?>
<?php
$sub_menu_group ="Property Amenities";
$property_id = $_GET['property_id'];
?>
<?php
if(isset($_POST['btnAdd'])){
$message = "";

//echo "Inserting...<br/>";

$result= mysql_query("SELECT * FROM ".AMENITIES." ORDER BY sortby ASC") or die(__LINE__.mysql_error());
while($row = mysql_fetch_array($result)){
$amenity_id  = $row['id'];
if(!isset($_POST["value_$amenity_id"])){
$value = "No";
}else{
$value = $_POST["value_$amenity_id"];
}

$sql		=	"INSERT INTO ".PROPERTIESAMENITIES." (property_id,amenity_id,value) VALUES ('$property_id','$amenity_id','$value')";
$res		=	mysql_query($sql) or die(__LINE__.mysql_error());
$message	= "Added successfuly.";
$done = true;

}

	
}
?>
<?php
if(isset($_POST['btnEditDo'])){

//echo "Updating...<br/>";

$result= mysql_query("SELECT * FROM ".AMENITIES." ORDER BY sortby ASC") or die(__LINE__.mysql_error());
while($row = mysql_fetch_array($result)){

$amenity_id  = $row['id'];
if(!isset($_POST["value_$amenity_id"])){
$value = "No";
}else{
$value = $_POST["value_$amenity_id"];
}

		$query= mysql_query("SELECT * FROM ".PROPERTIESAMENITIES." WHERE amenity_id = '$row[id]' AND property_id = '$property_id'") or die(__LINE__.mysql_error());
	 	$num_rows = mysql_num_rows($query);
		  if($num_rows != '1'){
			$sql		=	"INSERT INTO ".PROPERTIESAMENITIES." (property_id,amenity_id,value) VALUES ('$property_id','$amenity_id','$value')";
				$res		=	mysql_query($sql) or die(__LINE__.mysql_error());
		  }else{
			$sql		=	"UPDATE ".PROPERTIESAMENITIES." SET value = '$value' WHERE amenity_id = '$amenity_id' AND property_id = '$property_id' ";
				$res		=	mysql_query($sql) or die(__LINE__.mysql_error());
		  }



$message	= "Successfuly updated.";
$done = true;


}

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link type="text/css" href="css/layout.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/easyTooltip.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/searchbox.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<style type="text/css">
body {
	 background:#fff;
	 margin:20px;
}
</style>
<?php if(isset($done)){ ?>
<SCRIPT LANGUAGE="JavaScript">
	setTimeout("parent.parent.GB_hide();",2000);
	</SCRIPT>
<?php } ?>
</head>
<body>
<?php include"messages-display.php";?>

<?php


$result_ifedit= mysql_query("SELECT * FROM ".PROPERTIESAMENITIES." WHERE property_id = '$property_id'");
while($row_ifedit = mysql_fetch_array($result_ifedit)){$edit = 1; }



if(isset($edit)){ ?>
<h2>Manage Amenities for: <?php echo get_property_name($property_id); ?></h2>
<form action="" method="post" enctype="multipart/form-data">
  <table align="center" class="form-table">
    <?php
			$resultA= mysql_query("SELECT * FROM ".AMENITIES." ORDER BY grouped DESC, sortby ASC ");
			while($rowA = mysql_fetch_array($resultA)){
			$amenitytype = $rowA['type'];
			?>
    <tr valign="top">
      <th width="240" bgcolor="#efefef" scope="row" style="padding:4px;"><?php echo stripslashes($rowA['title']);?></th>
	  <?php
	  $result_child= mysql_query("SELECT * FROM ".PROPERTIESAMENITIES." WHERE amenity_id = '$rowA[id]' AND property_id = '$property_id'"); 
	  $num_rows = mysql_num_rows($result_child);
		  if($num_rows != '1'){
			$value = "";
		  }else{
			 while($row_child = mysql_fetch_array($result_child)){$value = $row_child['value']; }
		  }
	  ?>
      <td width="378">
	  <?php if($amenitytype=="InputText"){?>
	  <input name="value_<?=$rowA['id']?>" type="text" size="12" value="<?=$value?>"/>
	  <?php }elseif($amenitytype=="CheckBox"){?>
	  <input name="value_<?=$rowA['id']?>" type="checkbox" value="Yes" <?php if($value=="Yes") echo 'checked="checked"';?> />
	  <?php }elseif($amenitytype=="Dropdown"){?>
	  <select name="value_<?=$rowA['id']?>">
			<option value="<?=$rowAA['value']?>" <?php if(empty($rowAA['value']) OR $rowAA['value']=="0") echo 'selected="selected" ';?>>none</option>
			<?php 
			$resultAA= mysql_query("SELECT * FROM ".AMENITIESDROPDOWNVALUES." WHERE amenity_id = '".$rowA['id']."' ORDER BY id ASC");
			while($rowAA = mysql_fetch_array($resultAA)){
				echo '<option value="'.$rowAA['value'].'" ';
				if($rowAA['value']==$value){echo 'selected="selected" ';}
				echo '>'. $rowAA['value'] .'</option>';
			}
			?>
	    </select>	  
		<?php } ?>	  
	  </td>
    </tr>
    <?php
			}
			?>
    <tr valign="top">
      <th scope="row"></th>
      <td><span class="submit">
        <input type="submit" name="btnEditDo" id="btnEditDo" value="Update" class="button" />
      </span></td>
    </tr>
  </table>
</form>
<?php }else{ ?>
<h2>Add Amenities for: (<?php echo get_property_name($property_id); ?>)</h2>

<form action="" method="post" enctype="multipart/form-data">
  <table align="center" class="form-table">
    <?php
			$resultA= mysql_query("SELECT * FROM ".AMENITIES." ORDER BY grouped DESC, sortby ASC ");
			while($rowA = mysql_fetch_array($resultA)){
			$amenitytype = $rowA['type'];
			?>
    <tr valign="top">
      <th width="240" bgcolor="#efefef" scope="row" style="padding:4px;"><?php echo stripslashes($rowA['title']);?></th>
      <td width="378"><?php if($amenitytype=="InputText"){?>
	  <input name="value_<?=$rowA['id']?>" type="text" size="12" />
	  <?php }elseif($amenitytype=="CheckBox"){?>
	  <input name="value_<?=$rowA['id']?>" type="checkbox" value="Yes" <?php if($value=="Yes") echo 'checked="checked"';?> />
	  <?php }elseif($amenitytype=="Dropdown"){?>
	  <select name="value_<?=$rowA['id']?>">
			<option value="0">None</option>
			<?php 
			$resultAA= mysql_query("SELECT * FROM ".AMENITIESDROPDOWNVALUES." WHERE amenity_id = '".$rowA['id']."' ORDER BY id ASC");
			while($rowAA = mysql_fetch_array($resultAA)){
				echo '<option value="'.$rowAA['value'].'" ';
				echo '>'. $rowAA['value'] .'</option>';
			}
			?>
	    </select>	  
		<?php } ?>	  
	  </td>
    </tr>
    <?php
			}
			?>
    <tr valign="top">
      <th scope="row"></th>
      <td><span class="submit">
        <input type="submit" name="btnAdd" id="btnAdd" value="Add" class="button" />
      </span></td>
    </tr>
  </table>
</form>


<?php } ?>

</body>
</html>