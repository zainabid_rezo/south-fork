function CalculateAdditionalRooms(){
var totalamount = document.getElementById('total_amount_basic').value;
var total_rooms = document.getElementById("total_rooms").options[document.getElementById("total_rooms").selectedIndex].value;
if(totalamount=="0" || totalamount==""){
			alert('You must calculate the price first');
}else{
			if(document.getElementById('ifadditionalrooms').checked==false){
				document.getElementById('total_rooms_display').style.display = 'none';
				document.getElementById('total_amount').value = document.getElementById('total_amount_basic').value;
			}else{
				document.getElementById('total_rooms_display').style.display = 'block';
				total_rooms = total_rooms*1;
				totalamount = totalamount*total_rooms;
				document.getElementById('total_amount').value = roundNumber(totalamount,2);
			}
	}
}

function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}