<?php
if(isset($_POST['btnUpdate']))
{
$error = "";
$site_name = addslashes($_POST['site_name']);
if(empty($site_name)) $error .= "Please enter site name.<br/>";
$site_url = addslashes($_POST['site_url']);
if(!filter_var($site_url, FILTER_VALIDATE_URL))	$error .="Please enter correct url<br>";

$admin_firstname = addslashes($_POST['admin_firstname']);
if(empty($admin_firstname)) $error .= "Please enter first name.<br/>";
$admin_lastname = addslashes($_POST['admin_lastname']);
if(empty($admin_lastname)) $error .= "Please enter last name.<br/>";
$admin_email = addslashes($_POST['admin_email']);
if(empty($admin_email)) $error .= "Please enter email.<br/>";
if(!filter_var($admin_email, FILTER_VALIDATE_EMAIL)) $error .= 'Please enter correct email address<br />';

$username = addslashes($_POST['username']);
$password = addslashes($_POST['passcode']);
if(!empty($password) AND (strlen($password)<4)) $error .= "Your password must consist at least 4 characters.<br/>";
$password_confirm = addslashes($_POST['passcode_confirm']);
if($password != $password_confirm) $error .="Password does not match.<br>";
$gateway_mode = addslashes($_POST['gateway_mode']);

if(empty($error)){


	$sql = "UPDATE ".ADMIN." SET site_name = '$site_name', site_url='$site_url', admin_firstname = '$admin_firstname', admin_lastname = '$admin_lastname', admin_email='$admin_email', username='$username', gateway_mode='$gateway_mode' WHERE id = '1'";
	if(mysql_query($sql)){$success = "Successfully updated.";}else{ $error = "Error: ".__LINE__.": ".mysql_error();};

	if(!empty($password)){
	
	$sql = "UPDATE ".ADMIN." SET password='$password' WHERE id = '1'";
	if(mysql_query($sql)){$success = "Successfully updated.";}else{ $error = "Error: ".__LINE__.": ".mysql_error();};

	}

}

}
?>

<h1>Settings!</h1>
<?php include"messages-display.php";?>
<?php
$sql = "SELECT * FROM ".ADMIN." WHERE id = '1'";
$res = mysql_query($sql) or die("Error:".__LINE__.mysql_error());
while($row=mysql_fetch_array($res))
{
?>
<form id="update-form" name="update-form" method="post" action="" autocomplete="off">
  <table class="form-table">
    <tr>
      <th scope="row">Site Name </th>
      <td><input name="site_name" type="text" value="<?=stripslashes($row['site_name'])?>" size="35" />
      </td>
    </tr>
	<tr>
      <th scope="row">First Name </th>
      <td><input name="admin_firstname" type="text" value="<?=stripslashes($row['admin_firstname'])?>" size="20" />      </td>
    </tr>
    <tr>
      <th scope="row">Last Name </th>
      <td><input name="admin_lastname" type="text" value="<?=stripslashes($row['admin_lastname'])?>" size="20" />      </td>
    </tr>
    <tr>
      <th scope="row">Email </th>
      <td><input name="admin_email" type="text" value="<?=stripslashes($row['admin_email'])?>" size="35" /></td>
    </tr>
    <tr>
      <th scope="row">Site URL </th>
      <td><input name="site_url" type="text" value="<?=stripslashes($row['site_url'])?>" size="35" /></td>
    </tr>
    <tr>
      <th scope="row">Login</th>
      <td><input name="username" type="text" value="<?=stripslashes($row['username'])?>" size="20" /></td>
    </tr>
    <tr>
      <th scope="row">Password</th>
      <td><input name="passcode" id="passcode" type="password" value="" size="20" onKeyUp="matchPassword()" onblur="matchPassword()" />
        Only add a passsword if you want to change the current password.</td>
    </tr>
    <tr>
      <th scope="row">Confirm Password</th>
      <td><input name="passcode_confirm" id="passcode_confirm" type="password" value="" size="20" onKeyUp="matchPassword()" onblur="matchPassword()" />
        <span id="passwordmatch"></span>
        <script language="JavaScript" type="text/JavaScript">
				function matchPassword() {
				    if (document.getElementById('passcode').value != document.getElementById('passcode_confirm').value)
					{	
						document.getElementById('passwordmatch').innerHTML = "<font color=\"#ff0000\">Password does not match.</font>";
						return false;
					} else {
						document.getElementById('passwordmatch').innerHTML = "<img src=\"http://wolfwebdevelopment.com/API/ok.gif\" />";
						return true;
					}
				}
				</script>
      </td>
    </tr>
    <tr>
      <th>Gateway Mode </th>
      <td colspan="2"><label>
        <input name="gateway_mode" type="radio" value="live" <?php if($row['gateway_mode']=="live") echo 'checked="checked"';?> />
        Live</label>
        <label>
        <input name="gateway_mode" type="radio" value="demo" <?php if($row['gateway_mode']=="demo") echo 'checked="checked"';?> />
        Demo </label></td>
    </tr>
    <tr>
      <td></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td><a href="settings-setup-questions.php" rel="gb_page_center[415, 270]">Setup Security Question</a>			</td>
    </tr>
    <tr>
      <td></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td><a href="settings-smtp.php" rel="gb_page_center[400, 400]">SMTP Settings</a></td>
    </tr>
    <tr>
      <td></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td><a href="settings-database-backup.php" rel="gb_page_center[400, 400]">Database Backup Settings</a></td>
    </tr>
    <tr>
      <td></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td><input type="submit" name="btnUpdate" value="Update" class="button" /></td>
    </tr>
  </table>
</form>
<?php } ?>
