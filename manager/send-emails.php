<?php

include_once("conn.php");

include_once("db-tables.php");

include_once("site-details.php");

include_once("functions.php");

?>

<?php

$showform = 'yes';

function smtpmailerDynamic($to, $from, $from_name, $subject, $body, $ishtml = true, $is_gmail = false) {

/*			$headers = "MIME-Version: 1.0" . "\r\n";

 			$headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";

			$headers .= "From: $from_name <$from>" . "\r\n";

			mail($to,$subject,$body,$headers);

			return true;

*/



	global $emailwarning;

	global $emailsuccess;

	$mail = new PHPMailer();

	$mail->IsSMTP();

	$mail->IsHTML($ishtml);

	$mail->SMTPAuth = true; 

	if ($is_gmail) {

		$mail->SMTPSecure = 'ssl'; 

		$mail->Host = 'localhost';//'smtp.gmail.com';

		$mail->Port = 25; //465;  

		$mail->Username = 'wolfwebdevelopment@gmail.com';  

		$mail->Password = 'password';   

	} else {

		$mail->Host = $_SESSION['SMTPSERVER'];

		$mail->Username = $_SESSION['SMTPUSER'];  

		$mail->Password = $_SESSION['SMTPPWD'];

		$mail->Port = $_SESSION['SMTPPORT'];  

	}        

	$mail->SetFrom($from, $from_name);

	$mail->Subject = $subject;

	$mail->Body = $body;

	$mail->ClearAddresses();

	$mail->AddAddress($to);

	if(!$mail->Send()) {

		$emailwarning .= 'Mail error: '.$mail->ErrorInfo;

		return false;

	} else {

		$emailsuccess .= 'Email Message sent!';

		return true;

	}

}


function send_credit_card_payment_due_notice_to_guest($to_email,$firstname,$lastname,$date_start,$date_end,$due_date,$amount_due,$property,$guest_id){



		get_email_template(14);

		$property = get_property_name($property);

		

			$searchvariables  = array('[firstname]','[lastname]','[due_date]','[date_start]','[date_end]','[amount_due]','[property]','[sitename]');

			$replacevariables = array($firstname,$lastname,$due_date,$date_start,$date_end,money($amount_due),$property,SITE_NAME);

			$msg1 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY_FIXED']);

			$msg1 = nl2br($msg1);
			
			$msg3 = 'This is the final payment for your reservation on '.$property.' arriving on '.$date_start.' and departing on '.$date_end.'.';

			$msg2 = str_replace($searchvariables, $replacevariables, $_SESSION['EMAIL_BODY']);

			$msg = $msg1."<br/>".$msg3."<br/><br/>".$msg2;

			 $msg = html_entity_decode($msg);

		
			

		if (smtpmailerDynamic($to_email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) {
				save_email_archive($to_email, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg,'sent',$guest_id,'Customer','');
			

					if (smtpmailerDynamic(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg)) {
						
						save_email_archive(ADMIN_EMAIL, $_SESSION['EMAIL_FROM'], $_SESSION['EMAIL_FROM_NAME'], $_SESSION['EMAIL_SUBJECT'], $msg,'sent',$guest_id,'ADMIN','');

						return true;

					} else {

						return false;

					}

			

		} else {

			return false;

		}/**/



	}




if(isset($_GET['send_email']))

{

$error = "";


		$send_email = $_GET['send_email'];
		$guest_id = $_GET['guest_id'];
		
		if($send_email == 12){
			$result= mysql_query("SELECT * FROM ".GUESTS." WHERE id = '$guest_id'") or die(mysql_error());
			$row = mysql_fetch_array($result);
			
			$email = $row['email'];
			$firstname = $row['firstname'];
			$lastname = $row['lastname'];
			$adults = $row['adults'];
			$children = $row['children'];
			$pets = $row['pets'];
			$sum_detail = $row['sum_detail'];
			$total_amount = $row['total_amount'];
			$date_start = date('m/d/Y',strtotime($row['date_start']));
			$date_end = date('m/d/Y',strtotime($row['date_end']));
			$property_id = $row['property_id'];
			$coupon_dis = 0;
			
			
			sendbookedemail100cc($email,$guest_id,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property_id,$coupon_dis,$send_email);
		}
		
		elseif($send_email == 5){
			$result= mysql_query("SELECT * FROM ".GUESTS." WHERE id = '$guest_id'") or die(mysql_error());
			$row = mysql_fetch_array($result);
			
			$seel_pay = mysql_query("SELECT * FROM ".PAYMENTS." WHERE transaction_status = 'Pending' AND type = 'vault' and guest_id = '".$row['id']."'") or die(mysql_error());
			$row_pay = mysql_fetch_array($seel_pay);
			
			$email = $row['email'];
			$firstname = $row['firstname'];
			$lastname = $row['lastname'];
			$adults = $row['adults'];
			$children = $row['children'];
			$pets = $row['pets'];
			$owner = $row['owner'];
			$housekeeper = $row['housekeeper'];
			$hottub = $row['maintenance'];
			$sum_detail = $row['sum_detail'];
			$total_amount = $row['total_amount'];
			$date_start = date('m/d/Y',strtotime($row['date_start']));
			$date_end = date('m/d/Y',strtotime($row['date_end']));
			$property = $row['property_id'];
			$amount = $row_pay['amount'];
			$coupon_dis = 0;
			
			sendfinalemailcc($email,$guest_id,$firstname,$lastname,$adults,$children,$pets,$amount,$sum_detail,$total_amount,$date_start,$date_end,$property);
			
			
					
				$paid_date = date('Y-m-d h:i:s');
				mysql_query("update payments_posts set transaction_status = 'Paid', type = 'sale', paid_date = '$paid_date' where id = '".$row_pay['id']."'") or die(mysql_error());



			
		}
		
		elseif($send_email == 11){
			$result= mysql_query("SELECT * FROM ".GUESTS." WHERE id = '$guest_id'") or die(mysql_error());
			$row = mysql_fetch_array($result);
			
			$email = $row['email'];
			$firstname = $row['firstname'];
			$lastname = $row['lastname'];
			$adults = $row['adults'];
			$children = $row['children'];
			$pets = $row['pets'];
			$sum_detail = $row['sum_detail'];
			$total_amount = $row['total_amount'];
			$date_start = date('m/d/Y',strtotime($row['date_start']));
			$date_end = date('m/d/Y',strtotime($row['date_end']));
			$property_id = $row['property_id'];
			$coupon_dis = 0;
			
			sendbookedemail50cc($email,$guest_id,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$date_due,$property_id,
			$coupon_dis);
		}
		
		elseif($send_email == 14){
			$sqlP = "SELECT * FROM ".PAYMENTS." WHERE guest_id = '$guest_id'"; //


$resultP= mysql_query($sqlP) or die(__LINE__.mysql_error()); //

mysql_num_rows($resultP);

$rowP = mysql_fetch_array($resultP);



		$guest_id = $rowP['guest_id'];

		$date_due = date("m/d/Y",strtotime($rowP['date_due']));

		$amount = $amount_due = $rowP['amount'];





$sql = "SELECT * FROM ".GUESTS." WHERE id = '".$guest_id."' "; //

$result= mysql_query($sql) or die(__LINE__.mysql_error()); //

mysql_num_rows($result);

$row = mysql_fetch_array($result);



		$to_email = $row['email'];

		$firstname = $row['firstname'];

		$lastname = $row['lastname'];

		$date_start = date("m/d/Y",strtotime($row['date_start']));

		$date_end = date("m/d/Y",strtotime($row['date_end']));

		$property = $row['property_id'];



			if(send_credit_card_payment_due_notice_to_guest($to_email,$firstname,$lastname,$date_start,$date_end,$due_date,$amount_due,$property,$guest_id)){


			}
		}
		
		elseif($send_email == 22){
			$result= mysql_query("SELECT * FROM ".GUESTS." WHERE id = '$guest_id'") or die(mysql_error());
			$row = mysql_fetch_array($result);
			
			$email = $row['email'];
			$firstname = $row['firstname'];
			$lastname = $row['lastname'];
			$adults = $row['adults'];
			$children = $row['children'];
			$pets = $row['pets'];
			$sum_detail = $row['sum_detail'];
			$total_amount = $row['total_amount'];
			$date_start = date('m/d/Y',strtotime($row['date_start']));
			$date_end = date('m/d/Y',strtotime($row['date_end']));
			$property_id = $row['property_id'];
			$coupon_dis = 0;
			
			$selhouse = mysql_query("select * from properties_posts where id = '$property_id'") or die(mysql_error());
					$gethouse = mysql_fetch_array($selhouse);
					
					
					$housekeeper_id = $gethouse['housekeeper'];
			
			
			
			sendhousekeeperemail($housekeeper_id,$guest_id,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property_id);
		}
		
		
		elseif($send_email == 24){
			$result= mysql_query("SELECT * FROM ".GUESTS." WHERE id = '$guest_id'") or die(mysql_error());
			$row = mysql_fetch_array($result);
			
			$email = $row['email'];
			$firstname = $row['firstname'];
			$lastname = $row['lastname'];
			$adults = $row['adults'];
			$children = $row['children'];
			$pets = $row['pets'];
			$sum_detail = $row['sum_detail'];
			$total_amount = $row['total_amount'];
			$date_start = date('m/d/Y',strtotime($row['date_start']));
			$date_end = date('m/d/Y',strtotime($row['date_end']));
			$property_id = $row['property_id'];
			$coupon_dis = 0;
			
			$selhottub = mysql_query("select * from properties_posts where id = '$property_id'") or die(mysql_error());
					$gethottub = mysql_fetch_array($selhottub);
					
					
					$hottub_id = $gethouse['maintenance'];
			
			sendhottubemail($hottub_id,$guest_id,$firstname,$lastname,$adults,$children,$pets,$sum_detail,$total_amount,$date_start,$date_end,$property_id);
		}
	

	$success = "Email Successfully Sent.";





$showform = 'no';

$done = true;



}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<link type="text/css" href="css/layout.css" rel="stylesheet" />

<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>

<script type="text/javascript" src="js/easyTooltip.js"></script>

<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>

<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>

<script type="text/javascript" src="js/hoverIntent.js"></script>

<script type="text/javascript" src="js/superfish.js"></script>

<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>

<script type="text/javascript" src="js/searchbox.js"></script>

<script type="text/javascript" src="js/custom.js"></script>


<style type="text/css">

body {

	 background:#fff;

	 margin:20px;

}

</style>


</head>

<body>

<?php include"messages-display.php";?>


<table border="0">


  <tr>


    


    <td valign="top"><h2>Email Templates List</h2>


<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">


<thead>


  <tr>


    <th width="300">Name/Subject</th>


    <td width="50">&nbsp;</td>


  </tr>


</thead>


<tbody>


<?php


$result= mysql_query("SELECT * FROM ".EMAILTEMPLATES." WHERE (type = 'Credit Card' OR type = 'General') AND active = '1' ORDER BY id ASC");


while($row = mysql_fetch_array($result)){


?>


    <tr>


      <td><?=stripslashes($row['email_subject'])?></td>


      <td><a href="?send_email=<?=$row['id']?>&guest_id=<?=$_GET['guest_id']?>"><input type="button" class="button" value="Send" /></a></td>


    </tr>


  <?php } ?>


</tbody>


</table></td>


  </tr>


</table>

</body>

</html>