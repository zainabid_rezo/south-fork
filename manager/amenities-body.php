<script language="javascript" type="text/javascript">

function showtypesdetail(){ 
var type = document.getElementById('type').options[document.getElementById('type').selectedIndex].value;

	if(type=="Dropdown"){
	document.getElementById("dropdown").style.display="block";
	}
	else{
	document.getElementById("dropdown").style.display="none";
	}

}
</script>
<?php
if(isset($_POST['btnAdd'])){

$error = "";
$success = "";

	$title		=	addslashes($_POST['title']);
	$type		=	addslashes($_POST['type']);
	$grouped	=	addslashes($_POST['grouped']);
	$sortby		=	addslashes($_POST['sortby']);
	$status		=	"1";

	if($title=="")
	{
	 $error .="Please enter title.<br>";
	}

	if($type=="0")
	{
	 $error .="Please select a type.<br>";
	}
	
	if($grouped=="0")
	{
	 $error .="Please select group.<br>";
	}

	if($sortby=="")
	{
	 $error .="Please enter display order.<br>";
	}

	if($error=="")
	{
	
	$sql		=	"INSERT INTO ".AMENITIES." (`title`, `type`, `grouped`, `sortby`, `status`) VALUES ('$title', '$type', '$grouped', '$sortby', '$status')";
	mysql_query($sql) or die(__LINE__.mysql_error());
	$amenity_id 	=	mysql_insert_id();
	$success	.= 	"Added successfuly.";
			if($type=='Dropdown'){
			$amenity_dropdown_values = $_POST["amenity_dropdown_value"];
			foreach ($amenity_dropdown_values as $dropdown_value) {
		 	if(!empty($dropdown_value)){
		 	$sql_2		=	"INSERT INTO ".AMENITIESDROPDOWNVALUES." (`amenity_id`,`value`) VALUES ('$amenity_id','$dropdown_value')";
			mysql_query($sql_2) or die(__LINE__.mysql_error());
			}
			}
			}
	unset($_GET);
	
	}

}
?>
<?php
if(isset($_POST['btnEditDo'])){

$error = "";
$success = "";

	$editid		=	addslashes($_POST['editid']);
	$title		=	addslashes($_POST['title']);
	$type		=	addslashes($_POST['type']);
	$grouped	=	addslashes($_POST['grouped']);
	$sortby		=	addslashes($_POST['sortby']);

	if($title=="")
	{
	 $error .="Please enter title.<br>";
	}

	if($type=="0")
	{
	 $error .="Please select a type.<br>";
	}

	if($grouped=="0")
	{
	 $error .="Please select group.<br>";
	}
	
	if($sortby=="")
	{
	 $error .="Please enter display order.<br>";
	}

	if($error=="")
	{

	$sql		=	"UPDATE ".AMENITIES." SET title = '$title', type = '$type', grouped = '$grouped', sortby = '$sortby' WHERE id = '$editid'";
	//echo $sql;
	$res		=	mysql_query($sql) or die(__LINE__.mysql_error());
	$success	= "Successfully updated.";
	
	$sql		=	"DELETE FROM ".AMENITIESDROPDOWNVALUES." WHERE amenity_id = '$editid'";
	mysql_query($sql) or die(mysql_error());
			if($type=='Dropdown'){
			$amenity_dropdown_values = $_POST["amenity_dropdown_value"];
			foreach ($amenity_dropdown_values as $dropdown_value) {
		 	if(!empty($dropdown_value)){
		 	$sql_2		=	"INSERT INTO ".AMENITIESDROPDOWNVALUES." (`amenity_id`,`value`) VALUES ('$editid','$dropdown_value')";
			mysql_query($sql_2) or die(__LINE__.mysql_error());
			}
			}
			}
	unset($_GET);
	}
	
}
?>
<?php if(isset($_GET['delete']) AND !empty($_GET['delete'])){

$delid = $_GET['delete'];

	$sql		=	"DELETE FROM ".AMENITIESDROPDOWNVALUES." WHERE amenity_id = '$delid'";
	mysql_query($sql) or die(__LINE__.mysql_error());

	$sql		=	"DELETE FROM ".AMENITIES." WHERE id = '$delid'";
	mysql_query($sql) or die(__LINE__.mysql_error());
	$success	= "Successfully deleted.<br/>";
	unset($_GET);

}
?>
<!------------------------------------------------------------------------------->
<?php include"messages-display.php";?>
<?php if(isset($_GET['add'])){ ?>

<form action="" method="post" enctype="multipart/form-data">
  <fieldset>
  <legend>
  <h2>Add Amenity </h2>
  </legend>
  <table class="form-table">
    <tr>
      <th>Title<span> <font color="#FF0000">*</font></span></th>
      <td colspan="2"><input name="title" type="text" id="title" value="<?=$_POST['title']?>" /></td>
    </tr>
    <tr>
      <th>Type<span> <font color="#FF0000">*</font></span></th>
      <td><select name="type" id="type" onchange="showtypesdetail();">
        <option value="0" <?php if(isset($_POST['type']) AND $_POST['type']=="0"){echo 'selected="selected"';}?> > - select - </option>
        <option value="InputText" <?php if(isset($_POST['type']) AND $_POST['type']=="InputText"){echo 'selected="selected"';}?> > InputText </option>
        <option value="CheckBox" <?php if(isset($_POST['type']) AND $_POST['type']=="CheckBox"){echo 'selected="selected"';}?> > CheckBox </option>
        <option value="Dropdown" <?php if(isset($_POST['type']) AND $_POST['type']=="Dropdown"){echo 'selected="selected"';}?> > Dropdown </option>
      </select></td>
      <td>
        <div id="dropdown" style="display:none">
          <script language="Javascript" type="text/javascript">
		var counter = 2;
		function createInput(CntnrDv){

				  var newdiv = document.createElement('div');
				  newdiv.innerHTML = "Value " + (counter + 1) + " <input type='text' name='amenity_dropdown_value[]'><br>";
				  document.getElementById(CntnrDv).appendChild(newdiv);
				  counter++;

		}
		</script>
          <div id="ContainerDiv">
            <div>Value 1
              <input type="text" name="amenity_dropdown_value[]">
              <br />
            </div>
            <div>Value 2
              <input type="text" name="amenity_dropdown_value[]">
              <br />
            </div>
          </div>
          <img src="assets/add-icon.jpg" onClick="createInput('ContainerDiv');"/> </div></td>
    </tr>
    <tr>
      <th>Group <font color="#FF0000">*</font></th>
      <td colspan="2"><select name="grouped" id="grouped">
          <option value="0" <?php if(isset($_POST['grouped']) AND $_POST['grouped']=="0"){echo 'selected="selected"';}?> > - select - </option>
          <option value="General" <?php if(isset($_POST['grouped']) AND $_POST['grouped']=="General"){echo 'selected="selected"';}?> > General </option>
          <option value="Bedding" <?php if(isset($_POST['grouped']) AND $_POST['grouped']=="Bedding"){echo 'selected="selected"';}?> > Bedding </option>
          <option value="Amenities" <?php if(isset($_POST['grouped']) AND $_POST['grouped']=="Amenities"){echo 'selected="selected"';}?> > Amenities </option>
      </select>      </td>
    </tr>
    <tr>
      <th>Display Order <font color="#FF0000">*</font></th>
      <td colspan="2"><input name="sortby" type="text" id="sortby" value="<?=$_POST['sortby']?>" size="5" /></td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td colspan="2"><input type="submit" name="btnAdd" id="btnAdd" value="Add" class="button" />      </td>
    </tr>
  </table>
  </fieldset>
</form>
<?php }
elseif(isset($_GET['editid'])){ ?>
<?php
$id = $_GET['editid'];
$result= mysql_query("SELECT * FROM ".AMENITIES." WHERE id = '$id'");

while($row = mysql_fetch_array($result)){
?>
<form action="" method="post" enctype="multipart/form-data">
  <fieldset>
  <legend>
  <h2>Edit Amenity </h2>
  </legend>
  <table class="form-table">
    <tr>
      <th>Title <font color="#FF0000">*</font></th>
      <td colspan="2"><input name="title" type="text" id="title" value="<?php if(isset($_POST['title'])) {echo $_POST['title'];} else {echo stripslashes($row['title']);}?>" /></td>
    </tr>
    <tr>
      <th>Type<font color="#FF0000">*</font></th>
      <td><select name="type" id="type" onchange="showtypesdetail();">
        <option value="InputText" <?php if(isset($_POST['type']) AND $_POST['type']=="InputText"){echo 'selected="selected"';}elseif($row['type']=="InputText"){echo 'selected="selected"';}?> > InputText </option>
        <option value="CheckBox" <?php if(isset($_POST['type']) AND $_POST['type']=="CheckBox"){echo 'selected="selected"';}elseif($row['type']=="CheckBox"){echo 'selected="selected"';}?> > CheckBox </option>
        <option value="Dropdown" <?php if(isset($_POST['type']) AND $_POST['type']=="Dropdown"){echo 'selected="selected"';}elseif($row['type']=="Dropdown"){echo 'selected="selected"';}?> > Dropdown </option>
      </select></td>
      <td>
        <div id="dropdown" style="display:<?php if($row['type']=="Dropdown") echo 'block'; else echo 'none';?>">
          <script language="Javascript" type="text/javascript">
		<?php
		$resultTDV	= mysql_query("SELECT * FROM ".AMENITIESDROPDOWNVALUES." WHERE amenity_id = '$id'");
		$counter = mysql_num_rows($resultTDV);
		?>
		var counter = <?=$counter?>;
		function createInput(CntnrDv){

				  var newdiv = document.createElement('div');
				  newdiv.innerHTML = "Value " + (counter + 1) + " <input type='text' name='amenity_dropdown_value[]'><br>";
				  document.getElementById(CntnrDv).appendChild(newdiv);
				  counter++;

		}
		</script>
          <div id="ContainerDiv">
            <?php
				  $counter = "1";
				  $resultTDV	= mysql_query("SELECT * FROM ".AMENITIESDROPDOWNVALUES." WHERE amenity_id = '$id' ORDER BY value ASC");
					while($rowTDV = mysql_fetch_array($resultTDV)){
					?>
            Value
            <?=$counter?>
            <input type="text" name="amenity_dropdown_value[]" value="<?=stripslashes($rowTDV['value'])?>">
            <br />
            <?
					$counter++;
					}
					?>
          </div>
          <img src="assets/add-icon.jpg" onClick="createInput('ContainerDiv');"/> </div></td>
    </tr>
    <tr>
      <th>Group <font color="#FF0000">*</font></th>
      <td colspan="2"><select name="grouped" id="grouped">
          <option value="General" <?php if(isset($_POST['grouped']) AND $_POST['grouped']=="General"){echo 'selected="selected"';}elseif($row['grouped']=="General"){echo 'selected="selected"';}?> > General </option>
          <option value="Bedding" <?php if(isset($_POST['grouped']) AND $_POST['grouped']=="Bedding"){echo 'selected="selected"';}elseif($row['grouped']=="Bedding"){echo 'selected="selected"';}?> > Bedding </option>
          <option value="Amenities" <?php if(isset($_POST['grouped']) AND $_POST['grouped']=="Amenities"){echo 'selected="selected"';}elseif($row['grouped']=="Amenities"){echo 'selected="selected"';}?> > Amenities </option>
      </select>      </td>
    </tr>
    <tr>
      <th>Display Order <font color="#FF0000">*</font></th>
      <td colspan="2"><input name="sortby" type="text" id="sortby" value="<?php if(isset($_POST['sortby'])) {echo $_POST['sortby'];} else {echo stripslashes($row['sortby']);}?>" size="5" /></td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td colspan="2"><input type="submit" name="btnEditDo" id="btnEditDo" value="Update" class="button" />
      <input name="editid" type="hidden" value="<?=$row['id']?>" />      </td>
    </tr>
  </table>
  </fieldset>
</form>
<?php } ?>
<?php }else{ ?>
<h2>View</h2>
<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0">
  <thead>
    <tr>
      <th width="200">Name</th>
      <td width="50">Edit</td>
	  <td width="50">Delete</td>
    </tr>
  </thead>
  <tbody>
    <?php
$result= mysql_query("SELECT * FROM ".AMENITIES." ORDER BY grouped DESC, sortby ASC "); //
while($row = mysql_fetch_array($result)){
?>
  <form action="" method="post">
    <tr>
      <td><?php echo stripslashes($row['grouped']);?> - <strong><?php echo stripslashes($row['title']);?></strong></td>
      <td><a href="?editid=<?=$row['id']?>"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
	  <td><a href="javascript:confirmDelete('?delete=<?=$row['id']?>');"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
  </form>
  <?php } ?>
  </tbody>
  
</table>
<?php } ?>
