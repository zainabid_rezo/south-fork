<?php
$_POST['date_start'] = $_POST['date_start'] ? $_POST['date_start'] : date('m/d/Y',strtotime("-30 days"));
$_POST['date_end'] = $_POST['date_end'] ? $_POST['date_end'] : date('m/d/Y');
$_POST['property_id'] = $_POST['property_id'] ? $_POST['property_id'] : 'all';
?>

<table class="noprint">
  <form name="statistics" method="post" action="">
    <tr>
      <td class="redtext">Select Property:</td>
      <td><select name="property_id" id="property_id">
          <option value="all" <?php if(isset($_POST['property_id']) AND ($_POST['property_id']=="all")){echo "selected";}?>> All </option>
          <?php 
			$sql = "SELECT * FROM ".PROPERTIES." ORDER BY title ASC";
			$res = mysql_query($sql) or die(__LINE__.mysql_error());
			while($row=mysql_fetch_array($res))
			{
			?>
          <option value="<?=$row['id']?>" <?php if(isset($_POST['property_id']) AND ($_POST['property_id']==$row['id'])){echo "selected";}?> >
          <?=stripslashes($row['title'])?>
          </option>
          <?php } ?>
        </select>      </td>
      <td><span class="redtext">From:</span></td>
      <td><input name="date_start" type="text" id="date_start" value="<?php if(isset($_POST['date_start'])) echo $_POST['date_start']; else echo "mm/dd/yyyy";?>" size="12" readonly="readonly" /></td>
      <td><span class="redtext">To:</span></td>
      <td><input name="date_end" type="text" id="date_end" value="<?php if(isset($_POST['date_end'])) echo $_POST['date_end']; else echo "mm/dd/yyyy";?>" size="12" readonly="readonly" /></td>
      <td><input type="submit" name="btnSearch" id="Search" value="Search" class="button" /></td>
      <td><!----><a href="reports-special-export.php?date_start=<?=$_POST['date_start']?>&date_end=<?=$_POST['date_end']?>&property_id=<?=$_POST['property_id']?>"><strong>Export CSV</strong></a></td>
    </tr>
  </form>
</table>


<br />
<br />


<div align="right"><input type="button" value="Print this page"  onClick="printpage()"class="button noprint" /></div>
<table class="hiddenprint" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td width="100"><strong>Property</strong></td>
      <td width="100"><strong>Date</strong></td>
      <td width="100"><strong>Lodging Total</strong></td>
      <td width="100"><strong>Lodging Tax</strong></td>
      <td width="100"><strong>Sales Tax</strong></td>
      <td width="100"><strong>Damage</strong></td>
      <td width="100"><strong>Cleaning</strong></td>
      <td width="100"><strong>Payment Amount</strong></td>
      <td width="100"><strong>Date Paid </strong></td>
    </tr>

<?php
$logding_total = '0';
$commission_total = '0';
$maintenance_total = '0';
if($_POST['property_id']=="all"){
$sqlP = "SELECT * FROM ".PROPERTIES." ORDER BY title ASC"; // 
}else{
$sqlP = "SELECT * FROM ".PROPERTIES." WHERE id = '".$_POST['property_id']."'"; // 
}
$resP = mysql_query($sqlP) or die(__LINE__.mysql_error());
while($rowP=mysql_fetch_array($resP))
{

$sqlG = "SELECT * FROM ".GUESTS." WHERE status='B' AND property_id = '".$rowP['id']."' AND date_start >= '".date("Y-m-d",strtotime($_POST['date_start']))."' AND date_start <= '".date("Y-m-d",strtotime($_POST['date_end']))."'";
$resultG= mysql_query($sqlG) or die(__LINE__.mysql_error());
while($rowG = mysql_fetch_array($resultG)){

$logding_total += $rowG['lodging_amount'];
$commission_total += ($rowG['lodging_amount']*$rowP['commission']/100);
?>
    <tr>
      <td><?=stripslashes($rowP['title'])?></td>
      <td><?=date("m/d/Y",strtotime($rowG['date_start']))?></td>
      <td><?=stripslashes('\$'.money($rowG['lodging_amount']))?></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td><?=stripslashes('\$'.money(($rowG['lodging_amount'])-($rowG['lodging_amount']*$rowP['commission']/100)))?></td>
      <td></td>
    </tr>

<?php
}

$sqlM = "SELECT * FROM ".MAINTENANCEJOBS." WHERE property_id = '".$rowP['id']."' AND date >= '".date("Y-m-d",strtotime($_POST['date_start']))."' AND date <= '".date("Y-m-d",strtotime($_POST['date_end']))."'";
$resultM= mysql_query($sqlM) or die(__LINE__.mysql_error());
while($rowM = mysql_fetch_array($resultM)){

$maintenance_total += $rowM['amount'];
?>
    <tr style="color:#aa0000">
      <td>&nbsp;</td>
      <td><?=date("m/d/Y",strtotime($rowM['date']))?></td>
      <td>&nbsp;</td>
      <td></td>
      <td></td>
      <td><?=stripslashes($rowM['title'])?></td>
      <td>&nbsp;</td>
      <td>(-<?=stripslashes('\$'.money($rowM['amount']))?>)</td>
      <td>&nbsp;</td>
    </tr>

<?php
}

}
?>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>Total:</td>
      <td><?=stripslashes('\$'.money($logding_total))?></td>
      <td><?=stripslashes('\$'.money($commission_total))?></td>
      <td><?=stripslashes('\$'.money($logding_total-$commission_total-$maintenance_total))?></td>
      <td></td>
    </tr>
</table>
<!---------NO PRINT------------------>

<table class="tablesorter normal noprint" cellspacing="0" cellpadding="0" border="0">
