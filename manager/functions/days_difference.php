<?php


function days_difference($date_start, $date_end)


	{ 
		
		$date1=date_create($date_start);
		$date2=date_create($date_end);
		$diff=date_diff($date1,$date2);
		$days = str_replace('+','',$diff->format("%R%a"));
		return $days;


	}