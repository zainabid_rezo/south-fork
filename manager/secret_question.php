<?php

include_once("conn.php");

include_once("db-tables.php");

include_once("site-details.php");

include_once("functions.php");

?>



<!DOCTYPE html>

<html>

<head>

<?php include"head-include.php";?>

</head>

<body>

<!-- Container -->

<div id="container">

  <!-- Header -->

  <div id="header">

    <!-- Top -->

    <div class="logo">

      <h1>

        <?=SITE_NAME?>

      </h1>

    </div>

    <!-- End of Top-->

    <!-- The navigation bar -->

    <div id="navbar">

      <?php //include"navigation.php";?>

    </div>

    <!-- End of navigation bar" -->

  </div>

  <!-- End of Header -->

  <!-- Background wrapper -->

  <div id="bgwrap">

    <!-- Main Content -->

    <div id="content">

      <div id="main">

        <?php include"secret_question_body.php";?>

      </div>

    </div>

    <!-- End of Main Content -->

  </div>

  <!-- End of bgwrap -->

</div>

<!-- End of Container -->

<!-- Footer -->

<div id="footer">

  <?php include"footer.php";?>

</div>

<!-- End of Footer -->

</body>

</html>

