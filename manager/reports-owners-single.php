<?php

include_once("conn.php");

include_once("db-tables.php");

include_once("site-details.php");

include_once("functions.php");

include_once("authenticate.php");

?>

<!DOCTYPE html>

<html>

<head>

<?php include"head-include.php";?>



<style type="text/css" media="print" >

<!--



. {

	display: none;

}



.noprint {

	display: none;

}



.printable, hiddenprint{

	display: block;

}



-->

</style>

<style type="text/css" media="screen" >

<!--



.hiddenprint{

	display: none;

}



-->

</style>

<script type="text/javascript">

function printpage()

{

window.print();

}

</script>

</head>

<body id="top">

<!-- Container -->

<div id="container">

  <!-- Header -->

  <div id="header" class="noprint">

    <!-- Top -->

    <?php include"header-right.php";?>

    <!-- End of Top-->

    <!-- The navigation bar -->

    <div id="navbar" class="noprint">

      <?php include"navigation.php";?>

    </div>

    <!-- End of navigation bar" -->

  </div>

  <!-- End of Header -->

  <!-- Background wrapper -->

  <div id="bgwrap">

    <!-- Main Content -->

    <div id="content">

      <div id="main">

        <?php

		include"reports-owners-single-body.php";

		?>

      </div>

    </div>

    <!-- End of Main Content -->

  </div>

  <!-- End of bgwrap -->

</div>

<!-- End of Container -->



</body>

</html>

