<h1>Welcome, <span>
  <?=$_SESSION['admin_firstname']?>
  <?=$_SESSION['admin_lastname']?>
  </span>!</h1>

<h2>New Reservations</h2>

<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0" style="width:980px">
  <thead>
    <tr>
      <th width="150">Name</td>
      <th width="150">Property</td>
      <th width="100">Created</td>
      <th width="50">Arrival</td>
      <th width="30">Amount</td>
      <td width="30">Edit</td>
      <td width="30">Delete</td>
    </tr>
  </thead>
  <?php
	$result= mysql_query("SELECT * FROM ".GUESTS." WHERE status = 'Z' OR status = 'P' ORDER BY id DESC");
	if(mysql_num_rows($result)=="0"){
	?>
    <tr>
      <td colspan="7">No results found!</td>
    </tr>
	<?php
	}else{
	while($row = mysql_fetch_array($result)){
	?>
    <tr>
      <td><?=stripslashes($row['firstname'])?> <?=stripslashes($row['lastname'])?></td>
      <td><?=stripslashes(get_property_name($row['property_id']))?></td>
      <td><font size="1"><?=date("m/d/Y H:i:s",strtotime($row['created']))?></font></td>
      <td><?=date("m/d/y",strtotime($row['date_start']))?></td>
      <td>$<?=money($row['total_amount'])?></td>
      <td><a href="reservations-view.php?editid=<?=$row['id']?>"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
      <td><a href="javascript:confirmDelete('reservations-view.php?delete=<?=$row['id']?>');"><img src="assets/delete.png" width="16" height="16" border="0" /></a></td>
    </tr>
  <?php } ?>
  <?php } ?>
</table>


<h2>Payments Due</h2>

<table class="tablesorter normal" cellspacing="0" cellpadding="0" border="0" style="width:980px">
  <thead>
    <tr>
      <th width="150">Property</td>
      <th width="150">Name</td>
      <th width="30">Amount</td>
      <th width="50">Due Date</td>
      <td width="30">Edit</td>
    </tr>
  </thead>
  <?php
	$result= mysql_query("SELECT * FROM ".GUESTS." WHERE partialpayment = '1' AND amount_due > '0' AND final_payment_received != '1' ORDER BY due_date DESC");
	if(mysql_num_rows($result)=="0"){
	?>
    <tr>
      <td colspan="7">No results found!</td>
    </tr>
	<?php
	}else{
	while($row = mysql_fetch_array($result)){
	?>
    <tr>
      <td><?=stripslashes(get_property_name($row['property_id']))?></td>
      <td><?=stripslashes($row['firstname'])?> <?=stripslashes($row['lastname'])?></td>
      <td>$<?=money($row['amount_due'])?></td>
      <td><?=date("m/d/Y",strtotime($row['due_date']))?></td>
	  <td><a href="reservations-view.php?editid=<?=$row['id']?>"><img src="assets/edit.png" width="16" height="16" border="0" /></a></td>
    </tr>
  <?php } ?>
  <?php } ?>
</table>

