<?php
$_POST['date_start'] = $_POST['date_start'] ? $_POST['date_start'] : date('m/d/Y',strtotime("-30 days"));
$_POST['date_end'] = $_POST['date_end'] ? $_POST['date_end'] : date('m/d/Y');
$_POST['owner_id'] = $_POST['owner_id'] ? $_POST['owner_id'] : 'all';
?>

<table class="noprint">
  <form name="statistics" method="post" action="">
    <tr>
      <td class="redtext">Select Owner:</td>
      <td><select name="owner_id" id="owner_id">
          <option value="all" <?php if(isset($_POST['owner_id']) AND ($_POST['owner_id']=="all")){echo "selected";}?>> All </option>
          <?php 
			$sql = "SELECT * FROM ".OWNERS." ORDER BY lastname ASC";
			$res = mysql_query($sql) or die(__LINE__.mysql_error());
			while($row=mysql_fetch_array($res))
			{
			?>
          <option value="<?=$row['id']?>" <?php if(isset($_POST['owner_id']) AND ($_POST['owner_id']==$row['id'])){echo "selected";}?> >
          <?=stripslashes($row['lastname'])?>, <?=stripslashes($row['firstname'])?> 
          </option>
          <?php } ?>
        </select>      </td>
      <td><span class="redtext">From:</span></td>
      <td><input name="date_start" type="text" id="date_start" value="<?php if(isset($_POST['date_start'])) echo $_POST['date_start']; else echo "mm/dd/yyyy";?>" size="12" readonly="readonly" /></td>
      <td><span class="redtext">To:</span></td>
      <td><input name="date_end" type="text" id="date_end" value="<?php if(isset($_POST['date_end'])) echo $_POST['date_end']; else echo "mm/dd/yyyy";?>" size="12" readonly="readonly" /></td>
      <td><input type="submit" name="btnSearch" id="Search" value="Search" class="button" /></td>
      <td><!--<a href="reports-owner-export.php?date_start=<?=$_POST['date_start']?>&date_end=<?=$_POST['date_end']?>&owner_id=<?=$_POST['owner_id']?>"><strong>Export CSV</strong></a>--></td>
    </tr>
  </form>
</table>


<br />
<br />
<h2>Owner Reports</h2>

<div align="right"><input type="button" value="Print this page"  onClick="printpage()"class="button noprint" /></div>
<table class="hiddenprint" cellspacing="0" cellpadding="0" border="0">
    <tr>
      <td width="100"><strong>Date</strong></td>
      <td width="100"><strong>Guest</strong></td>
      <td width="100"><strong>Property</strong></td>
      <td width="100"><strong># of days</strong></td>
      <td width="100"><strong>Owner</strong></td>
      <td width="100"><strong>Lodging Total</strong></td>
      <td width="100"><strong>Commission</strong></td>
      <td width="100"><strong>Amount</strong></td>
    </tr>

<?php
$logding_total = '0';
$commission_total = '0';
$maintenance_total = '0';
if($_POST['owner_id']=="all"){
$sqlW = "SELECT * FROM ".OWNERS." ORDER BY lastname ASC"; // 
}else{
$sqlW = "SELECT * FROM ".OWNERS." WHERE id = '".$_POST['owner_id']."'"; // 
}
$resW = mysql_query($sqlW) or die(__LINE__.mysql_error());
while($rowW=mysql_fetch_array($resW))
{
$sqlP = "SELECT * FROM ".PROPERTIES." WHERE owner = '".$rowW['id']."' ";
$resultP= mysql_query($sqlP) or die(__LINE__.mysql_error());
while($rowP = mysql_fetch_array($resultP)){
$sqlG = "SELECT * FROM ".GUESTS." WHERE status='B' AND property_id = '".$rowP['id']."' AND date_start >= '".date("Y-m-d",strtotime($_POST['date_start']))."' AND date_start <= '".date("Y-m-d",strtotime($_POST['date_end']))."'";
$resultG= mysql_query($sqlG) or die(__LINE__.mysql_error());
while($rowG = mysql_fetch_array($resultG)){

$logding_total += $rowG['lodging_amount'];
$commission_total += ($rowG['lodging_amount']*$rowP['commission']/100);
?>
    <tr>
      <td><?=date("m/d/Y",strtotime($rowG['date_start']))?></td>
      <td><?=stripslashes($rowG['lastname'])?>, <?=stripslashes($rowG['firstname'])?></td>
      <td><?=stripslashes($rowP['title'])?></td>
      <td><?=days_difference($rowG['date_start'],$rowG['date_end'])?></td>
      <td><?=stripslashes($rowW['name'])?></td>
      <td><?=stripslashes('\$'.money($rowG['lodging_amount']))?></td>
      <td><?=$rowP['commission']?>% &nbsp;&nbsp;&nbsp;<?=stripslashes('\$'.money($rowG['lodging_amount']*$rowP['commission']/100))?></td>
      <td><?=stripslashes('\$'.money(($rowG['lodging_amount'])-($rowG['lodging_amount']*$rowP['commission']/100)))?></td>
    </tr>

<?php
}

$sqlM = "SELECT * FROM ".MAINTENANCEJOBS." WHERE property_id = '".$rowP['id']."' AND date >= '".date("Y-m-d",strtotime($_POST['date_start']))."' AND date <= '".date("Y-m-d",strtotime($_POST['date_end']))."'";
$resultM= mysql_query($sqlM) or die(__LINE__.mysql_error());
while($rowM = mysql_fetch_array($resultM)){

$maintenance_total += $rowM['amount'];
?>
    <tr style="color:#aa0000">
      <td><?=date("m/d/Y",strtotime($rowM['date']))?></td>
      <td><?=stripslashes($rowM['title'])?></td>
      <td><?=stripslashes($rowP['title'])?></td>
      <td></td>
      <td></td>
      <td></td>
      <td>Maintenance Fee:</td>
      <td>(-<?=stripslashes('\$'.money($rowM['amount']))?>)</td>
    </tr>

<?php
}
}
}
?>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>Total:</td>
      <td><?=stripslashes('\$'.money($logding_total))?></td>
      <td><?=stripslashes('\$'.money($commission_total))?></td>
      <td><?=stripslashes('\$'.money($logding_total-$commission_total-$maintenance_total))?></td>
    </tr>
</table>
<!---------NO PRINT------------------>

<table class="tablesorter normal noprint" cellspacing="0" cellpadding="0" border="0">
<thead>
    <tr>
      <th width="100"><strong>Date</strong></th>
      <th width="100"><strong>Guest</strong></th>
      <th width="100"><strong>Property</strong></th>
      <th width="100"><strong># of days</strong></th>
      <th width="100"><strong>Owner</strong></th>
      <th width="100"><strong>Lodging Total</strong></th>
      <th width="100"><strong>Commission</strong></th>
      <th width="100"><strong>Amount</strong></th>
    </tr>
</thead>
<tbody>
<?php
$logding_total = '0';
$commission_total = '0';
$maintenance_total = '0';

if($_POST['owner_id']=="all"){
$sqlW = "SELECT * FROM ".OWNERS." ORDER BY lastname ASC"; // 
}else{
$sqlW = "SELECT * FROM ".OWNERS." WHERE id = '".$_POST['owner_id']."'"; // 
}
$resW = mysql_query($sqlW) or die(__LINE__.mysql_error());
while($rowW=mysql_fetch_array($resW))
{

$sqlP = "SELECT * FROM ".PROPERTIES." WHERE owner = '".$rowW['id']."' ";
$resultP= mysql_query($sqlP) or die(__LINE__.mysql_error());
while($rowP = mysql_fetch_array($resultP)){

$sqlG = "SELECT * FROM ".GUESTS." WHERE status='B' AND property_id = '".$rowP['id']."' AND date_start >= '".date("Y-m-d",strtotime($_POST['date_start']))."' AND date_start <= '".date("Y-m-d",strtotime($_POST['date_end']))."'";
$resultG= mysql_query($sqlG) or die(__LINE__.mysql_error());
while($rowG = mysql_fetch_array($resultG)){

$logding_total += $rowG['lodging_amount'];
$commission_total += ($rowG['lodging_amount']*$rowP['commission']/100);
?>
    <tr>
      <td><?=date("m/d/Y",strtotime($rowG['date_start']))?></td>
      <td><?=stripslashes($rowG['lastname'])?>, <?=stripslashes($rowG['firstname'])?></td>
      <td><?=stripslashes($rowP['title'])?></td>
      <td><?=days_difference($rowG['date_start'],$rowG['date_end'])?></td>
      <td><?=stripslashes($rowW['name'])?></td>
      <td><?=stripslashes('\$'.money($rowG['lodging_amount']))?></td>
      <td><?=stripslashes('\$'.money($rowG['lodging_amount']*$rowP['commission']/100))?>&nbsp;&nbsp;&nbsp;(<?=$rowP['commission']?>%)</td>
      <td><?=stripslashes('\$'.money(($rowG['lodging_amount'])-($rowG['lodging_amount']*$rowP['commission']/100)))?></td>
    </tr>

<?php
}

$sqlM = "SELECT * FROM ".MAINTENANCEJOBS." WHERE property_id = '".$rowP['id']."' AND date >= '".date("Y-m-d",strtotime($_POST['date_start']))."' AND date <= '".date("Y-m-d",strtotime($_POST['date_end']))."'";
$resultM= mysql_query($sqlM) or die(__LINE__.mysql_error());
while($rowM = mysql_fetch_array($resultM)){

$maintenance_total += $rowM['amount'];
?>
    <tr style="color:#aa0000">
      <td><?=date("m/d/Y",strtotime($rowM['date']))?></td>
      <td><?=stripslashes($rowM['title'])?></td>
      <td><?=stripslashes($rowP['title'])?></td>
      <td></td>
      <td></td>
      <td></td>
      <td>Maintenance Fee:</td>
      <td>(-<?=stripslashes('\$'.money($rowM['amount']))?>)</td>
    </tr>

<?php
}
}
}
?>
    <tr>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td>Total:</td>
      <td><?=stripslashes('\$'.money($logding_total))?></td>
      <td><?=stripslashes('\$'.money($commission_total))?></td>
      <td><?=stripslashes('\$'.money($logding_total-$commission_total-$maintenance_total))?></td>
    </tr>
</tbody>
</table>
