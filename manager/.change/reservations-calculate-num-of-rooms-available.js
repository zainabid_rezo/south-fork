function CalculateNumRoomsAvailableAjax(){
	
	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
	// Create a function that will receive data sent from the server
	ajaxRequest.onreadystatechange = function(){
		if(ajaxRequest.readyState == 4){
			var ajaxanswer = ajaxRequest.responseText;

			if(is_int(ajaxanswer)){
			
			if(ajaxanswer>2){
			document.getElementById('additionalrooms_display').style.display = 'block';
			}else{
				alert(ajaxanswer);
				}
			
			
			}else{alert(ajaxanswer)}
		}
	}

	var date_start = document.getElementById('date_start').value;
	var date_end = document.getElementById('date_end').value;
	var property_id = document.getElementById('property_id').value;

	var queryString = "?date_start=" + date_start + "&date_end=" + date_end + "&property_id=" + property_id;
	ajaxRequest.open("GET", "reservations-calculate-num-of-rooms-available.php" + queryString, true);
	ajaxRequest.send(null); 
}

function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
function is_int(value){ 
   for (i = 0 ; i < value.length ; i++) { 
      if ((value.charAt(i) < '0') || (value.charAt(i) > '9')) return false 
    } 
   return true; 
}