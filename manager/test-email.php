<?php
include_once("conn.php");
include_once("db-tables.php");
include_once("site-details.php");
include_once("functions.php");
?>

<?php
$showform = 'yes';



if(isset($_POST['btnUpdate']))
{
$error = "";


	$send_to	=	$_POST['send_to'];
	$fname	=	$_POST['fname'];
	$lname	=	$_POST['lname'];
	$date_start		=	$_POST['date_start'];
	$date_end		=	$_POST['date_end'];
	$nights		=	$_POST['nights'];
	$adults = $_POST['adults'];
	$children		=	$_POST['children'];
	$pets		=	$_POST['pets'];

	$to = $send_to;
					
$subject = "TEST EMAIL";

$message = "
Hi ".$fname.", <br><br>

  

Guest Name:  ".$fname." ".$lname."<br><br>
Arrival:  ".$date_start." Departure: ".$date_end." <br> Number of nights: ".$nights."<br><br>
Number of guests:    Adults ".$adults."  Children ".$children."  Pets  ".$pets."
";

// Always set content-type when sending HTML email
$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

// More headers
$headers .= 'From: South Fork Vacation <info@southforkvacation.com>' . "\r\n";

mail($to,$subject,$message,$headers);
	$success = "Successfully Send.";


$showform = 'no';
$done = true;

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link type="text/css" href="css/layout.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="js/easyTooltip.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="js/jquery.wysiwyg.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" src="js/searchbox.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<style type="text/css">
body {
	 background:#fff;
	 margin:20px;
}
</style>
<style type="text/css"> 


@import "calendar_popup/jquery.datepick.css";


.img {


background-color:#90654B;


display:inline;


float:left;


margin:10px;


padding:5px;


}


</style>


<script type="text/javascript" src="calendar_popup/jquery.datepick.js"></script>


<script type="text/javascript"> 


$(function() {





$('#date').datepick();


$('#due_date').datepick();


$('#paid_date').datepick();


$('#final_payment_date').datepick();





$('#date_start,#date_end').datepick({onSelect: customRange, 


    showOn: 'both', buttonImageOnly: true, buttonImage: 'calendar_popup/calendar.gif'}); 


 function customRange(dateStr, date) { 


    if (this.id == 'date_start') { 


        $('#date_end').datepick('option', 'minDate', date); 


    } 


    else { 


        $('#date_start').datepick('option', 'maxDate', date); 


    } 


}


 


});


 


</script>
<?php if(isset($done)){ ?>
<SCRIPT LANGUAGE="JavaScript">
	setTimeout("parent.parent.GB_hide();",2000);
	</SCRIPT>
<?php } ?>
</head>
<body>
<?php include"messages-display.php";?>
<?php if($showform == "yes"){ ?>

<h1>Test this email!</h1>
<?php include"messages-display.php";?>

<?php
/*$sql = "SELECT * FROM ".EMAILTEMPLATES." LIMIT 1";
$res = mysql_query($sql) or die("Error:".__LINE__.mysql_error());
while($row=mysql_fetch_array($res))
{*/
?>

<form id="update-form" name="update-form" method="post" action="" autocomplete="off">
  <table class="form-table">

    <tr>
      <td>Send to</td>
      <td><input type="text" name="send_to" id="send_to" value="<?=$POST['send_to']?>" class="big" size="30"></td>
    </tr>
    <tr>
      <td>First Name</td>
      <td><input type="text" name="fname" id="fname" value="<?=$_POST['fname'];?>" />
        </td>
    </tr>
    <tr>
      <td>Last Name</td>
      <td><input type="text" name="lname" id="lname" value="<?=$_POST['lname'];?>" /></td>
    </tr>
    <tr>
      <td>Start Date</td>
      <td><input type="text" name="date_start" id="date_start" value="<?=$_POST['date_start'];?>" size="8"/></td>
    </tr>
    <tr>
      <td>Departure Date</td>
      <td><input type="text" name="date_end" id="date_end" value="<?=$_POST['date_end'];?>" size="8"/></td>
    </tr>
    <tr>
      <td># of Nights</td>
      <td><input type="text" name="nights" id="nights" value="<?=$_POST['nights'];?>" size="5"/></td>
    </tr>
    <tr>
      <td>Adults</td>
      <td><input type="text" name="adults" id="adults" value="<?=$_POST['adults'];?>" size="5"/></td>
    </tr>
     <tr>
      <td>Children</td>
      <td><input type="text" name="children" id="children" value="<?=$_POST['children'];?>" size="5"/></td>
    </tr>
    <tr>
      <td>Pets</td>
      <td><input type="text" name="pets" id="pets" value="<?=$_POST['pets'];?>" size="5"/></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="btnUpdate" value="Test" class="button" /></td>
    </tr>
  </table>
</form>

<?php //} ?>



<?php } ?>
</body>
</html>