<?php
$_POST['date_start'] = mysql_real_escape_string(htmlspecialchars(strip_tags(date('m/d/Y',strtotime($_POST['date_start'])))));
$_POST['date_end'] = mysql_real_escape_string(htmlspecialchars(strip_tags(date('m/d/Y',strtotime($_POST['date_end'])))));
if(isset($back)){
?>
<form action="property-book-step-2.php?property_id=<?=(int)$_GET['property_id']?>&date_start=<?=mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['date_start'])))?>&date_end=<?=mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['date_end'])))?>" method="post">
<?php
foreach($_POST as $key=>$value) {
echo '<input type="hidden" name="'.htmlentities($key).'" value="'.htmlentities($value).'" />';
}
?>
<input name="Back" value="Back" type="submit" />
</form>
<?php
}
?>

<table width="90%" border="0" align="center">
  <tr>
    <td colspan="2">
      <?php
		$cronjobdays = $GLOBALS['cronjobdays'];
		$date1 = time();
		$date2 = strtotime($_POST['date_start']);
		$dateDiff = $date2 - $date1;
		$daysdifference = floor($dateDiff/(60*60*24));
		if($daysdifference>$cronjobdays && $cronjobdays > 0){ //2000 = to disable
		$deposit_term = "half";
		}else{
		$deposit_term = "full";
		}
		$rental_total_days = floor((strtotime($_POST['date_end'])-strtotime($_POST['date_start']))/(60*60*24));
		?>
    
      
      <?php 

      if($showform==true){

	  ?>
        <table width="700">
          <tr>
            <td colspan="4" align="left">
			<h2>Booking:
          	<?=stripslashes($property_name)?>
            
        	</h2>
			<h3>Step 3 - Confirm Details</h3>
		</td>
          </tr>
          <tr>
            <td width="149" align="right"><font color="#666666">First Name: </font></td>
            <td width="5">&nbsp;</td>
            <td width="149"><?=mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['firstname'])))?></td>
            <td width="149" align="right"><?php echo mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['international']))) ? '(International Customer)' : '';?></td>
          </tr>
          <tr>
            <td align="right"><font color="#666666">Last Name: </font></td>
            <td>&nbsp;</td>
            <td colspan="2"><?=mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['lastname'])))?></td>
          </tr>
          <tr>
            <td align="right"><font color="#666666">Address Line 1</font></td>
            <td>&nbsp;</td>
            <td colspan="2"><?=mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['address1'])))?></td>
          </tr>
          <tr>
            <td align="right"><font color="#666666">Address Line 2</font></td>
            <td>&nbsp;</td>
            <td colspan="2"><?=mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['address2'])))?></td>
          </tr>
          <tr>
            <td align="right"><font color="#666666">City</font></td>
            <td>&nbsp;</td>
            <td colspan="2"><?=mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['city'])))?></td>
          </tr>
          <tr>
            <td align="right"><font color="#666666">
              <?php if(isset($_POST['international'])) echo 'Province/';?>
              State:</font></td>
            <td>&nbsp;</td>
            <td colspan="2"><?php if(isset($_POST['international'])) echo mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['state_international']))); else echo mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['state']))); ?></td>
          </tr>
          <tr>
            <td align="right"><font color="#666666">Zip:</font></td>
            <td>&nbsp;</td>
            <td colspan="2"><?php if(isset($_POST['international'])) echo mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['zip_international']))); else echo mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['zip']))); ?></td>
          </tr>
          <?php if(isset($_POST['international'])) {?>
          <tr>
            <td align="right"><font color="#666666">Country:</font></td>
            <td></td>
            <td colspan="2"><?=mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['country'])))?></td>
          </tr>
          <?php } ?>
          <tr>
            <td align="right"><font color="#666666">Email:</font></td>
            <td>&nbsp;</td>
            <td colspan="2"><?=mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['email'])))?></td>
          </tr>
          <tr>
            <td align="right"><font color="#666666">Phone:</font></td>
            <td>&nbsp;</td>
            <td colspan="2"><?php if(isset($_POST['international'])) echo mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['phone_international']))); else echo mysql_real_escape_string(htmlspecialchars(strip_tags('('.$_POST['phone1'].') '.$_POST['phone2'].'-'.$_POST['phone3']))); ?>
            </td>
          </tr>
          <tr>
            <td align="right"><font color="#666666">Cell:</font></td>
            <td>&nbsp;</td>
            <td colspan="2"><?php if(isset($_POST['international'])) echo mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['cell_international']))); else echo mysql_real_escape_string(htmlspecialchars(strip_tags('('.$_POST['cell1'].') '.$_POST['cell2'].'-'.$_POST['cell3'])));?>
            </td>
          </tr>
          <tr>
            <td align="right"><font color="#666666">Property:</font></td>
            <td>&nbsp;</td>
            <td colspan="2"><?=stripslashes(get_property_name((int)$_GET['property_id']))?></td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2"><?php

			$lodgingamount = $price;

			$totalamount = $lodgingamount;
			
			//echo $totalamount.'<br>';
			
			//echo number_format($totalamount,2);

			$lodgertaxrate = get_lodger_tax_percentage((int)$_GET['property_id']);

			$salestaxrate = get_sales_tax_percentage((int)$_GET['property_id']);

			$subtotaltext1 .= "Lodging amount: $".number_format($lodgingamount,2)."\n";

			if($lodgertaxrate>'0' AND $rental_total_days<='29'){

			$subtotaltext1 .= "Lodgers Tax ($lodgertaxrate)%: $".number_format($lodgingamount*$lodgertaxrate/100,2,'.','')."\n";

			$totalamount = $totalamount+($lodgingamount*$lodgertaxrate/100);

			}

			if($salestaxrate>'0' AND $rental_total_days<='29'){

			$subtotaltext1 .= "Sales Tax ($salestaxrate)%: $".number_format($lodgingamount*$salestaxrate/100,2,'.','')."\n";

			$totalamount = $totalamount+($lodgingamount*$salestaxrate/100);

			}

			$subtotaltext2 = "<table width='300px'>";

			$subtotaltext2 .= "<tr><td>Arrival:</td><td align='right'>".mysql_real_escape_string(htmlspecialchars(strip_tags(date('m/d/Y',strtotime($_POST['date_start'])))))."</td></tr>";

			$subtotaltext2 .= "<tr><td>Departure:</td><td align='right'>".mysql_real_escape_string(htmlspecialchars(strip_tags(date('m/d/Y',strtotime($_POST['date_end'])))))."</td></tr>";

			$subtotaltext2 .= "<tr><td>Lodging amount:</td><td align='right'>$".number_format($lodgingamount,2)."</td></tr>";

			if($lodgertaxrate>'0' AND $rental_total_days<='29'){

			$subtotaltext2 .= "<tr><td>Lodgers Tax ($lodgertaxrate)%:</td><td align='right'>$".number_format($lodgingamount*$lodgertaxrate/100,2,'.','')."</td></tr>";

			}

			if($salestaxrate>'0' AND $rental_total_days<='29'){

			$subtotaltext2 .= "<tr><td>Sales Tax ($salestaxrate)%:</td><td align='right'>$".number_format($lodgingamount*$salestaxrate/100,2,'.','')."</td></tr>";

			}
			//echo $totalamount;
			

			/////////////////////////////////////////////////////////////////////

		

				$date01 = strtotime($_POST['date_start']);

				$date02 = strtotime($_POST['date_end']);

				$totalnights = $date02 - $date01;

				$totalnights = floor($totalnights/(60*60*24));

		

		

			  if($_POST['adults']>1000){ //change to '2' to enable

			  $additionaladults = $_POST['adults']-2;

				$additionalcharges = $additionaladults *20* $totalnights;

				$subtotaltext1 .= "$additionaladults additional people @ \$20 per night for $totalnights nights: $".number_format($additionalcharges,2,'.','')."\n";

				$subtotaltext2 .= "<tr><td>$additionaladults additional people @ \$20 per night for $totalnights nights:</td><td align='right'>$".number_format($additionalcharges,2,'.','')."</td></tr>";

		

			  $totalamount = $lodgingamount = str_replace(',','',$totalamount) + $additionalcharges;

			  }

		
				//echo '<br>'.$totalamount;
		

			/////////////////////////////////////////////////////////////////////

		

					  $result_lineitem= mysql_query("SELECT * FROM ".LINEITEMS." ORDER BY sortby ASC"); //WHERE importance = 'optional' 

						while($row_lineitem = mysql_fetch_array($result_lineitem)){



						if($row_lineitem['importance'] == 'optional'){

		

		

								if($row_lineitem['type']=="fixed"){

											if(isset($_POST[$row_lineitem['idname']])){
												
												//echo $row_lineitem['value'];

													$totalamount = str_replace(',','',$totalamount) + $row_lineitem['value'];
													
													//echo '<br>'.$totalamount;

													$subtotaltext1 .= stripslashes($row_lineitem['title']).": $".number_format($row_lineitem['value'],2,'.','')."\n";

													$subtotaltext2 .= "<tr><td>".stripslashes($row_lineitem['title']).": </td><td align='right'>$".number_format($row_lineitem['value'],2,'.','')."</td></tr>";

											}

								}elseif($row_lineitem['type']=="percentage"){

											if(isset($_POST[$row_lineitem['idname']])){

												if($row_lineitem['applyto']=="base"){

													$subtotaltext1 .= stripslashes($row_lineitem['title'])." ".$row_lineitem['value']."% : $".number_format($lodgingamount*$row_lineitem['value']/100,2,'.','')."\n";

													$subtotaltext2 .= "<tr><td>".stripslashes($row_lineitem['title'])." ".$row_lineitem['value']."% : </td><td align='right'>$".number_format($lodgingamount*$row_lineitem['value']/100,2,'.','')."</td></tr>";

													$totalamount = str_replace(',','',$totalamount) + (str_replace(',','',$lodgingamount)*$row_lineitem['value']/100);

													
													//echo '<br>'.$totalamount;
												
												}elseif($row_lineitem['applyto']=="sum"){

													$subtotaltext1 .= stripslashes($row_lineitem['title'])." ".$row_lineitem['value']."% : $".number_format($totalamount*$row_lineitem['value']/100,2,'.','')."\n";

													$subtotaltext2 .= "<tr><td>".stripslashes($row_lineitem['title'])." ".$row_lineitem['value']."% : </td><td align='right'>$".number_format($totalamount*$row_lineitem['value']/100,2,'.','')."</td></tr>";

													$totalamount = str_replace(',','',$totalamount) + (str_replace(',','',$totalamount)*$row_lineitem['value']/100);
													
													//echo '<br>'.$totalamount;
												
												}

											}

								}

		

						}elseif($row_lineitem['importance'] == 'mandatory'){

						

								if($row_lineitem['type']=="fixed"){

											

													$totalamount = str_replace(',','',$totalamount) + $row_lineitem['value'];

													$subtotaltext1 .= stripslashes($row_lineitem['title'])." : $".number_format($row_lineitem['value'],2,'.','')."\n";

													$subtotaltext2 .= "<tr><td>".stripslashes($row_lineitem['title']).": </td><td align='right'>$".number_format($row_lineitem['value'],2,'.','')."</td></tr>";

											

								}elseif($row_lineitem['type']=="percentage"){

											

												if($row_lineitem['applyto']=="base"){

													$subtotaltext1 .= stripslashes($row_lineitem['title'])." ".$row_lineitem['value']."% : $".number_format($lodgingamount*$row_lineitem['value']/100,2,'.','')."\n";

													$subtotaltext2 .= "<tr><td>".stripslashes($row_lineitem['title'])." ".$row_lineitem['value']."% :</td><td align='right'>$".number_format($lodgingamount*$row_lineitem['value']/100,2,'.','')."</td></tr>";

													$totalamount = str_replace(',','',$totalamount) + (str_replace(',','',$lodgingamount)*$row_lineitem['value']/100);	
													
													//echo '<br>'.$totalamount;				

												}elseif($row_lineitem['applyto']=="sum"){

													$subtotaltext1 .= stripslashes($row_lineitem['title'])." ".$row_lineitem['value']."% : $".number_format($totalamount*$row_lineitem['value']/100,2,'.','')."\n";

													$subtotaltext2 .= "<tr><td>".stripslashes($row_lineitem['title'])." ".$row_lineitem['value']."% :</td><td align='right'>$".number_format($totalamount*$row_lineitem['value']/100,2,'.','')."</td></tr>";

													$totalamount = str_replace(',','',$totalamount) + (str_replace(',','',$totalamount)*$row_lineitem['value']/100);
													
													//echo '<br>'.$totalamount;

												}

								}				

						} ?>
              <?php  } //while 

					$cleaning_fee = get_cleaning_fee((int)$_GET['property_id']);

					$subtotaltext1 .= "Cleaning Fee: $".number_format($cleaning_fee,2,'.','')."\n";

					$subtotaltext2 .= "<tr><td>Cleaning Fee: </td><td align='right'>$".number_format($cleaning_fee,2,'.','')."</td></tr>";

					//echo '<br>'.str_replace(',','',$totalamount);
					
					$totalamount = $totalamount + $cleaning_fee;

					

					$subtotaltext2 .= "<tr><td>Total:</td><td align='right'>$".number_format($totalamount,2,'.','')."</td></tr>";

					$subtotaltext2 .= "</table>";

					?>
              <?php //echo $totalamount;?>
              <?php echo str_replace(',','',$subtotaltext2);?> </td>
          </tr>
          <tr>
            <td align="right" valign="top"><!--<font color="#666666">Message:</font>--></td>
            <td>&nbsp;</td>
            <td colspan="2"><?php

			$_POST['lodging_amount'] = str_replace(',','',$lodgingamount);
			
			//echo $_POST['lodging_amount'];
			
			$_POST['date_start'] = mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['date_start'])));

			$_POST['date_end'] = mysql_real_escape_string(htmlspecialchars(strip_tags($_POST['date_end'])));

			$_POST['property_id'] = (int)$_POST['id'];

			$_POST['sub_total_amount'] = $subtotaltext1;

			$_POST['total_amount'] = number_format($totalamount,2,'.','');

			?>
            </td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="center"></td>
            <td colspan="2"><table>

			<?php

			if($deposit_term == "half"){

			?>

			  <tr>

				<td width="300"><font color="#FF0000"><em><strong>50% </strong>Payable today: </em></font></td>

				<td align="right"><font color="#FF0000">$<?php $halfamount = $totalamount/2; echo number_format($halfamount,2,'.','');?>

				</font></td>

			  </tr>

			  <tr>

				<td><font color="#0000FF"><em>Balance due on:

				  <?php echo date ("m/d/Y", strtotime ("-$cronjobdays days", strtotime($_POST['date_start']))); ?></em></font></td>

				<td align="right"><font color="#0000FF">$<?php echo number_format($halfamount,2,'.','');?></font></td>

			  </tr>

			  <?php

			  }else{

			  $deposit_term = "full";

			  ?>

			  <tr>

				<td><font color="#009900"><em>Payable today: </em></font></td>

				<td align="right"><font color="#009900">$<?php echo number_format($totalamount,2,'.','');?></font></td>

			  </tr>

			  <?php } ?>

			</table><!---->
            </td>
          </tr>
		  <form name="ccform" action="" method="post" onSubmit="javascript:return checkRead();">
          <input type="hidden" name="disprice" value="<?=number_format($show_dis_price,2);?>" />
		  <?php
				foreach($_POST as $key=>$value) {
				echo '<input type="hidden" name="'.htmlentities($key).'" value="'.htmlentities($value).'" />';
				}
				?>
          <tr>
            <td align="right">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td colspan="2">
			
			  
			  <table class="form-table">
  <tr>
    <th scope="row">CC Number: </th>
    <td><input type="text" name="ccnumber" value="" /></td>
    <td><label><img src="creditcards.png" width="165" height="27" />    </label></td>
  </tr>
  <tr>
    <th scope="row">Expiration:</th>
    <td colspan="2"><select name="ccexpM">
      <option value="01">01</option>
      <option value="02">02</option>
      <option value="03">03</option>
      <option value="04">04</option>
      <option value="05">05</option>
      <option value="06">06</option>
      <option value="07">07</option>
      <option value="08">08</option>
      <option value="09">09</option>
      <option value="10">10</option>
      <option value="11">11</option>
      <option value="12">12</option>
    </select>
      MM
      <select name="ccexpY">
        <option value="2014">2014</option>
        <option value="2015">2015</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019">2019</option>
        <option value="2020">2020</option>
      </select>
      YY</td>
  </tr>
  <tr>
    <th scope="row">CVV: </th>
    <td colspan="2"><input name="cvv" type="text" required="required" id="cvv" size="5" maxlength="4" /></td>
  </tr>
</table></td>
          </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td align="center">&nbsp;</td>
            <td colspan="2"><label>
              <input name="agree_to_terms" type="checkbox" id="agree_to_terms" onClick="return checkRead()" <?php if(isset($_POST['agree_to_terms'])){ echo 'checked="checked"'; }?> >
              I have read &amp; agree to the <a href="javascript:readTerms();">Terms &amp; Conditions</a></label>
              <script type="text/javascript">

				var g_readTerms;

				<?php if(isset($_POST['agree_to_terms'])){ ?>g_readTerms = 1;<?php }?>		

				

				function checkRead() {

					if(!g_readTerms){

					alert("You must read the Terms & Conditions.");

					return false;

					}

				}

				

				function readTerms() {

					g_readTerms = 1;

					popitup('terms-and-conditions.php');

				}

				</script>
              <script language="javascript" type="text/javascript">

				function popitup(url) {

					newwindow=window.open(url,'name','width=700,height=500,toolbar=0,menubar=0,location=1,status=1,scrollbars=1,resizable=1');

					if (window.focus) {newwindow.focus()}

					return false;

				}

				</script>
            </td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="center">&nbsp;</td>
            <td colspan="2">
                <input type="submit" name="btnRegisterGuestDo" style="background-color:#435161; width:100px; height:40px; border-radius:10px; cursor:pointer;" value="Reserve" />
                <input type="hidden" name="property_id" value="<?=(int)$_GET['property_id']?>" />
                <input type="hidden" name="deposit_term" value="<?=$deposit_term?>" />
                
              </td>
          </tr>
          </form>
		  <tr>
            <td align="right"></td>
            <td align="center">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="center">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="center">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="center">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="center">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="center">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="center">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td align="right"></td>
            <td align="center">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
        </table>
        <?php } ?>
      </td>
  </tr>
</table>